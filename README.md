# idyllic-website-2017

## Prerequisites
- [Node.js](http://nodejs.org/) - install via
- [NPM](https://www.npmjs.com/) - this comes with Node.js
- [Yarn](https://yarnpkg.com/en/) - `npm install -g yarn`
- [Gulp.js](http://gulpjs.com/) - `npm install -g gulp`
- [Nunjucks](https://mozilla.github.io/nunjucks/) - used as node js view engine

## Initial Setup
- run `yarn install` to get all the dependencies
- run `gulp` to run localhost server (http://localhost:8000/)
- run `npm start` to run like prodcation, staging server. ( All compressed assets are served )