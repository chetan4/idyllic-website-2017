'use strict';
// Dependencies
process.title = "idyllic_gulp"; // Do Not Delete this line

import gulp from 'gulp'; 
import del from 'del';
import revCollector from 'gulp-rev-collector';
import browserSync from 'browser-sync';
import gulpLoadPlugins from 'gulp-load-plugins';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const inject = browserSync.stream();

const paths = {
    fontsSrc: 'src/fonts/',
    jsonSrc: 'src/json/',
    htmlSrc: 'src/views/',
    sassSrc: 'src/sass/',
    jsSrc: 'src/js/',
    imgSrc: 'src/images/',

    buildDir: 'build/',
    revDir: 'build/rev/',
    distDir: 'dist/'
}

// Start thr nodemon server
gulp.task('nodemon', ['build'], (cb) => {

    var started = false;

    return $.nodemon({
        script: 'app.js'
    }).on('start', () => {
        // to avoid nodemon begin started multiple times
        if(!started){
            cb();
            started = true;
        }
    });
});

// Start the browser-sync on port 8000 once server is started
gulp.task('browser-sync', ['nodemon'], () => {
    browserSync.init(null, {
        proxy: "http://localhost:3000",
        port: 8000,
        notify: false
    });
});

// Check for the ENV
if(process.env.NODE_ENV === 'prod') {
    gulp.task('default', ['dist', 'nodemon']);
}else {
    gulp.task('default', ['build', 'browser-sync', 'watch']);
}

// Delete the build and dist folder 
gulp.task('clean', () => {
    del([paths.buildDir, paths.distDir])
        .then(console.log('deleting build and dist for rebuilding'))
});

gulp.task('build', ['build-html', 'build-css', 'build-js', 'build-images', 'build-favicon', 'build-fonts', 'build-json', 'build-xml'])

gulp.task('dist', ['dist-html', 'dist-js', 'dist-css', 'dist-images', 'dist-favicon', 'dist-fonts', 'dist-json', 'dist-xml'])

/*
HTML Tasks
*/

gulp.task('build-html', ['clean'], () => {
    return gulp.src(paths.htmlSrc + '**/*.html')
        .pipe(gulp.dest(paths.buildDir + 'views/'))
        .pipe(reload({ stream: true }))
})

gulp.task('dist-html', ['build-html'], () => {
    return gulp.src([
        paths.revDir + '**/*.json',
        paths.buildDir + 'views/' + '**/*.html'
    ])
        .pipe(revCollector())
        .pipe($.htmlmin({
            removeComments: true,
            collapseWhitespace: true,
            collapseBooleanAttributes: true,
            removeAttributeQuotes: true,
            removeRedundantAttributes: true,
            removeEmptyAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true,
            removeOptionalTags: true
        }))
        .pipe(gulp.dest(paths.distDir + 'views'));
})

/*
	CSS tasks
*/

gulp.task('build-css', () => {
    const AUTOPREFIXER_BROWSERS = [
        'ie >= 10',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ];
    return gulp.src(paths.sassSrc + 'styles.scss')
        .pipe($.newer(paths.buildDir + 'css/'))
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            precision: 10
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(gulp.dest(paths.buildDir + 'css/'))
        .pipe(inject)
})

gulp.task('dist-css', ['build-css', 'dist-images'], () => {
    return gulp.src([
        paths.buildDir + 'css/*',
        paths.revDir + "images/*.json"
    ])
        .pipe(revCollector())
        .pipe($.cssnano())
        .pipe($.rev())
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest(paths.distDir + 'css'))
        .pipe($.rev.manifest())
        .pipe(gulp.dest(paths.revDir + 'css'));
})

/*
	Font and Xml tasks
*/

gulp.task('build-fonts', () => {
    return gulp.src(paths.fontsSrc + '**/*.*')
        .pipe(gulp.dest(paths.buildDir + 'fonts/'))
        .pipe(reload({ stream: true }))
});

gulp.task('dist-fonts', ['build-fonts'], () => {
    return gulp.src('build/fonts/' + '**/*.*')
        .pipe(gulp.dest(paths.distDir + "/fonts/"));
});

gulp.task('build-xml', () => {
    return gulp.src('src/' + '**/*.xml')
        .pipe(gulp.dest(paths.buildDir))
        .pipe(reload({ stream: true }))
});

gulp.task('dist-xml', ['build-xml'], () => {
    return gulp.src('build/' + '**/*.xml')
        .pipe(gulp.dest(paths.distDir));
});

/*
JS Tasks
*/

gulp.task('build-js', ['js', 'js-plugins']);

gulp.task('js', () => {
    return gulp.src(paths.jsSrc + 'main.js')
        .pipe($.newer(paths.buildDir + 'js'))
        .pipe($.sourcemaps.init())
        .pipe($.babel())
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(paths.buildDir + 'js'))
        .pipe(reload({ stream: true }))
});

gulp.task('js-plugins', () => {
    return gulp.src([
        'src/lib/*.js'
    ])
        .pipe($.concat('vendor.js'))
        .pipe(gulp.dest(paths.buildDir + 'js/'))
        .pipe(reload({ stream: true }))
});

gulp.task('dist-js', ['build-js'], () => {
    return gulp.src(paths.buildDir + 'js/*')
        .pipe($.uglify())
        .pipe($.rev())
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(paths.distDir + 'js'))

        .pipe($.rev.manifest())
        .pipe(gulp.dest(paths.revDir + 'js'));
});

gulp.task('build-json', [], () => {
    return gulp.src(paths.jsonSrc + '*.json')
        .pipe(gulp.dest(paths.buildDir + 'json/'))
        .pipe(reload({ stream: true }))
});

gulp.task('dist-json', ['build-json', 'dist-images'], () => {
    return gulp.src([
        paths.buildDir + 'json/*',
        paths.revDir + "images/*.json"
    ])
        .pipe(revCollector())
        .pipe(gulp.dest(paths.distDir + "/json/"));
});

/*
Image Tasks
*/

gulp.task('build-images', () => {
    return gulp.src(paths.imgSrc + '**/*.+(png|jpeg|jpg|gif|svg|eps)')
        .pipe($.newer(paths.buildDir + 'images'))
        .pipe(gulp.dest(paths.buildDir + 'images'))
        .pipe(reload({ stream: true }))
})

gulp.task('dist-images', ['build-images'], () => {
    return gulp.src(paths.buildDir + 'images/**/*')
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(paths.distDir + 'images'))
        .pipe($.rev.manifest())
        .pipe(gulp.dest(paths.revDir + 'images'));
});

gulp.task('build-favicon', () => {
    return gulp.src('src/favicon.ico')
        .pipe($.newer(paths.buildDir))
        .pipe(gulp.dest(paths.buildDir))
        .pipe(reload({ stream: true }))
});

gulp.task('dist-favicon', ['build-favicon'], () => {
    return gulp.src(paths.buildDir + 'favicon.ico')
        .pipe($.newer(paths.distDir))
        .pipe(gulp.dest(paths.distDir));
});

gulp.task('watch', ['build'], () => {
    gulp.watch(['src/views/**/*.html'], reload);
    gulp.watch('src/sass/**', inject);
    gulp.watch('src/json/**', reload);
    gulp.watch(paths.jsSrc + '**/*.js', reload);
    gulp.watch('src/lib/**' + '**/*.js', reload);
    gulp.watch(paths.imgSrc + '**/*.+(png|jpeg|jpg|gif|svg)', reload);
});