"use strict"; /*global window*/
/*global $*/
/*global jQuery*/
/*global document*/
/*global swal*/

$(document).ready(function () {
  "use strict";
  $.fn.serializeObject = function (options) {
    options = jQuery.extend({}, options);

    var self = this,
    json = {},
    push_counters = {},
    patterns = {
      "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
      "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
      "push": /^$/,
      "fixed": /^\d+$/,
      "named": /^[a-zA-Z0-9_]+$/ };



    this.build = function (base, key, value) {
      base[key] = value;
      return base;
    };

    this.push_counter = function (key) {
      if (push_counters[key] === undefined) {
        push_counters[key] = 0;
      }
      return push_counters[key]++;
    };

    jQuery.each(jQuery(this).serializeArray(), function () {

      // skip invalid keys
      if (!patterns.validate.test(this.name)) {
        return;
      }

      var k,
      keys = this.name.match(patterns.key),
      merge = this.value,
      reverse_key = this.name;

      while ((k = keys.pop()) !== undefined) {

        // adjust reverse_key
        reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

        // push
        if (k.match(patterns.push)) {
          merge = self.build([], self.push_counter(reverse_key), merge);
        }

        // fixed
        else if (k.match(patterns.fixed)) {
            merge = self.build([], k, merge);
          }

          // named
          else if (k.match(patterns.named)) {
              merge = self.build({}, k, merge);
            }
      }

      json = jQuery.extend(true, json, merge);
    });


    return json;
  };
});

var Idyllic = function Idyllic() {};
Idyllic.Helper = function (config) {
  this.config = config;
  this.audio_tag = new Audio();
};

Idyllic.Home = function (rootScope) {
  this.helper = rootScope.helper;
};

Idyllic.Creates = function (rootScope) {
  this.helper = rootScope.helper;
};

Idyllic.Careers = function (rootScope) {
  this.helper = rootScope.helper;
};

Idyllic.CareerDetail = function (rootScope) {
  this.helper = rootScope.helper;
};

Idyllic.CaseStudies = function (rootScope) {
  this.helper = rootScope.helper;
};

Idyllic.Studio = function (rootScope) {
  this.helper = rootScope.helper;
};

var _util = {
  prevArrow: '<button type="button" data-role="none" class="slick-prev-icon hidden-xs" aria-label="Previous" tabindex="0" role="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 38"><defs><style>.brand-orange-fill-color{fill:#ee6321;}</style></defs><path d="M0 19a19 19 0 0 0 37.95 1.06h14.93a1 1 0 1 0 0-2H37.95A19 19 0 0 0 0 19zm2 0a17 17 0 0 1 33.95-.94H17.81L23 12.89a1.13 1.13 0 0 0-1.58-1.61l-7.14 6.91a1.05 1.05 0 0 0 0 1.53l7.14 6.92a1.14 1.14 0 0 0 .79.32 1.12 1.12 0 0 0 .79-.3 1.07 1.07 0 0 0 0-1.54l-5.24-5.06h18.19A17 17 0 0 1 2 19z" class="brand-orange-fill-color"/></svg></button>',
  nextArrow: '<button type="button" data-role="none" class="slick-next-icon hidden-xs" aria-label="Next" tabindex="0" role="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 38"><defs><style>.brand-orange-fill-color{fill:#ee6321;}</style></defs><path d="M35 0a19 19 0 0 0-18.95 18H1.12a1 1 0 1 0 0 2h14.93A19 19 0 1 0 35 0zm0 36a17 17 0 0 1-16.95-16h18.14L31 25.11a1.11 1.11 0 0 0 0 1.57 1.14 1.14 0 0 0 1.58 0l7.14-6.92a1.05 1.05 0 0 0 0-1.52l-7.14-6.91a1.14 1.14 0 0 0-1.58 0 1.08 1.08 0 0 0 0 1.55L36.19 18H18.05A17 17 0 1 1 35 36z" class="brand-orange-fill-color"/></svg></button>',

  sendEventGA: function sendEventGA(eventCategory, eventAction, eventLabel) {
    if (window.ga) {
      window.ga('send', {
        hitType: 'event',
        eventCategory: eventCategory || '',
        eventAction: eventAction || '',
        eventLabel: eventLabel || '' });

    }
  },

  sendExceptionGA: function sendExceptionGA(exDescription) {
    if (window.ga) {
      window.ga('send', 'exception', {
        hitType: 'event',
        'exDescription': exDescription,
        'exFatal': false });

    }
  } };


Idyllic.Helper.prototype = {
  initBasicSlick: function initBasicSlick() {
    $('.slides').each(function () {
      var params = {
        dots: false,
        autoplay: false,
        draggable: false,
        swipe: false,
        arrows: false,
        prevArrow: _util.prevArrow,
        nextArrow: _util.nextArrow,
        speed: 1000 //,
        // fade: true,
        // cssEase: 'linear'
      };
      $.extend(params, $(this).data('slick') || {});
      $(this).slick(params);
      //Custome Slider Click
      $(".project-section .slider-1").click(function () {
        $(".project-section .slider-2").removeClass("active");
        $(this).addClass("active");
        $(".project-section .fbg").addClass("hide-slider");
        $(".project-section .bbg").removeClass("hide-slider");
        $(".slides").slick('slickGoTo', parseInt('0'));
      });
      $(".project-section .slider-2").click(function () {
        $(".project-section .slider-1").removeClass("active");
        $(this).addClass("active");
        $(".project-section .bbg").addClass("hide-slider");
        $(".project-section .fbg").removeClass("hide-slider");
        $(".slides").slick('slickGoTo', parseInt('1'));
      });
    });
  },



  howWeDoItSlick: function howWeDoItSlick() {
    $(".how-we-do-slides").slick({
      dots: true,
      arrows: false,
      nextArrow: '<button type="button" data-role="none" class="slick-next-icon" aria-label="Next" tabindex="0" role="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 38"><defs><style>.brand-orange-fill-color{fill:#ee6321;}</style></defs><path d="M35 0a19 19 0 0 0-18.95 18H1.12a1 1 0 1 0 0 2h14.93A19 19 0 1 0 35 0zm0 36a17 17 0 0 1-16.95-16h18.14L31 25.11a1.11 1.11 0 0 0 0 1.57 1.14 1.14 0 0 0 1.58 0l7.14-6.92a1.05 1.05 0 0 0 0-1.52l-7.14-6.91a1.14 1.14 0 0 0-1.58 0 1.08 1.08 0 0 0 0 1.55L36.19 18H18.05A17 17 0 1 1 35 36z" class="brand-orange-fill-color"/></svg></button>',
      speed: 1000 });


    $(".how-we-do-it-section .nav-links li a").click(function () {
      _util.sendEventGA('How we do it', 'Click', 'How we do it Blue Screen click');
      $(".how-we-do-slides").slick('slickGoTo', $(this).data('index'));
    });

    $('.how-we-do-slides').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      var el = $(this),
      mainContainer = el.closest(".how-we-do-it-section"),
      navLinksContainer = mainContainer.find(".nav-links-container"),
      activeLi = navLinksContainer.find(".active"),
      allLi = navLinksContainer.find("li");

      activeLi.removeClass('active');
      allLi.eq(nextSlide).addClass('active');
    });
  },

  teamImageSliderSlick: function teamImageSliderSlick() {
    $(".team-image-slider").slick({
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      infinite: true,
      slidesToShow: 5,
      centerMode: true,
      variableWidth: true,
      asNavFor: '.team-name-slider' });


    $('.team-name-slider').slick({
      dots: false,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
      asNavFor: '.team-image-slider',
      prevArrow: _util.prevArrow,
      nextArrow: _util.nextArrow,
      fade: true });

  },

  coreTeamImageSliderSlick: function coreTeamImageSliderSlick() {
    $(".core_team-image-slider").slick({
      arrows: false,
      slidesToShow: 3,
      focusOnSelect: true,
      asNavFor: '.core_team-name-slider' });


    $('.core_team-name-slider').slick({
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      asNavFor: '.core_team-image-slider',
      fade: true });

  },

  initTwoSliderSlick: function initTwoSliderSlick() {
    $('.slides-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      fade: true,
      asNavFor: '.slides-nav' });


    $('.slides-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      asNavFor: '.slides-for',
      centerMode: true,
      focusOnSelect: true });

  },

  randomIndex: function randomIndex(len) {
    return Math.floor(Math.random() * len);
  },

  get_random: function get_random(data, count) {
    var self = this;
    if (count && count >= 1) {
      var arr = [],
      len = data.length,
      indexHash = {};
      if (count >= len) {
        return data;
      }
      while (1) {
        var index = self.randomIndex(len);
        if (arr.length == count) {
          break;
        }
        if (!indexHash.hasOwnProperty(index) && index < len) {
          indexHash[index] = true;
          arr.push(data[index]);
        }
      }
      return arr;
    } else {
      return data[self.randomIndex(data.length)] || data[0];
    }
  },

  videoInit: function videoInit() {
    /* Play video in a modal window */
    var myContent = $('.modal_content'),
    player,
    triggerButtons = $('.modal_trigger, .btn_watch_video'),
    closeButton = $('.modal_head a'),
    vimeoId,
    modal_overlay = $('.modal_overlay');

    function hideModal(evt) {
      evt.preventDefault();
      $('body').removeClass('modal_opened');
      modal_overlay.removeClass('active');
      _util.sendEventGA('Video Modal', 'Click', 'Video Model Closed');
      var tmp = $('<div class="iframe_container"></div>');
      myContent.
      removeClass('modal_open').
      find('.iframe_container')[0].replaceWith(tmp[0]);
      try {
        player.pauseVideo();
      } catch (e) {}
      player = null;
    }

    modal_overlay.click(hideModal);
    $(closeButton).click(hideModal);

    function loadVimeoVideo(vimeoId) {
      var el,
      popupEl = $("#video_modal_content"),
      data;
      $('.thumbnail-section.active').removeClass('active');
      player.loadVideo(vimeoId).then(function (id) {
        player.play().catch();
        el = $('.thumbnail-section[data-video-id="' + id + '"]');
        data = el.data();
        el.addClass('active');
        _util.sendEventGA('Vimeo Video Played', 'Click', data.vimeo_title);
        // popupEl.find('.vimeo_title').html(data.vimeo_title);
        // popupEl.find('.vimeo_description').html(data.vimeo_description);
      });
    }

    $('#video_modal_content .thumbnail-section').click(function () {
      vimeoId = $(this).data('videoId');
      loadVimeoVideo(vimeoId);
    });

    triggerButtons.click(function (e) {
      e.preventDefault();
      var el = $(this),
      data = el.data(),
      modelEl = $(data.modal_id),
      container = modelEl.find('.iframe_container')[0];
      _util.sendEventGA('Video Modal', 'Click', data.videoFor + " Model Open");
      $('body').addClass('modal_opened');
      modal_overlay.addClass('active');
      modelEl.addClass('modal_open');
      if (data.video_type == 'youtube') {
        player = new YT.Player(container, {
          videoId: data.video,
          playerVars: { 'autoplay': 1, 'controls': 0 },
          events: {
            'onReady': function onReady(event) {
              _util.sendEventGA('Video Played', 'Click', data.videoFor);
            } } });


      } else if (data.video_type == 'vimeo') {
        player = new Vimeo.Player(container, {
          id: data.video,
          loop: false,
          title: false,
          autoplay: true });

      } else
      if (data.video_type == 'vimeo_album') {
        modelEl.find('.slides').slick('slickGoTo', 0);
        vimeoId = modelEl.find('.thumbnail-section').not('.slick-cloned').eq(0).data('videoId');
        player = new Vimeo.Player(container, {
          id: vimeoId,
          loop: false,
          title: false });

        player.getEnded().then(function (ended) {
        }).catch(function (error) {
        });
        player.on('ended', function () {
          try {
            var nextEl = $('.thumbnail-section[data-video-id="' + vimeoId + '"]').not('.slick-cloned').next(),
            nextIndex = nextEl.data('index');
            modelEl.find('.slides').slick('slickGoTo', nextIndex);
            vimeoId = nextEl.data('videoId');
            loadVimeoVideo(vimeoId);
          } catch (e) {}
        });
        loadVimeoVideo(vimeoId);
      }
    });
  },

  loadImage: function loadImage(url) {
    var a = new Image();
    a.src = url;
  },

  playAudio: function playAudio(src, volume) {
    this.audio_tag.pause();
    this.audio_tag.src = src;
    if (volume) {
      this.audio_tag.volume = volume;
    }
    return this.audio_tag.play().catch();
  },

  processData: function processData(data) {
    var details = '',
    tags = data.tags;
    if (data.hasOwnProperty('tags')) {
      details += "Looking for";
      if (tags.hasOwnProperty('product_tags')) {
        details += ' PRODUCT: ' + tags.product_tags.join(", ") + '.';
      }
      if (tags.hasOwnProperty('design_tags')) {
        details += ' DESIGN: ' + tags.design_tags.join(", ") + '.';
      }
      if (tags.hasOwnProperty('development_tags')) {
        details += ' DEVELOPMENT: ' + tags.development_tags.join(", ") + '.';
      }
      if (tags.hasOwnProperty('budget_tags')) {
        details += ' BUDGET: ' + tags.budget_tags.join(", ") + '.';
      }
    } else {
      details = "Looking for other details";
    }
    data.details = details;
    delete data.tags;
    return data;
  },

  addAjaxLoader: function addAjaxLoader(scope) {
    var loader = '<span class="ajax-loader"></span>';
    if (scope.find('.ajax-loader')) {
      scope.find('.ajax-loader').remove();
    }
    scope.addClass('loaderIn');
    scope.append(loader);
    scope.attr('disabled', 'disabled');
  },

  removeAjaxLoader: function removeAjaxLoader(scope) {
    scope.removeClass('loaderIn');
    scope.find('.ajax-loader').remove();
    scope.removeAttr('disabled');
  },

  submitData: function submitData(url, jFrom, data, cb, extraParams) {
    var self = this,
    config = self.config,
    params,
    btn = jFrom.find('button[type="submit"]');
    self.addAjaxLoader(btn);

    params = {
      url: config.baseUrl + url,
      headers: {
        'Authorization': 'Token token=' + config.apiKey },

      method: 'POST',
      cache: false,
      dataType: 'json',
      data: data,
      success: function success(res) {
        self.removeAjaxLoader(btn);
        if (res.success) {
          jFrom[0].reset();
        }
        if (cb) {
          cb(res);
        }
      },
      error: function error() {
        self.removeAjaxLoader(btn);
        _util.sendExceptionGA("Server Error for " + url);
        swal({ title: "Oops!", text: "Something went wrong. Please try later.", type: "error", confirmButtonText: "OK" });
      } };

    $.extend(params, extraParams || {});
    $.ajax(params);
  } };


Idyllic.prototype = {
  init: function init(config) {
    var self = this;
    self.helper = new Idyllic.Helper(config);
    self.helper.initBasicSlick();
    self.initEventListeners(config);

    switch (config.page_type) {
      case "home":
        self.home = new Idyllic.Home(self);
        self.home.init();
        break;

      case "creates":
        self.creates = new Idyllic.Creates(self);
        self.creates.init();
        break;

      case "case_studies":
        self.caseStudies = new Idyllic.CaseStudies(self);
        self.caseStudies.init();
        break;

      case "studio":
        self.studio = new Idyllic.Studio(self);
        self.studio.init();
        break;

      case "careers":
        self.careers = new Idyllic.Careers(self);
        self.careers.init();
        break;

      case "career_detail":
        self.careerDetail = new Idyllic.CareerDetail(self);
        self.careerDetail.init();
        break;}


  },

  initEventListeners: function initEventListeners(config) {
    var self = this,
    is_home = config.page_type == "home",
    is_studio = config.page_type == "studio";

    if (is_home) {
      var videoEl = $("#bg-video");
      if (videoEl.length) {
        videoEl[0].play().catch();
      }
    }

    $(document).on('change', ".date_section input[type='radio']", function () {
      var index = $(this).parent().index(),
      el = $("#choose_another_time_field_set fieldset").eq(index);

      $("#choose_another_time_field_set fieldset").addClass('hidden').attr('disabled', 'disabled');
      el.removeClass('hidden').removeAttr('disabled');
      el.find("input[type='radio']")[0].checked = true;
    });

    $(document).on('change', ".choose_tomorrow_time input[type='radio']", function () {
      $("#choose_another_time_field_set").addClass('hidden').attr('disabled', 'disabled');
    });

    $(document).on('click', ".choose_another_time", function () {
      var el = $(".date_section input[type='radio']");
      $("#choose_another_time_field_set").removeClass('hidden').removeAttr('disabled');
      $("fieldset input[type='radio']").removeAttr('checked');
      el.eq(0)[0].checked = true;
      el.eq(0).trigger('change');
    });

    $('.menu-popup-icon .menu-icon-btn').on('click', function (e) {
      e.preventDefault();
      _util.sendEventGA('Menu', 'Click', 'Menu Button Opened');
      $('.menu-section-wrap').removeClass('hidden').show();
      $("body").addClass('menu-open');
      $('.close').on('click', function () {
        $('.menu-section-wrap').addClass('hidden').hide();
        $("body").removeClass('menu-open');
      });
    });

    $(".video_email_form").submit(function (e) {
      e.preventDefault();
      var jFrom = $(this),
      data = jFrom.serializeObject();

      _util.sendEventGA('Video Link', 'Click', 'Video Link Ajax request');
      self.helper.submitData('api/v1/intro_video', jFrom, data, function (res) {
        jFrom[0].reset();
        swal({
          title: "Stay Tuned",
          text: "You will shortly receive video link email",
          timer: 3000,
          type: 'success',
          showConfirmButton: false });

      });
    });

    $(".fact_book_form").submit(function (e) {
      e.preventDefault();
      var jFrom = $(this),
      data = jFrom.serializeObject();

      _util.sendEventGA('Fact Book', 'Click', 'Fact book Ajax request');
      self.helper.submitData('api/v1/downloads.json', jFrom, data, function (res) {
        jFrom[0].reset();
        swal({
          title: "Stay Tuned",
          text: "You will shortly receive download link email",
          timer: 3000,
          type: 'success',
          showConfirmButton: false });

      });
    });

    $(window).scroll(function () {
      var top = this.pageYOffset,
      width = $(window).width(),
      endPoint = $("[data-hide-menu-section]").height() + $("[data-hide-menu-section]").offset().top - 150;
      endPoint = endPoint < 50 ? 0 : endPoint;
      if (top > endPoint) {
        if (is_studio && width >= 768) {
          $('.header-section-wrap .page-heading p').addClass('hideText').removeClass('showText');
          $('.header-section-wrap .mobile-submenu').addClass('hideText').removeClass('showText');
          $('.header-section-wrap .page-heading').addClass('scrollHeading');
        } else
        {
          $('.header-section-wrap .mobile-submenu').addClass('hideText').removeClass('showText');
          $('.header-section-wrap .page-heading').addClass('hideText').removeClass('showText');
          $('.header-section-wrap .menu-popup-icon').addClass('fixedPosition').removeClass('staticPosition');
        }
      } else {
        $('.header-section-wrap .page-heading').removeClass('scrollHeading').removeClass('hideText').addClass('showText');
        $('.header-section-wrap .page-heading p').removeClass('hideText').addClass('showText');
        $('.header-section-wrap .mobile-submenu').removeClass('hideText').addClass('showText');
        $('.header-section-wrap .menu-popup-icon').removeClass('fixedPosition').addClass('staticPosition');
        $('.header-section-wrap').addClass('fixedPosition');
      }

      if (is_home) {
        var viewPortHeight = $(".main-viewport-section").height(),
        videoEl = $("#bg-video"),
        videoOutViewportMax = $(".we-specialize-section").offset().top + 300;
        if (videoEl.length) {
          if (top > videoOutViewportMax) {
            videoEl[0].pause();
          } else {
            videoEl[0].play().catch();
          }
        }
        if (top > $(".project-section-wrap.brig_bg").offset().top - 120) {
          $(".project-section-wrap.brig_bg").addClass('in_viewport_section');
        }
        if (top > $(".project-section-wrap.fankave_bg").offset().top - 80) {
          $(".project-section-wrap.fankave_bg").addClass('in_viewport_section');
        }
        if (top > viewPortHeight) {
          width = $(window).width();
          if (width >= 768) {
            $(".phontom-div").height(viewPortHeight);
            $("body").removeClass("overlayIn");
            $(".we-specialize-section").addClass("scrollIn");
          } else
          {
            $(".phontom-div").height(0);
            $("body").removeClass("overlayIn");
            $(".we-specialize-section").addClass("scrollIn");
          }
        } else {
          $("body").addClass("overlayIn");
          $(".phontom-div").height(top);
          $(".we-specialize-section").removeClass("scrollIn");
        }
      }
    });


    $("#contact_from").submit(function (e) {
      e.preventDefault();
      var jFrom = $(this),
      data = self.helper.processData(jFrom.serializeObject());
      data['g-recaptcha-response'] = $("#g-recaptcha-response").val();

      _util.sendEventGA('Contact Form', 'Click', 'Contact form Ajax request');
      self.helper.submitData('api/v1/inquiries.json', jFrom, data, function (res) {
        if (res.success) {
          swal({
            title: "Stay Tuned",
            text: "Idyllic team will contact you.",
            timer: 3000,
            type: 'success',
            showConfirmButton: false });

        } else {
          if (res.errors) {
            try {
              swal({ title: "Oops!", text: res.errors.join(", "), type: "error", confirmButtonText: "OK" });
            } catch (e) {
              swal({ title: "Oops!", text: "Something went wrong. Please try later.", type: "error", confirmButtonText: "OK" });
            }
          }
        }
      });
    });


    $("footer .idyllic-locations img").hover(
    function () {
      var width = $(window).width();
      if (width >= 768) {
        _util.sendEventGA('Footer team Image', 'Hover', 'Footer Team Image Hover');
        self.helper.playAudio('/audio/' + $(this).closest('.idyllic-locations').data('soundType') + '.mp3', 0.4);
      }
    }, function () {
      self.helper.audio_tag.pause();
    });



    $('.scroll_to').on('click', function () {
      var cls = $(this).data('whereToScroll'),
      el = $(cls);
      _util.sendEventGA('Scroll To', 'Click', 'Scroll to Bounce button click');
      $("html,body").animate({ scrollTop: el.offset().top }, 400);
    });

  } };


Idyllic.Home.prototype = {
  init: function init() {
    var self = this,
    helper = self.helper;

    helper.videoInit();
    $("body").addClass("overlayIn");

    $("#give_me_more").click(function () {
      _util.sendEventGA('Scroll To', 'Click', 'Scroll to Bounce button click');
      $("html,body").animate({ scrollTop: $(".main-page-top-section").outerHeight() + 50 }, 400);
    });
  } };


Idyllic.CaseStudies.prototype = {
  init: function init() {
    var self = this;
    self.initFankave();
  },

  initFankave: function initFankave() {
    var self = this,
    helper = self.helper;

    $(".slide-fankave").slick({
      dots: false,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
      prevArrow: '<button type="button" data-role="none" class="slick-prev-icon hidden-xs" aria-label="Previous" tabindex="0" role="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 38"><defs><style>.brand-orange-fill-color{fill:#ee6321;}</style></defs><path d="M0 19a19 19 0 0 0 37.95 1.06h14.93a1 1 0 1 0 0-2H37.95A19 19 0 0 0 0 19zm2 0a17 17 0 0 1 33.95-.94H17.81L23 12.89a1.13 1.13 0 0 0-1.58-1.61l-7.14 6.91a1.05 1.05 0 0 0 0 1.53l7.14 6.92a1.14 1.14 0 0 0 .79.32 1.12 1.12 0 0 0 .79-.3 1.07 1.07 0 0 0 0-1.54l-5.24-5.06h18.19A17 17 0 0 1 2 19z" class="brand-orange-fill-color"/></svg></button>',
      nextArrow: '<button type="button" data-role="none" class="slick-next-icon hidden-xs" aria-label="Next" tabindex="0" role="button"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 38"><defs><style>.brand-orange-fill-color{fill:#ee6321;}</style></defs><path d="M35 0a19 19 0 0 0-18.95 18H1.12a1 1 0 1 0 0 2h14.93A19 19 0 1 0 35 0zm0 36a17 17 0 0 1-16.95-16h18.14L31 25.11a1.11 1.11 0 0 0 0 1.57 1.14 1.14 0 0 0 1.58 0l7.14-6.92a1.05 1.05 0 0 0 0-1.52l-7.14-6.91a1.14 1.14 0 0 0-1.58 0 1.08 1.08 0 0 0 0 1.55L36.19 18H18.05A17 17 0 1 1 35 36z" class="brand-orange-fill-color"/></svg></button>'
      // fade: true,
      // cssEase: 'linear'
    });
    $('.ideation-section-wrap .slides .slides-items').each(function () {helper.loadImage($(this).data('url'));});
    $('.ideation-section-wrap .slides').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      var el = $(slick.$slides[nextSlide]);
      $('.ideation-section-wrap .ideation-wrap-up-img img').attr('src', el.data('url'));
    });

  } };


Idyllic.Studio.prototype = {
  init: function init() {
    var self = this,
    helper = self.helper;

    helper.howWeDoItSlick();
    helper.teamImageSliderSlick();
    helper.coreTeamImageSliderSlick();
    helper.videoInit();
    self.initEventListeners();
  },

  initEventListeners: function initEventListeners() {
    var facts = [],
    fact,index,
    self = this,
    afterLastSection = function afterLastSection() {
      $('.embarrassing-facts-section').addClass('last-section-visible');
      $('.btn_refresh_section').hide();
      $('.btn_smile_section').show();
      _util.sendEventGA('About Us', 'Click', 'About Us Last Fact Download more visible');
    },
    aboutUsSoundLoad = function aboutUsSoundLoad() {
      index = $(".fact_slide_item.slick-active").data('index');
      if (index != -1) {
        $(".fact_slides").slick('slickGoTo', index + 1);
        _util.sendEventGA('About Us', 'Click', 'Show more fact on about us');
      } else {
        afterLastSection();
      }
    },
    afterSoundLoad = function afterSoundLoad(fact) {
      _util.sendEventGA('Embarrassing Fact', 'Click', 'Show more embarrassing fact');
      $(".random_facts_text").html(fact.text);
      $(".random_facts_sub_text").html(fact.sub_text);
    },
    helper = self.helper;

    $(".mobile-submenu").scrollLeft($(".mobile-submenu .active-page").offset().left);

    $(".fact_slides").on('afterChange', function (event, slick, currentSlide) {
      var els = $(this).find('.fact_slide_item');
      if (els.eq(currentSlide).data('index') == -1) {
        afterLastSection();
      }
    });


    if (sessionStorage.facts) {
      facts = JSON.parse(sessionStorage.facts);
    } else {
      _util.sendEventGA('Get Fact Ajax', 'Click', 'Get Fact Data from Ajax');
      $.ajax({
        url: '/facts',
        method: 'GET',
        success: function success(res) {
          if (res.success) {
            sessionStorage.setItem('facts', JSON.stringify(res.facts));
            facts = res.facts;
          }
        } });

    }

    $('.fact_slides').slick({
      autoplay: false,
      arrows: false,
      fade: true });


    $(".refresh_facts_btn").on('click', function () {
      var type = $(this).data('type'),playPromise,
      width = $(window).width(),
      index;
      if (type == 'about_us') {
        playPromise = helper.playAudio("/audio/page_flip.mp3");
        if (playPromise !== undefined) {
          playPromise.then(function () {
            aboutUsSoundLoad();
          });
        } else {
          aboutUsSoundLoad();
        }
      } else {
        fact = self.helper.get_random(facts[type] || []);
        if (fact) {
          if (width >= 768) {
            playPromise = helper.playAudio("/audio/refresh.mp3");
            if (playPromise !== undefined) {
              playPromise.then(function () {
                afterSoundLoad(fact);
              });
            } else {
              afterSoundLoad(fact);
            }
          } else {
            afterSoundLoad(fact);
          }
        }
      }
    });
  } };


Idyllic.Creates.prototype = {
  init: function init() {
    var self = this,
    helper = self.helper,
    slickNavEl = $('.creates-viewport-section .slides-nav'),
    slickForEl = $('.creates-viewport-section .slides-for');

    helper.initTwoSliderSlick();

    slickForEl.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      var els = $(this).find('.slides-items');

      $('.creates-viewport-section').
      removeClass(els.eq(currentSlide).data('cls')).
      addClass(els.eq(nextSlide).data('cls'));
    });

    slickNavEl.find('.slides-items').hover(
    function () {
      _util.sendEventGA('Create Case Study', 'Hover', 'Creates Page Case Study Mouse Hover');
      slickForEl.slick('slickGoTo', $(this).data('index'));
      slickForEl.slick('slickPause');
    }, function () {
      slickForEl.slick('slickPlay');
    });


    $("#where_to_begin_form").submit(function (e) {
      e.preventDefault();
      var jFrom = $(this),
      data = helper.processData(jFrom.serializeObject());

      data.name = "Only Email inquiries from Create Page";
      data.location = "NA";
      data.phone_number = "NA";

      _util.sendEventGA('Creates Inquiries', 'Click', 'Creates Inquiries Form Submit');
      self.helper.submitData('api/v1/inquiries/email_inquiry.json', jFrom, data, function (res) {
        if (res.success) {
          swal({
            title: "Stay Tuned",
            text: "Idyllic team will contact you.",
            timer: 3000,
            type: 'success',
            showConfirmButton: false });

        } else {
          if (res.errors) {
            try {
              swal({ title: "Oops!", text: res.errors.join(", "), type: "error", confirmButtonText: "OK" });
            } catch (e) {
              swal({ title: "Oops!", text: "Something went wrong. Please try later.", type: "error", confirmButtonText: "OK" });
            }
          }
        }
      });

    });
  } };


Idyllic.Careers.prototype = {
  init: function init() {
    var self = this,
    helper = this.helper;
    $('.work_at_idyllic_section .slides .slides-items').each(function () {helper.loadImage($(this).data('url'));});

    $('.work_at_idyllic_section .slides').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      var el = $(slick.$slides[nextSlide]);
      $('.work_at_idyllic_section').css('background-image', 'url(' + el.data('url') + ')');
    });
  } };



Idyllic.CareerDetail.prototype = {
  init: function init() {
    var self = this;

    $("#career_detail_form").submit(function (e) {
      e.preventDefault();
      var jFrom = $(this),
      data = new FormData(jFrom[0]);

      _util.sendEventGA('Job Application', 'Click', 'Job Application Form Submit');
      self.helper.submitData('api/v1/job_applications.json', jFrom, data, function (res) {
        if (res.success) {
          swal({
            title: "Stay Tuned",
            text: "Idyllic team will contact you.",
            timer: 3000,
            type: 'success',
            showConfirmButton: false });

        } else {
          if (res.errors) {
            try {
              swal({ title: "Oops!", text: res.errors.join(", "), type: "error", confirmButtonText: "OK" });
            } catch (e) {
              swal({ title: "Oops!", text: "Something went wrong. Please try later.", type: "error", confirmButtonText: "OK" });
            }
          }
        }
      }, {
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false });

    });
  } };


//We Learn Section Slider
var learn = {
  slider: function slider() {
    $(".we-learn-slides").slick({
      dots: false,
      speed: 500,
      slidesToShow: 1,
      arrows: false,
      draggable: false });

    $(".We-learn-section .slider-1").click(function () {
      $(this).addClass("active");
      $(".We-learn-section .slider-2").removeClass("active");
      $(".we-learn-slides").slick("slickGoTo", parseInt("0"));
    });
    $(".We-learn-section .slider-2").click(function () {
      $(this).addClass("active");
      $(".We-learn-section .slider-1").removeClass("active");
      $(".we-learn-slides").slick("slickGoTo", parseInt("1"));
    });
  } };


$(document).ready(function () {
  learn.slider();
});

//res-slider
var res = {
  res: function res() {
    $(".res-slides").slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      arrows: false,
      centerMode: true,
      centerPadding: '40px',
      responsive: [
      {
        breakpoint: 1200,
        settings: "unslick" },

      {
        breakpoint: 1920,
        settings: "unslick" },

      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1 } }] });




    $(".res-slider .left-arrow").click(function () {
      $(".res-slides").slick('slickPrev');
    });
    $(".res-slider .right-arrow").click(function () {
      $(".res-slides").slick('slickNext');
    });
  } };


// Calling the res slider function
$(document).ready(function () {
  res.res();
});

//Clients slider
var clients = {
  slider: function slider() {
    $(".clients-slides").slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      arrows: false,
      centerMode: true,
      responsive: [
      {
        breakpoint: 1200,
        settings: "unslick" },

      {
        breakpoint: 1920,
        settings: "unslick" },

      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1 } },


      {
        breakpoint: 480,
        settings: {
          variableWidth: true } }] });




    $(".clients-slider .left-arrow").click(function () {
      $(".clients-slides").slick('slickPrev');
    });
    $(".clients-slider .right-arrow").click(function () {
      $(".clients-slides").slick('slickNext');
    });
  } };


// Calling the res slider function
$(document).ready(function () {
  clients.slider();
});

// //Scroll to contact page
// $(function() {
//     console.log();
//     var $root = $('html, body');
//
//     $("#contact-us").click(function(e) {
//         var linkId = $(this).attr("href");
//
//         if (window.location.pathname === "/") {
//             $root.animate({
//                 scrollTop: $(linkId).offset().top + 500
//             }, 500);
//         } else {
//             $root.animate({
//                 scrollTop: $(linkId).offset().top
//             }, 1000);
//         }
//         return false;
//
//     });
// });


$(function () {
  if ($(window).width("<768px")) {
    $(".nav-list, .social-contact").hide();
  }

  $(".menu-icon").click(function () {
    $(".nav-list, .social-contact").slideToggle();
  });
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOlsiJCIsImRvY3VtZW50IiwicmVhZHkiLCJmbiIsInNlcmlhbGl6ZU9iamVjdCIsIm9wdGlvbnMiLCJqUXVlcnkiLCJleHRlbmQiLCJzZWxmIiwianNvbiIsInB1c2hfY291bnRlcnMiLCJwYXR0ZXJucyIsImJ1aWxkIiwiYmFzZSIsImtleSIsInZhbHVlIiwicHVzaF9jb3VudGVyIiwidW5kZWZpbmVkIiwiZWFjaCIsInNlcmlhbGl6ZUFycmF5IiwidmFsaWRhdGUiLCJ0ZXN0IiwibmFtZSIsImsiLCJrZXlzIiwibWF0Y2giLCJtZXJnZSIsInJldmVyc2Vfa2V5IiwicG9wIiwicmVwbGFjZSIsIlJlZ0V4cCIsInB1c2giLCJmaXhlZCIsIm5hbWVkIiwiSWR5bGxpYyIsIkhlbHBlciIsImNvbmZpZyIsImF1ZGlvX3RhZyIsIkF1ZGlvIiwiSG9tZSIsInJvb3RTY29wZSIsImhlbHBlciIsIkNyZWF0ZXMiLCJDYXJlZXJzIiwiQ2FyZWVyRGV0YWlsIiwiQ2FzZVN0dWRpZXMiLCJTdHVkaW8iLCJfdXRpbCIsInByZXZBcnJvdyIsIm5leHRBcnJvdyIsInNlbmRFdmVudEdBIiwiZXZlbnRDYXRlZ29yeSIsImV2ZW50QWN0aW9uIiwiZXZlbnRMYWJlbCIsIndpbmRvdyIsImdhIiwiaGl0VHlwZSIsInNlbmRFeGNlcHRpb25HQSIsImV4RGVzY3JpcHRpb24iLCJwcm90b3R5cGUiLCJpbml0QmFzaWNTbGljayIsInBhcmFtcyIsImRvdHMiLCJhdXRvcGxheSIsImRyYWdnYWJsZSIsInN3aXBlIiwiYXJyb3dzIiwic3BlZWQiLCJkYXRhIiwic2xpY2siLCJjbGljayIsInJlbW92ZUNsYXNzIiwiYWRkQ2xhc3MiLCJwYXJzZUludCIsImhvd1dlRG9JdFNsaWNrIiwib24iLCJldmVudCIsImN1cnJlbnRTbGlkZSIsIm5leHRTbGlkZSIsImVsIiwibWFpbkNvbnRhaW5lciIsImNsb3Nlc3QiLCJuYXZMaW5rc0NvbnRhaW5lciIsImZpbmQiLCJhY3RpdmVMaSIsImFsbExpIiwiZXEiLCJ0ZWFtSW1hZ2VTbGlkZXJTbGljayIsImF1dG9wbGF5U3BlZWQiLCJpbmZpbml0ZSIsInNsaWRlc1RvU2hvdyIsImNlbnRlck1vZGUiLCJ2YXJpYWJsZVdpZHRoIiwiYXNOYXZGb3IiLCJmYWRlIiwiY29yZVRlYW1JbWFnZVNsaWRlclNsaWNrIiwiZm9jdXNPblNlbGVjdCIsImluaXRUd29TbGlkZXJTbGljayIsInNsaWRlc1RvU2Nyb2xsIiwicmFuZG9tSW5kZXgiLCJsZW4iLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJnZXRfcmFuZG9tIiwiY291bnQiLCJhcnIiLCJsZW5ndGgiLCJpbmRleEhhc2giLCJpbmRleCIsImhhc093blByb3BlcnR5IiwidmlkZW9Jbml0IiwibXlDb250ZW50IiwicGxheWVyIiwidHJpZ2dlckJ1dHRvbnMiLCJjbG9zZUJ1dHRvbiIsInZpbWVvSWQiLCJtb2RhbF9vdmVybGF5IiwiaGlkZU1vZGFsIiwiZXZ0IiwicHJldmVudERlZmF1bHQiLCJ0bXAiLCJyZXBsYWNlV2l0aCIsInBhdXNlVmlkZW8iLCJlIiwibG9hZFZpbWVvVmlkZW8iLCJwb3B1cEVsIiwibG9hZFZpZGVvIiwidGhlbiIsImlkIiwicGxheSIsImNhdGNoIiwidmltZW9fdGl0bGUiLCJtb2RlbEVsIiwibW9kYWxfaWQiLCJjb250YWluZXIiLCJ2aWRlb0ZvciIsInZpZGVvX3R5cGUiLCJZVCIsIlBsYXllciIsInZpZGVvSWQiLCJ2aWRlbyIsInBsYXllclZhcnMiLCJldmVudHMiLCJWaW1lbyIsImxvb3AiLCJ0aXRsZSIsIm5vdCIsImdldEVuZGVkIiwiZW5kZWQiLCJlcnJvciIsIm5leHRFbCIsIm5leHQiLCJuZXh0SW5kZXgiLCJsb2FkSW1hZ2UiLCJ1cmwiLCJhIiwiSW1hZ2UiLCJzcmMiLCJwbGF5QXVkaW8iLCJ2b2x1bWUiLCJwYXVzZSIsInByb2Nlc3NEYXRhIiwiZGV0YWlscyIsInRhZ3MiLCJwcm9kdWN0X3RhZ3MiLCJqb2luIiwiZGVzaWduX3RhZ3MiLCJkZXZlbG9wbWVudF90YWdzIiwiYnVkZ2V0X3RhZ3MiLCJhZGRBamF4TG9hZGVyIiwic2NvcGUiLCJsb2FkZXIiLCJyZW1vdmUiLCJhcHBlbmQiLCJhdHRyIiwicmVtb3ZlQWpheExvYWRlciIsInJlbW92ZUF0dHIiLCJzdWJtaXREYXRhIiwiakZyb20iLCJjYiIsImV4dHJhUGFyYW1zIiwiYnRuIiwiYmFzZVVybCIsImhlYWRlcnMiLCJhcGlLZXkiLCJtZXRob2QiLCJjYWNoZSIsImRhdGFUeXBlIiwic3VjY2VzcyIsInJlcyIsInJlc2V0Iiwic3dhbCIsInRleHQiLCJ0eXBlIiwiY29uZmlybUJ1dHRvblRleHQiLCJhamF4IiwiaW5pdCIsImluaXRFdmVudExpc3RlbmVycyIsInBhZ2VfdHlwZSIsImhvbWUiLCJjcmVhdGVzIiwiY2FzZVN0dWRpZXMiLCJzdHVkaW8iLCJjYXJlZXJzIiwiY2FyZWVyRGV0YWlsIiwiaXNfaG9tZSIsImlzX3N0dWRpbyIsInZpZGVvRWwiLCJwYXJlbnQiLCJjaGVja2VkIiwidHJpZ2dlciIsInNob3ciLCJoaWRlIiwic3VibWl0IiwidGltZXIiLCJzaG93Q29uZmlybUJ1dHRvbiIsInNjcm9sbCIsInRvcCIsInBhZ2VZT2Zmc2V0Iiwid2lkdGgiLCJlbmRQb2ludCIsImhlaWdodCIsIm9mZnNldCIsInZpZXdQb3J0SGVpZ2h0IiwidmlkZW9PdXRWaWV3cG9ydE1heCIsInZhbCIsImVycm9ycyIsImhvdmVyIiwiY2xzIiwiYW5pbWF0ZSIsInNjcm9sbFRvcCIsIm91dGVySGVpZ2h0IiwiaW5pdEZhbmthdmUiLCIkc2xpZGVzIiwiZmFjdHMiLCJmYWN0IiwiYWZ0ZXJMYXN0U2VjdGlvbiIsImFib3V0VXNTb3VuZExvYWQiLCJhZnRlclNvdW5kTG9hZCIsImh0bWwiLCJzdWJfdGV4dCIsInNjcm9sbExlZnQiLCJsZWZ0IiwiZWxzIiwic2Vzc2lvblN0b3JhZ2UiLCJKU09OIiwicGFyc2UiLCJzZXRJdGVtIiwic3RyaW5naWZ5IiwicGxheVByb21pc2UiLCJzbGlja05hdkVsIiwic2xpY2tGb3JFbCIsImxvY2F0aW9uIiwicGhvbmVfbnVtYmVyIiwiY3NzIiwiRm9ybURhdGEiLCJjb250ZW50VHlwZSIsImVuY3R5cGUiLCJsZWFybiIsInNsaWRlciIsImNlbnRlclBhZGRpbmciLCJyZXNwb25zaXZlIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwiY2xpZW50cyIsInNsaWRlVG9nZ2xlIl0sIm1hcHBpbmdzIjoiY0FBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVTtBQUMxQjtBQUNBRixJQUFFRyxFQUFGLENBQUtDLGVBQUwsR0FBdUIsVUFBVUMsT0FBVixFQUFtQjtBQUN4Q0EsY0FBVUMsT0FBT0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JGLE9BQWxCLENBQVY7O0FBRUEsUUFBSUcsT0FBTyxJQUFYO0FBQ0lDLFdBQU8sRUFEWDtBQUVJQyxvQkFBZ0IsRUFGcEI7QUFHSUMsZUFBVztBQUNULGtCQUFZLHVEQURIO0FBRVQsYUFBTyx5QkFGRTtBQUdULGNBQVEsSUFIQztBQUlULGVBQVMsT0FKQTtBQUtULGVBQVMsaUJBTEEsRUFIZjs7OztBQVlBLFNBQUtDLEtBQUwsR0FBYSxVQUFVQyxJQUFWLEVBQWdCQyxHQUFoQixFQUFxQkMsS0FBckIsRUFBNEI7QUFDdkNGLFdBQUtDLEdBQUwsSUFBWUMsS0FBWjtBQUNBLGFBQU9GLElBQVA7QUFDRCxLQUhEOztBQUtBLFNBQUtHLFlBQUwsR0FBb0IsVUFBVUYsR0FBVixFQUFlO0FBQ2pDLFVBQUlKLGNBQWNJLEdBQWQsTUFBdUJHLFNBQTNCLEVBQXNDO0FBQ3BDUCxzQkFBY0ksR0FBZCxJQUFxQixDQUFyQjtBQUNEO0FBQ0QsYUFBT0osY0FBY0ksR0FBZCxHQUFQO0FBQ0QsS0FMRDs7QUFPQVIsV0FBT1ksSUFBUCxDQUFZWixPQUFPLElBQVAsRUFBYWEsY0FBYixFQUFaLEVBQTJDLFlBQVk7O0FBRXJEO0FBQ0EsVUFBSSxDQUFDUixTQUFTUyxRQUFULENBQWtCQyxJQUFsQixDQUF1QixLQUFLQyxJQUE1QixDQUFMLEVBQXdDO0FBQ3RDO0FBQ0Q7O0FBRUQsVUFBSUMsQ0FBSjtBQUNJQyxhQUFPLEtBQUtGLElBQUwsQ0FBVUcsS0FBVixDQUFnQmQsU0FBU0csR0FBekIsQ0FEWDtBQUVJWSxjQUFRLEtBQUtYLEtBRmpCO0FBR0lZLG9CQUFjLEtBQUtMLElBSHZCOztBQUtBLGFBQU8sQ0FBQ0MsSUFBSUMsS0FBS0ksR0FBTCxFQUFMLE1BQXFCWCxTQUE1QixFQUF1Qzs7QUFFckM7QUFDQVUsc0JBQWNBLFlBQVlFLE9BQVosQ0FBb0IsSUFBSUMsTUFBSixDQUFXLFFBQVFQLENBQVIsR0FBWSxNQUF2QixDQUFwQixFQUFvRCxFQUFwRCxDQUFkOztBQUVBO0FBQ0EsWUFBSUEsRUFBRUUsS0FBRixDQUFRZCxTQUFTb0IsSUFBakIsQ0FBSixFQUE0QjtBQUMxQkwsa0JBQVFsQixLQUFLSSxLQUFMLENBQVcsRUFBWCxFQUFlSixLQUFLUSxZQUFMLENBQWtCVyxXQUFsQixDQUFmLEVBQStDRCxLQUEvQyxDQUFSO0FBQ0Q7O0FBRUQ7QUFKQSxhQUtLLElBQUlILEVBQUVFLEtBQUYsQ0FBUWQsU0FBU3FCLEtBQWpCLENBQUosRUFBNkI7QUFDaENOLG9CQUFRbEIsS0FBS0ksS0FBTCxDQUFXLEVBQVgsRUFBZVcsQ0FBZixFQUFrQkcsS0FBbEIsQ0FBUjtBQUNEOztBQUVEO0FBSkssZUFLQSxJQUFJSCxFQUFFRSxLQUFGLENBQVFkLFNBQVNzQixLQUFqQixDQUFKLEVBQTZCO0FBQ2hDUCxzQkFBUWxCLEtBQUtJLEtBQUwsQ0FBVyxFQUFYLEVBQWVXLENBQWYsRUFBa0JHLEtBQWxCLENBQVI7QUFDRDtBQUNGOztBQUVEakIsYUFBT0gsT0FBT0MsTUFBUCxDQUFjLElBQWQsRUFBb0JFLElBQXBCLEVBQTBCaUIsS0FBMUIsQ0FBUDtBQUNELEtBbENEOzs7QUFxQ0EsV0FBT2pCLElBQVA7QUFDRCxHQWpFRDtBQWtFRCxDQXBFRDs7QUFzRUEsSUFBSXlCLFVBQVUsU0FBVkEsT0FBVSxHQUFZLENBQUUsQ0FBNUI7QUFDQUEsUUFBUUMsTUFBUixHQUFpQixVQUFVQyxNQUFWLEVBQWtCO0FBQy9CLE9BQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNBLE9BQUtDLFNBQUwsR0FBaUIsSUFBSUMsS0FBSixFQUFqQjtBQUNILENBSEQ7O0FBS0FKLFFBQVFLLElBQVIsR0FBZSxVQUFVQyxTQUFWLEVBQXFCO0FBQ2hDLE9BQUtDLE1BQUwsR0FBY0QsVUFBVUMsTUFBeEI7QUFDSCxDQUZEOztBQUlBUCxRQUFRUSxPQUFSLEdBQWtCLFVBQVVGLFNBQVYsRUFBcUI7QUFDbkMsT0FBS0MsTUFBTCxHQUFjRCxVQUFVQyxNQUF4QjtBQUNILENBRkQ7O0FBSUFQLFFBQVFTLE9BQVIsR0FBa0IsVUFBVUgsU0FBVixFQUFxQjtBQUNuQyxPQUFLQyxNQUFMLEdBQWNELFVBQVVDLE1BQXhCO0FBQ0gsQ0FGRDs7QUFJQVAsUUFBUVUsWUFBUixHQUF1QixVQUFVSixTQUFWLEVBQXFCO0FBQ3hDLE9BQUtDLE1BQUwsR0FBY0QsVUFBVUMsTUFBeEI7QUFDSCxDQUZEOztBQUlBUCxRQUFRVyxXQUFSLEdBQXNCLFVBQVVMLFNBQVYsRUFBcUI7QUFDdkMsT0FBS0MsTUFBTCxHQUFjRCxVQUFVQyxNQUF4QjtBQUNILENBRkQ7O0FBSUFQLFFBQVFZLE1BQVIsR0FBaUIsVUFBVU4sU0FBVixFQUFxQjtBQUNsQyxPQUFLQyxNQUFMLEdBQWNELFVBQVVDLE1BQXhCO0FBQ0gsQ0FGRDs7QUFJQSxJQUFJTSxRQUFRO0FBQ1ZDLGFBQVcscWxCQUREO0FBRVZDLGFBQVcsMGtCQUZEOztBQUlWQyxlQUFhLHFCQUFVQyxhQUFWLEVBQXlCQyxXQUF6QixFQUFzQ0MsVUFBdEMsRUFBa0Q7QUFDN0QsUUFBR0MsT0FBT0MsRUFBVixFQUFhO0FBQ1hELGFBQU9DLEVBQVAsQ0FBVSxNQUFWLEVBQWtCO0FBQ2hCQyxpQkFBUyxPQURPO0FBRWhCTCx1QkFBZUEsaUJBQWlCLEVBRmhCO0FBR2hCQyxxQkFBYUEsZUFBZSxFQUhaO0FBSWhCQyxvQkFBWUEsY0FBYyxFQUpWLEVBQWxCOztBQU1EO0FBQ0YsR0FiUzs7QUFlVkksbUJBQWlCLHlCQUFVQyxhQUFWLEVBQXlCO0FBQ3hDLFFBQUdKLE9BQU9DLEVBQVYsRUFBYTtBQUNYRCxhQUFPQyxFQUFQLENBQVUsTUFBVixFQUFrQixXQUFsQixFQUErQjtBQUM3QkMsaUJBQVMsT0FEb0I7QUFFN0IseUJBQWlCRSxhQUZZO0FBRzdCLG1CQUFXLEtBSGtCLEVBQS9COztBQUtEO0FBQ0YsR0F2QlMsRUFBWjs7O0FBMEJBeEIsUUFBUUMsTUFBUixDQUFld0IsU0FBZixHQUEyQjtBQUN2QkMsa0JBQWdCLDBCQUFZO0FBQzFCNUQsTUFBRSxTQUFGLEVBQWFrQixJQUFiLENBQWtCLFlBQVk7QUFDNUIsVUFBSTJDLFNBQVM7QUFDVEMsY0FBTSxLQURHO0FBRVRDLGtCQUFVLEtBRkQ7QUFHVEMsbUJBQVcsS0FIRjtBQUlUQyxlQUFPLEtBSkU7QUFLVEMsZ0JBQVEsS0FMQztBQU1UbEIsbUJBQVdELE1BQU1DLFNBTlI7QUFPVEMsbUJBQVdGLE1BQU1FLFNBUFI7QUFRVGtCLGVBQU8sSUFSRSxDQVFFO0FBQ2I7QUFDQTtBQVZXLE9BQWI7QUFZQW5FLFFBQUVPLE1BQUYsQ0FBU3NELE1BQVQsRUFBa0I3RCxFQUFFLElBQUYsRUFBUW9FLElBQVIsQ0FBYSxPQUFiLEtBQXlCLEVBQTNDO0FBQ0FwRSxRQUFFLElBQUYsRUFBUXFFLEtBQVIsQ0FBY1IsTUFBZDtBQUNBO0FBQ0E3RCxRQUFFLDRCQUFGLEVBQWdDc0UsS0FBaEMsQ0FBc0MsWUFBVztBQUM3Q3RFLFVBQUUsNEJBQUYsRUFBZ0N1RSxXQUFoQyxDQUE0QyxRQUE1QztBQUNBdkUsVUFBRSxJQUFGLEVBQVF3RSxRQUFSLENBQWlCLFFBQWpCO0FBQ0F4RSxVQUFFLHVCQUFGLEVBQTJCd0UsUUFBM0IsQ0FBb0MsYUFBcEM7QUFDQXhFLFVBQUUsdUJBQUYsRUFBMkJ1RSxXQUEzQixDQUF1QyxhQUF2QztBQUNBdkUsVUFBRSxTQUFGLEVBQWFxRSxLQUFiLENBQW1CLFdBQW5CLEVBQWdDSSxTQUFTLEdBQVQsQ0FBaEM7QUFDSCxPQU5EO0FBT0F6RSxRQUFFLDRCQUFGLEVBQWdDc0UsS0FBaEMsQ0FBc0MsWUFBVztBQUM3Q3RFLFVBQUUsNEJBQUYsRUFBZ0N1RSxXQUFoQyxDQUE0QyxRQUE1QztBQUNBdkUsVUFBRSxJQUFGLEVBQVF3RSxRQUFSLENBQWlCLFFBQWpCO0FBQ0F4RSxVQUFFLHVCQUFGLEVBQTJCd0UsUUFBM0IsQ0FBb0MsYUFBcEM7QUFDQXhFLFVBQUUsdUJBQUYsRUFBMkJ1RSxXQUEzQixDQUF1QyxhQUF2QztBQUNBdkUsVUFBRSxTQUFGLEVBQWFxRSxLQUFiLENBQW1CLFdBQW5CLEVBQWdDSSxTQUFTLEdBQVQsQ0FBaEM7QUFDSCxPQU5EO0FBT0QsS0E5QkQ7QUErQkQsR0FqQ3NCOzs7O0FBcUN2QkMsa0JBQWdCLDBCQUFZO0FBQzFCMUUsTUFBRSxtQkFBRixFQUF1QnFFLEtBQXZCLENBQTZCO0FBQzNCUCxZQUFNLElBRHFCO0FBRTNCSSxjQUFRLEtBRm1CO0FBRzNCakIsaUJBQVcsZ2tCQUhnQjtBQUkzQmtCLGFBQU8sSUFKb0IsRUFBN0I7OztBQU9BbkUsTUFBRSx1Q0FBRixFQUEyQ3NFLEtBQTNDLENBQWlELFlBQVk7QUFDekR2QixZQUFNRyxXQUFOLENBQWtCLGNBQWxCLEVBQWtDLE9BQWxDLEVBQTJDLGdDQUEzQztBQUNBbEQsUUFBRSxtQkFBRixFQUF1QnFFLEtBQXZCLENBQTZCLFdBQTdCLEVBQTBDckUsRUFBRSxJQUFGLEVBQVFvRSxJQUFSLENBQWEsT0FBYixDQUExQztBQUNILEtBSEQ7O0FBS0FwRSxNQUFFLG1CQUFGLEVBQXVCMkUsRUFBdkIsQ0FBMEIsY0FBMUIsRUFBMEMsVUFBU0MsS0FBVCxFQUFnQlAsS0FBaEIsRUFBdUJRLFlBQXZCLEVBQXFDQyxTQUFyQyxFQUErQztBQUN2RixVQUFJQyxLQUFLL0UsRUFBRSxJQUFGLENBQVQ7QUFDSWdGLHNCQUFnQkQsR0FBR0UsT0FBSCxDQUFXLHVCQUFYLENBRHBCO0FBRUlDLDBCQUFvQkYsY0FBY0csSUFBZCxDQUFtQixzQkFBbkIsQ0FGeEI7QUFHSUMsaUJBQVdGLGtCQUFrQkMsSUFBbEIsQ0FBdUIsU0FBdkIsQ0FIZjtBQUlJRSxjQUFRSCxrQkFBa0JDLElBQWxCLENBQXVCLElBQXZCLENBSlo7O0FBTUFDLGVBQVNiLFdBQVQsQ0FBcUIsUUFBckI7QUFDQWMsWUFBTUMsRUFBTixDQUFTUixTQUFULEVBQW9CTixRQUFwQixDQUE2QixRQUE3QjtBQUNELEtBVEQ7QUFVRCxHQTVEc0I7O0FBOER2QmUsd0JBQXNCLGdDQUFZO0FBQ2hDdkYsTUFBRSxvQkFBRixFQUF3QnFFLEtBQXhCLENBQThCO0FBQzVCTixnQkFBVSxJQURrQjtBQUU1QnlCLHFCQUFlLElBRmE7QUFHNUJ0QixjQUFRLEtBSG9CO0FBSTVCdUIsZ0JBQVUsSUFKa0I7QUFLNUJDLG9CQUFjLENBTGM7QUFNNUJDLGtCQUFZLElBTmdCO0FBTzVCQyxxQkFBZSxJQVBhO0FBUTVCQyxnQkFBVSxtQkFSa0IsRUFBOUI7OztBQVdBN0YsTUFBRSxtQkFBRixFQUF1QnFFLEtBQXZCLENBQTZCO0FBQzNCUCxZQUFNLEtBRHFCO0FBRTNCQyxnQkFBVSxJQUZpQjtBQUczQnlCLHFCQUFlLElBSFk7QUFJM0J0QixjQUFRLElBSm1CO0FBSzNCMkIsZ0JBQVUsb0JBTGlCO0FBTTNCN0MsaUJBQVdELE1BQU1DLFNBTlU7QUFPM0JDLGlCQUFXRixNQUFNRSxTQVBVO0FBUTNCNkMsWUFBTSxJQVJxQixFQUE3Qjs7QUFVRCxHQXBGc0I7O0FBc0Z2QkMsNEJBQTBCLG9DQUFZO0FBQ3BDL0YsTUFBRSx5QkFBRixFQUE2QnFFLEtBQTdCLENBQW1DO0FBQ2pDSCxjQUFRLEtBRHlCO0FBRWpDd0Isb0JBQWMsQ0FGbUI7QUFHakNNLHFCQUFlLElBSGtCO0FBSWpDSCxnQkFBVSx3QkFKdUIsRUFBbkM7OztBQU9BN0YsTUFBRSx3QkFBRixFQUE0QnFFLEtBQTVCLENBQWtDO0FBQ2hDTixnQkFBVSxJQURzQjtBQUVoQ3lCLHFCQUFlLElBRmlCO0FBR2hDdEIsY0FBUSxLQUh3QjtBQUloQzJCLGdCQUFVLHlCQUpzQjtBQUtoQ0MsWUFBTSxJQUwwQixFQUFsQzs7QUFPRCxHQXJHc0I7O0FBdUd2Qkcsc0JBQW9CLDhCQUFZO0FBQzlCakcsTUFBRSxhQUFGLEVBQWlCcUUsS0FBakIsQ0FBdUI7QUFDckJxQixvQkFBYyxDQURPO0FBRXJCUSxzQkFBZ0IsQ0FGSztBQUdyQm5DLGdCQUFVLElBSFc7QUFJckJ5QixxQkFBZSxJQUpNO0FBS3JCdEIsY0FBUSxLQUxhO0FBTXJCNEIsWUFBTSxJQU5lO0FBT3JCRCxnQkFBVSxhQVBXLEVBQXZCOzs7QUFVQTdGLE1BQUUsYUFBRixFQUFpQnFFLEtBQWpCLENBQXVCO0FBQ3JCcUIsb0JBQWMsQ0FETztBQUVyQlEsc0JBQWdCLENBRks7QUFHckJoQyxjQUFRLEtBSGE7QUFJckIyQixnQkFBVSxhQUpXO0FBS3JCRixrQkFBWSxJQUxTO0FBTXJCSyxxQkFBZSxJQU5NLEVBQXZCOztBQVFELEdBMUhzQjs7QUE0SHZCRyxlQUFhLHFCQUFTQyxHQUFULEVBQWE7QUFDeEIsV0FBT0MsS0FBS0MsS0FBTCxDQUFXRCxLQUFLRSxNQUFMLEtBQWdCSCxHQUEzQixDQUFQO0FBQ0QsR0E5SHNCOztBQWdJdkJJLGNBQVksb0JBQVNwQyxJQUFULEVBQWVxQyxLQUFmLEVBQXFCO0FBQy9CLFFBQUlqRyxPQUFPLElBQVg7QUFDQSxRQUFHaUcsU0FBU0EsU0FBUyxDQUFyQixFQUF3QjtBQUN0QixVQUFJQyxNQUFNLEVBQVY7QUFDSU4sWUFBTWhDLEtBQUt1QyxNQURmO0FBRUlDLGtCQUFZLEVBRmhCO0FBR0EsVUFBR0gsU0FBU0wsR0FBWixFQUFnQjtBQUNkLGVBQU9oQyxJQUFQO0FBQ0Q7QUFDRCxhQUFNLENBQU4sRUFBUTtBQUNOLFlBQUl5QyxRQUFRckcsS0FBSzJGLFdBQUwsQ0FBaUJDLEdBQWpCLENBQVo7QUFDQSxZQUFHTSxJQUFJQyxNQUFKLElBQWNGLEtBQWpCLEVBQXVCO0FBQ3JCO0FBQ0Q7QUFDRCxZQUFHLENBQUNHLFVBQVVFLGNBQVYsQ0FBeUJELEtBQXpCLENBQUQsSUFBb0NBLFFBQVFULEdBQS9DLEVBQW1EO0FBQ2pEUSxvQkFBVUMsS0FBVixJQUFtQixJQUFuQjtBQUNBSCxjQUFJM0UsSUFBSixDQUFTcUMsS0FBS3lDLEtBQUwsQ0FBVDtBQUNEO0FBQ0Y7QUFDRCxhQUFPSCxHQUFQO0FBQ0QsS0FsQkQsTUFrQk07QUFDSixhQUFPdEMsS0FBSzVELEtBQUsyRixXQUFMLENBQWlCL0IsS0FBS3VDLE1BQXRCLENBQUwsS0FBdUN2QyxLQUFLLENBQUwsQ0FBOUM7QUFDRDtBQUNGLEdBdkpzQjs7QUF5SnZCMkMsYUFBVyxxQkFBWTtBQUNyQjtBQUNBLFFBQUlDLFlBQVloSCxFQUFFLGdCQUFGLENBQWhCO0FBQ0lpSCxVQURKO0FBRUlDLHFCQUFpQmxILEVBQUUsa0NBQUYsQ0FGckI7QUFHSW1ILGtCQUFjbkgsRUFBRSxlQUFGLENBSGxCO0FBSUlvSCxXQUpKO0FBS0lDLG9CQUFnQnJILEVBQUUsZ0JBQUYsQ0FMcEI7O0FBT0EsYUFBU3NILFNBQVQsQ0FBbUJDLEdBQW5CLEVBQXVCO0FBQ3JCQSxVQUFJQyxjQUFKO0FBQ0F4SCxRQUFFLE1BQUYsRUFBVXVFLFdBQVYsQ0FBc0IsY0FBdEI7QUFDQThDLG9CQUFjOUMsV0FBZCxDQUEwQixRQUExQjtBQUNBeEIsWUFBTUcsV0FBTixDQUFrQixhQUFsQixFQUFpQyxPQUFqQyxFQUEwQyxvQkFBMUM7QUFDQSxVQUFJdUUsTUFBTXpILEVBQUUsc0NBQUYsQ0FBVjtBQUNBZ0g7QUFDS3pDLGlCQURMLENBQ2lCLFlBRGpCO0FBRUtZLFVBRkwsQ0FFVSxtQkFGVixFQUUrQixDQUYvQixFQUVrQ3VDLFdBRmxDLENBRThDRCxJQUFJLENBQUosQ0FGOUM7QUFHQSxVQUFJO0FBQ0ZSLGVBQU9VLFVBQVA7QUFDRCxPQUZELENBRUMsT0FBTUMsQ0FBTixFQUFRLENBQUU7QUFDWFgsZUFBUyxJQUFUO0FBQ0Q7O0FBRURJLGtCQUFjL0MsS0FBZCxDQUFvQmdELFNBQXBCO0FBQ0F0SCxNQUFFbUgsV0FBRixFQUFlN0MsS0FBZixDQUFxQmdELFNBQXJCOztBQUVBLGFBQVNPLGNBQVQsQ0FBeUJULE9BQXpCLEVBQWtDO0FBQ2hDLFVBQUlyQyxFQUFKO0FBQ0krQyxnQkFBVTlILEVBQUUsc0JBQUYsQ0FEZDtBQUVJb0UsVUFGSjtBQUdBcEUsUUFBRSwyQkFBRixFQUErQnVFLFdBQS9CLENBQTJDLFFBQTNDO0FBQ0EwQyxhQUFPYyxTQUFQLENBQWlCWCxPQUFqQixFQUEwQlksSUFBMUIsQ0FBK0IsVUFBU0MsRUFBVCxFQUFhO0FBQzFDaEIsZUFBT2lCLElBQVAsR0FBY0MsS0FBZDtBQUNBcEQsYUFBSy9FLEVBQUUsdUNBQXFDaUksRUFBckMsR0FBd0MsSUFBMUMsQ0FBTDtBQUNBN0QsZUFBT1csR0FBR1gsSUFBSCxFQUFQO0FBQ0FXLFdBQUdQLFFBQUgsQ0FBWSxRQUFaO0FBQ0F6QixjQUFNRyxXQUFOLENBQWtCLG9CQUFsQixFQUF3QyxPQUF4QyxFQUFpRGtCLEtBQUtnRSxXQUF0RDtBQUNBO0FBQ0E7QUFDRCxPQVJEO0FBU0Q7O0FBRURwSSxNQUFFLHlDQUFGLEVBQTZDc0UsS0FBN0MsQ0FBbUQsWUFBWTtBQUMzRDhDLGdCQUFVcEgsRUFBRSxJQUFGLEVBQVFvRSxJQUFSLENBQWEsU0FBYixDQUFWO0FBQ0F5RCxxQkFBZVQsT0FBZjtBQUNILEtBSEQ7O0FBS0FGLG1CQUFlNUMsS0FBZixDQUFxQixVQUFTc0QsQ0FBVCxFQUFZO0FBQy9CQSxRQUFFSixjQUFGO0FBQ0EsVUFBSXpDLEtBQUsvRSxFQUFFLElBQUYsQ0FBVDtBQUNJb0UsYUFBT1csR0FBR1gsSUFBSCxFQURYO0FBRUlpRSxnQkFBVXJJLEVBQUVvRSxLQUFLa0UsUUFBUCxDQUZkO0FBR0lDLGtCQUFZRixRQUFRbEQsSUFBUixDQUFhLG1CQUFiLEVBQWtDLENBQWxDLENBSGhCO0FBSUFwQyxZQUFNRyxXQUFOLENBQWtCLGFBQWxCLEVBQWlDLE9BQWpDLEVBQTBDa0IsS0FBS29FLFFBQUwsR0FBZ0IsYUFBMUQ7QUFDQXhJLFFBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQixjQUFuQjtBQUNBNkMsb0JBQWM3QyxRQUFkLENBQXVCLFFBQXZCO0FBQ0E2RCxjQUFRN0QsUUFBUixDQUFpQixZQUFqQjtBQUNBLFVBQUdKLEtBQUtxRSxVQUFMLElBQW1CLFNBQXRCLEVBQWlDO0FBQy9CeEIsaUJBQVMsSUFBSXlCLEdBQUdDLE1BQVAsQ0FBY0osU0FBZCxFQUF5QjtBQUNoQ0ssbUJBQVN4RSxLQUFLeUUsS0FEa0I7QUFFaENDLHNCQUFZLEVBQUMsWUFBWSxDQUFiLEVBQWdCLFlBQVksQ0FBNUIsRUFGb0I7QUFHaENDLGtCQUFRO0FBQ04sdUJBQVcsaUJBQVVuRSxLQUFWLEVBQWlCO0FBQzFCN0Isb0JBQU1HLFdBQU4sQ0FBa0IsY0FBbEIsRUFBa0MsT0FBbEMsRUFBMkNrQixLQUFLb0UsUUFBaEQ7QUFDRCxhQUhLLEVBSHdCLEVBQXpCLENBQVQ7OztBQVNELE9BVkQsTUFVTSxJQUFHcEUsS0FBS3FFLFVBQUwsSUFBbUIsT0FBdEIsRUFBOEI7QUFDbEN4QixpQkFBUyxJQUFJK0IsTUFBTUwsTUFBVixDQUFpQkosU0FBakIsRUFBNEI7QUFDbkNOLGNBQUk3RCxLQUFLeUUsS0FEMEI7QUFFbkNJLGdCQUFNLEtBRjZCO0FBR25DQyxpQkFBTyxLQUg0QjtBQUluQ25GLG9CQUFVLElBSnlCLEVBQTVCLENBQVQ7O0FBTUQsT0FQSztBQVFELFVBQUdLLEtBQUtxRSxVQUFMLElBQW1CLGFBQXRCLEVBQW9DO0FBQ3ZDSixnQkFBUWxELElBQVIsQ0FBYSxTQUFiLEVBQXdCZCxLQUF4QixDQUE4QixXQUE5QixFQUEyQyxDQUEzQztBQUNBK0Msa0JBQVVpQixRQUFRbEQsSUFBUixDQUFhLG9CQUFiLEVBQW1DZ0UsR0FBbkMsQ0FBdUMsZUFBdkMsRUFBd0Q3RCxFQUF4RCxDQUEyRCxDQUEzRCxFQUE4RGxCLElBQTlELENBQW1FLFNBQW5FLENBQVY7QUFDQTZDLGlCQUFTLElBQUkrQixNQUFNTCxNQUFWLENBQWlCSixTQUFqQixFQUE0QjtBQUNuQ04sY0FBSWIsT0FEK0I7QUFFbkM2QixnQkFBTSxLQUY2QjtBQUduQ0MsaUJBQU8sS0FINEIsRUFBNUIsQ0FBVDs7QUFLQWpDLGVBQU9tQyxRQUFQLEdBQWtCcEIsSUFBbEIsQ0FBdUIsVUFBU3FCLEtBQVQsRUFBZ0I7QUFDdEMsU0FERCxFQUNHbEIsS0FESCxDQUNTLFVBQVNtQixLQUFULEVBQWdCO0FBQ3hCLFNBRkQ7QUFHQXJDLGVBQU90QyxFQUFQLENBQVUsT0FBVixFQUFtQixZQUFZO0FBQzNCLGNBQUc7QUFDRCxnQkFBSTRFLFNBQVN2SixFQUFFLHVDQUFxQ29ILE9BQXJDLEdBQTZDLElBQS9DLEVBQXFEK0IsR0FBckQsQ0FBeUQsZUFBekQsRUFBMEVLLElBQTFFLEVBQWI7QUFDSUMsd0JBQVlGLE9BQU9uRixJQUFQLENBQVksT0FBWixDQURoQjtBQUVBaUUsb0JBQVFsRCxJQUFSLENBQWEsU0FBYixFQUF3QmQsS0FBeEIsQ0FBOEIsV0FBOUIsRUFBMkNvRixTQUEzQztBQUNBckMsc0JBQVVtQyxPQUFPbkYsSUFBUCxDQUFZLFNBQVosQ0FBVjtBQUNBeUQsMkJBQWVULE9BQWY7QUFDRCxXQU5ELENBTUMsT0FBTVEsQ0FBTixFQUFRLENBQUU7QUFDZCxTQVJEO0FBU0FDLHVCQUFlVCxPQUFmO0FBQ0Q7QUFDRixLQWxERDtBQW1ERCxHQTVQc0I7O0FBOFB2QnNDLGFBQVcsbUJBQVVDLEdBQVYsRUFBZTtBQUN0QixRQUFJQyxJQUFJLElBQUlDLEtBQUosRUFBUjtBQUNBRCxNQUFFRSxHQUFGLEdBQVFILEdBQVI7QUFDSCxHQWpRc0I7O0FBbVF2QkksYUFBVyxtQkFBVUQsR0FBVixFQUFlRSxNQUFmLEVBQXVCO0FBQzlCLFNBQUszSCxTQUFMLENBQWU0SCxLQUFmO0FBQ0EsU0FBSzVILFNBQUwsQ0FBZXlILEdBQWYsR0FBcUJBLEdBQXJCO0FBQ0EsUUFBR0UsTUFBSCxFQUFXO0FBQ1QsV0FBSzNILFNBQUwsQ0FBZTJILE1BQWYsR0FBd0JBLE1BQXhCO0FBQ0Q7QUFDRCxXQUFPLEtBQUszSCxTQUFMLENBQWU2RixJQUFmLEdBQXNCQyxLQUF0QixFQUFQO0FBQ0gsR0ExUXNCOztBQTRRdkIrQixlQUFhLHFCQUFVOUYsSUFBVixFQUFnQjtBQUN6QixRQUFJK0YsVUFBVSxFQUFkO0FBQ0lDLFdBQU9oRyxLQUFLZ0csSUFEaEI7QUFFQSxRQUFHaEcsS0FBSzBDLGNBQUwsQ0FBb0IsTUFBcEIsQ0FBSCxFQUErQjtBQUM3QnFELGlCQUFXLGFBQVg7QUFDQSxVQUFHQyxLQUFLdEQsY0FBTCxDQUFvQixjQUFwQixDQUFILEVBQXVDO0FBQ3JDcUQsbUJBQVcsZUFBZUMsS0FBS0MsWUFBTCxDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBZixHQUE4QyxHQUF6RDtBQUNEO0FBQ0QsVUFBR0YsS0FBS3RELGNBQUwsQ0FBb0IsYUFBcEIsQ0FBSCxFQUFzQztBQUNwQ3FELG1CQUFXLGNBQWNDLEtBQUtHLFdBQUwsQ0FBaUJELElBQWpCLENBQXNCLElBQXRCLENBQWQsR0FBNEMsR0FBdkQ7QUFDRDtBQUNELFVBQUdGLEtBQUt0RCxjQUFMLENBQW9CLGtCQUFwQixDQUFILEVBQTJDO0FBQ3pDcUQsbUJBQVcsbUJBQW1CQyxLQUFLSSxnQkFBTCxDQUFzQkYsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FBbkIsR0FBc0QsR0FBakU7QUFDRDtBQUNELFVBQUdGLEtBQUt0RCxjQUFMLENBQW9CLGFBQXBCLENBQUgsRUFBc0M7QUFDcENxRCxtQkFBVyxjQUFjQyxLQUFLSyxXQUFMLENBQWlCSCxJQUFqQixDQUFzQixJQUF0QixDQUFkLEdBQTRDLEdBQXZEO0FBQ0Q7QUFDRixLQWRELE1BY007QUFDSkgsZ0JBQVUsMkJBQVY7QUFDRDtBQUNEL0YsU0FBSytGLE9BQUwsR0FBZUEsT0FBZjtBQUNBLFdBQU8vRixLQUFLZ0csSUFBWjtBQUNBLFdBQU9oRyxJQUFQO0FBQ0gsR0FuU3NCOztBQXFTdkJzRyxpQkFBZSx1QkFBU0MsS0FBVCxFQUFlO0FBQzVCLFFBQUlDLFNBQVMsbUNBQWI7QUFDQSxRQUFHRCxNQUFNeEYsSUFBTixDQUFXLGNBQVgsQ0FBSCxFQUE4QjtBQUM1QndGLFlBQU14RixJQUFOLENBQVcsY0FBWCxFQUEyQjBGLE1BQTNCO0FBQ0Q7QUFDREYsVUFBTW5HLFFBQU4sQ0FBZSxVQUFmO0FBQ0FtRyxVQUFNRyxNQUFOLENBQWFGLE1BQWI7QUFDQUQsVUFBTUksSUFBTixDQUFXLFVBQVgsRUFBdUIsVUFBdkI7QUFDRCxHQTdTc0I7O0FBK1N2QkMsb0JBQWtCLDBCQUFTTCxLQUFULEVBQWU7QUFDL0JBLFVBQU1wRyxXQUFOLENBQWtCLFVBQWxCO0FBQ0FvRyxVQUFNeEYsSUFBTixDQUFXLGNBQVgsRUFBMkIwRixNQUEzQjtBQUNBRixVQUFNTSxVQUFOLENBQWlCLFVBQWpCO0FBQ0QsR0FuVHNCOztBQXFUdkJDLGNBQVksb0JBQVV2QixHQUFWLEVBQWV3QixLQUFmLEVBQXNCL0csSUFBdEIsRUFBNEJnSCxFQUE1QixFQUFnQ0MsV0FBaEMsRUFBNkM7QUFDdkQsUUFBSTdLLE9BQU8sSUFBWDtBQUNJNEIsYUFBUzVCLEtBQUs0QixNQURsQjtBQUVJeUIsVUFGSjtBQUdJeUgsVUFBTUgsTUFBTWhHLElBQU4sQ0FBVyx1QkFBWCxDQUhWO0FBSUEzRSxTQUFLa0ssYUFBTCxDQUFtQlksR0FBbkI7O0FBRUF6SCxhQUFTO0FBQ1A4RixXQUFLdkgsT0FBT21KLE9BQVAsR0FBaUI1QixHQURmO0FBRVA2QixlQUFTO0FBQ1AseUJBQWlCLGlCQUFpQnBKLE9BQU9xSixNQURsQyxFQUZGOztBQUtQQyxjQUFRLE1BTEQ7QUFNUEMsYUFBTyxLQU5BO0FBT1BDLGdCQUFVLE1BUEg7QUFRUHhILFlBQU1BLElBUkM7QUFTUHlILGVBQVMsaUJBQVVDLEdBQVYsRUFBZTtBQUN0QnRMLGFBQUt3SyxnQkFBTCxDQUFzQk0sR0FBdEI7QUFDQSxZQUFHUSxJQUFJRCxPQUFQLEVBQWdCO0FBQ2RWLGdCQUFNLENBQU4sRUFBU1ksS0FBVDtBQUNEO0FBQ0QsWUFBR1gsRUFBSCxFQUFPO0FBQ0xBLGFBQUdVLEdBQUg7QUFDRDtBQUNGLE9BakJNO0FBa0JQeEMsYUFBTyxpQkFBWTtBQUNqQjlJLGFBQUt3SyxnQkFBTCxDQUFzQk0sR0FBdEI7QUFDQXZJLGNBQU1VLGVBQU4sQ0FBc0Isc0JBQXFCa0csR0FBM0M7QUFDQXFDLGFBQUssRUFBQzlDLE9BQU8sT0FBUixFQUFtQitDLE1BQU0seUNBQXpCLEVBQXNFQyxNQUFNLE9BQTVFLEVBQXVGQyxtQkFBbUIsSUFBMUcsRUFBTDtBQUNELE9BdEJNLEVBQVQ7O0FBd0JBbk0sTUFBRU8sTUFBRixDQUFTc0QsTUFBVCxFQUFpQndILGVBQWUsRUFBaEM7QUFDQXJMLE1BQUVvTSxJQUFGLENBQU92SSxNQUFQO0FBQ0QsR0F0VnNCLEVBQTNCOzs7QUF5VkEzQixRQUFReUIsU0FBUixHQUFvQjtBQUNsQjBJLFFBQU0sY0FBVWpLLE1BQVYsRUFBa0I7QUFDdEIsUUFBSTVCLE9BQU8sSUFBWDtBQUNBQSxTQUFLaUMsTUFBTCxHQUFjLElBQUlQLFFBQVFDLE1BQVosQ0FBbUJDLE1BQW5CLENBQWQ7QUFDQTVCLFNBQUtpQyxNQUFMLENBQVltQixjQUFaO0FBQ0FwRCxTQUFLOEwsa0JBQUwsQ0FBd0JsSyxNQUF4Qjs7QUFFQSxZQUFRQSxPQUFPbUssU0FBZjtBQUNFLFdBQUssTUFBTDtBQUNFL0wsYUFBS2dNLElBQUwsR0FBWSxJQUFJdEssUUFBUUssSUFBWixDQUFpQi9CLElBQWpCLENBQVo7QUFDQUEsYUFBS2dNLElBQUwsQ0FBVUgsSUFBVjtBQUNBOztBQUVGLFdBQUssU0FBTDtBQUNFN0wsYUFBS2lNLE9BQUwsR0FBZSxJQUFJdkssUUFBUVEsT0FBWixDQUFvQmxDLElBQXBCLENBQWY7QUFDQUEsYUFBS2lNLE9BQUwsQ0FBYUosSUFBYjtBQUNBOztBQUVGLFdBQUssY0FBTDtBQUNFN0wsYUFBS2tNLFdBQUwsR0FBbUIsSUFBSXhLLFFBQVFXLFdBQVosQ0FBd0JyQyxJQUF4QixDQUFuQjtBQUNBQSxhQUFLa00sV0FBTCxDQUFpQkwsSUFBakI7QUFDQTs7QUFFRixXQUFLLFFBQUw7QUFDRTdMLGFBQUttTSxNQUFMLEdBQWMsSUFBSXpLLFFBQVFZLE1BQVosQ0FBbUJ0QyxJQUFuQixDQUFkO0FBQ0FBLGFBQUttTSxNQUFMLENBQVlOLElBQVo7QUFDQTs7QUFFRixXQUFLLFNBQUw7QUFDRTdMLGFBQUtvTSxPQUFMLEdBQWUsSUFBSTFLLFFBQVFTLE9BQVosQ0FBb0JuQyxJQUFwQixDQUFmO0FBQ0FBLGFBQUtvTSxPQUFMLENBQWFQLElBQWI7QUFDQTs7QUFFRixXQUFLLGVBQUw7QUFDRTdMLGFBQUtxTSxZQUFMLEdBQW9CLElBQUkzSyxRQUFRVSxZQUFaLENBQXlCcEMsSUFBekIsQ0FBcEI7QUFDQUEsYUFBS3FNLFlBQUwsQ0FBa0JSLElBQWxCO0FBQ0EsY0E3Qko7OztBQWdDRCxHQXZDaUI7O0FBeUNsQkMsc0JBQW9CLDRCQUFVbEssTUFBVixFQUFrQjtBQUNwQyxRQUFJNUIsT0FBTyxJQUFYO0FBQ0lzTSxjQUFVMUssT0FBT21LLFNBQVAsSUFBb0IsTUFEbEM7QUFFSVEsZ0JBQVkzSyxPQUFPbUssU0FBUCxJQUFvQixRQUZwQzs7QUFJQSxRQUFHTyxPQUFILEVBQVk7QUFDVixVQUFJRSxVQUFVaE4sRUFBRSxXQUFGLENBQWQ7QUFDQSxVQUFJZ04sUUFBUXJHLE1BQVosRUFBb0I7QUFDaEJxRyxnQkFBUSxDQUFSLEVBQVc5RSxJQUFYLEdBQWtCQyxLQUFsQjtBQUNIO0FBQ0Y7O0FBRURuSSxNQUFFQyxRQUFGLEVBQVkwRSxFQUFaLENBQWUsUUFBZixFQUF5QixtQ0FBekIsRUFBOEQsWUFBWTtBQUN4RSxVQUFJa0MsUUFBUTdHLEVBQUUsSUFBRixFQUFRaU4sTUFBUixHQUFpQnBHLEtBQWpCLEVBQVo7QUFDSTlCLFdBQUsvRSxFQUFFLHlDQUFGLEVBQTZDc0YsRUFBN0MsQ0FBZ0R1QixLQUFoRCxDQURUOztBQUdBN0csUUFBRSx5Q0FBRixFQUE2Q3dFLFFBQTdDLENBQXNELFFBQXRELEVBQWdFdUcsSUFBaEUsQ0FBcUUsVUFBckUsRUFBaUYsVUFBakY7QUFDQWhHLFNBQUdSLFdBQUgsQ0FBZSxRQUFmLEVBQXlCMEcsVUFBekIsQ0FBb0MsVUFBcEM7QUFDQWxHLFNBQUdJLElBQUgsQ0FBUSxxQkFBUixFQUErQixDQUEvQixFQUFrQytILE9BQWxDLEdBQTRDLElBQTVDO0FBQ0QsS0FQRDs7QUFTQWxOLE1BQUVDLFFBQUYsRUFBWTBFLEVBQVosQ0FBZSxRQUFmLEVBQXlCLDJDQUF6QixFQUFzRSxZQUFZO0FBQ2hGM0UsUUFBRSxnQ0FBRixFQUFvQ3dFLFFBQXBDLENBQTZDLFFBQTdDLEVBQXVEdUcsSUFBdkQsQ0FBNEQsVUFBNUQsRUFBd0UsVUFBeEU7QUFDRCxLQUZEOztBQUlBL0ssTUFBRUMsUUFBRixFQUFZMEUsRUFBWixDQUFlLE9BQWYsRUFBd0Isc0JBQXhCLEVBQWdELFlBQVk7QUFDMUQsVUFBSUksS0FBSy9FLEVBQUUsbUNBQUYsQ0FBVDtBQUNBQSxRQUFFLGdDQUFGLEVBQW9DdUUsV0FBcEMsQ0FBZ0QsUUFBaEQsRUFBMEQwRyxVQUExRCxDQUFxRSxVQUFyRTtBQUNBakwsUUFBRSw4QkFBRixFQUFrQ2lMLFVBQWxDLENBQTZDLFNBQTdDO0FBQ0FsRyxTQUFHTyxFQUFILENBQU0sQ0FBTixFQUFTLENBQVQsRUFBWTRILE9BQVosR0FBc0IsSUFBdEI7QUFDQW5JLFNBQUdPLEVBQUgsQ0FBTSxDQUFOLEVBQVM2SCxPQUFULENBQWlCLFFBQWpCO0FBQ0QsS0FORDs7QUFRQW5OLE1BQUUsaUNBQUYsRUFBcUMyRSxFQUFyQyxDQUF3QyxPQUF4QyxFQUFpRCxVQUFVaUQsQ0FBVixFQUFhO0FBQzVEQSxRQUFFSixjQUFGO0FBQ0F6RSxZQUFNRyxXQUFOLENBQWtCLE1BQWxCLEVBQTBCLE9BQTFCLEVBQW1DLG9CQUFuQztBQUNBbEQsUUFBRSxvQkFBRixFQUF3QnVFLFdBQXhCLENBQW9DLFFBQXBDLEVBQThDNkksSUFBOUM7QUFDQXBOLFFBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQixXQUFuQjtBQUNBeEUsUUFBRSxRQUFGLEVBQVkyRSxFQUFaLENBQWUsT0FBZixFQUF3QixZQUFZO0FBQ2xDM0UsVUFBRSxvQkFBRixFQUF3QndFLFFBQXhCLENBQWlDLFFBQWpDLEVBQTJDNkksSUFBM0M7QUFDQXJOLFVBQUUsTUFBRixFQUFVdUUsV0FBVixDQUFzQixXQUF0QjtBQUNELE9BSEQ7QUFJRCxLQVREOztBQVdBdkUsTUFBRSxtQkFBRixFQUF1QnNOLE1BQXZCLENBQThCLFVBQVUxRixDQUFWLEVBQWE7QUFDekNBLFFBQUVKLGNBQUY7QUFDQSxVQUFJMkQsUUFBUW5MLEVBQUUsSUFBRixDQUFaO0FBQ0lvRSxhQUFPK0csTUFBTS9LLGVBQU4sRUFEWDs7QUFHQTJDLFlBQU1HLFdBQU4sQ0FBa0IsWUFBbEIsRUFBZ0MsT0FBaEMsRUFBeUMseUJBQXpDO0FBQ0ExQyxXQUFLaUMsTUFBTCxDQUFZeUksVUFBWixDQUF1QixvQkFBdkIsRUFBNkNDLEtBQTdDLEVBQW9EL0csSUFBcEQsRUFBMEQsVUFBVTBILEdBQVYsRUFBZTtBQUN2RVgsY0FBTSxDQUFOLEVBQVNZLEtBQVQ7QUFDQUMsYUFBSztBQUNIOUMsaUJBQU8sWUFESjtBQUVIK0MsZ0JBQU0sMkNBRkg7QUFHSHNCLGlCQUFPLElBSEo7QUFJSHJCLGdCQUFNLFNBSkg7QUFLSHNCLDZCQUFtQixLQUxoQixFQUFMOztBQU9ELE9BVEQ7QUFVRCxLQWhCRDs7QUFrQkF4TixNQUFFLGlCQUFGLEVBQXFCc04sTUFBckIsQ0FBNEIsVUFBVTFGLENBQVYsRUFBYTtBQUN2Q0EsUUFBRUosY0FBRjtBQUNBLFVBQUkyRCxRQUFRbkwsRUFBRSxJQUFGLENBQVo7QUFDSW9FLGFBQU8rRyxNQUFNL0ssZUFBTixFQURYOztBQUdBMkMsWUFBTUcsV0FBTixDQUFrQixXQUFsQixFQUErQixPQUEvQixFQUF3Qyx3QkFBeEM7QUFDQTFDLFdBQUtpQyxNQUFMLENBQVl5SSxVQUFaLENBQXVCLHVCQUF2QixFQUFnREMsS0FBaEQsRUFBdUQvRyxJQUF2RCxFQUE2RCxVQUFVMEgsR0FBVixFQUFlO0FBQzFFWCxjQUFNLENBQU4sRUFBU1ksS0FBVDtBQUNBQyxhQUFLO0FBQ0g5QyxpQkFBTyxZQURKO0FBRUgrQyxnQkFBTSw4Q0FGSDtBQUdIc0IsaUJBQU8sSUFISjtBQUlIckIsZ0JBQU0sU0FKSDtBQUtIc0IsNkJBQW1CLEtBTGhCLEVBQUw7O0FBT0QsT0FURDtBQVVELEtBaEJEOztBQWtCQXhOLE1BQUVzRCxNQUFGLEVBQVVtSyxNQUFWLENBQWlCLFlBQVc7QUFDMUIsVUFBSUMsTUFBTSxLQUFLQyxXQUFmO0FBQ0lDLGNBQVE1TixFQUFFc0QsTUFBRixFQUFVc0ssS0FBVixFQURaO0FBRUlDLGlCQUFZN04sRUFBRSwwQkFBRixFQUE4QjhOLE1BQTlCLEtBQXlDOU4sRUFBRSwwQkFBRixFQUE4QitOLE1BQTlCLEdBQXVDTCxHQUFoRixHQUFzRixHQUZ0RztBQUdJRyxpQkFBV0EsV0FBVyxFQUFYLEdBQWdCLENBQWhCLEdBQW9CQSxRQUEvQjtBQUNKLFVBQUlILE1BQU1HLFFBQVYsRUFBb0I7QUFDbEIsWUFBSWQsYUFBYWEsU0FBUyxHQUExQixFQUErQjtBQUM3QjVOLFlBQUUsc0NBQUYsRUFBMEN3RSxRQUExQyxDQUFtRCxVQUFuRCxFQUErREQsV0FBL0QsQ0FBMkUsVUFBM0U7QUFDQXZFLFlBQUUsc0NBQUYsRUFBMEN3RSxRQUExQyxDQUFtRCxVQUFuRCxFQUErREQsV0FBL0QsQ0FBMkUsVUFBM0U7QUFDQXZFLFlBQUUsb0NBQUYsRUFBd0N3RSxRQUF4QyxDQUFpRCxlQUFqRDtBQUNELFNBSkQ7QUFLSztBQUNIeEUsWUFBRSxzQ0FBRixFQUEwQ3dFLFFBQTFDLENBQW1ELFVBQW5ELEVBQStERCxXQUEvRCxDQUEyRSxVQUEzRTtBQUNBdkUsWUFBRSxvQ0FBRixFQUF3Q3dFLFFBQXhDLENBQWlELFVBQWpELEVBQTZERCxXQUE3RCxDQUF5RSxVQUF6RTtBQUNBdkUsWUFBRSx1Q0FBRixFQUEyQ3dFLFFBQTNDLENBQW9ELGVBQXBELEVBQXFFRCxXQUFyRSxDQUFpRixnQkFBakY7QUFDRDtBQUNGLE9BWEQsTUFXTztBQUNMdkUsVUFBRSxvQ0FBRixFQUF3Q3VFLFdBQXhDLENBQW9ELGVBQXBELEVBQXFFQSxXQUFyRSxDQUFpRixVQUFqRixFQUE2RkMsUUFBN0YsQ0FBc0csVUFBdEc7QUFDQXhFLFVBQUUsc0NBQUYsRUFBMEN1RSxXQUExQyxDQUFzRCxVQUF0RCxFQUFrRUMsUUFBbEUsQ0FBMkUsVUFBM0U7QUFDQXhFLFVBQUUsc0NBQUYsRUFBMEN1RSxXQUExQyxDQUFzRCxVQUF0RCxFQUFrRUMsUUFBbEUsQ0FBMkUsVUFBM0U7QUFDQXhFLFVBQUUsdUNBQUYsRUFBMkN1RSxXQUEzQyxDQUF1RCxlQUF2RCxFQUF3RUMsUUFBeEUsQ0FBaUYsZ0JBQWpGO0FBQ0F4RSxVQUFFLHNCQUFGLEVBQTBCd0UsUUFBMUIsQ0FBbUMsZUFBbkM7QUFDRDs7QUFFRCxVQUFHc0ksT0FBSCxFQUFXO0FBQ1AsWUFBSWtCLGlCQUFrQmhPLEVBQUUsd0JBQUYsRUFBNEI4TixNQUE1QixFQUF0QjtBQUNJZCxrQkFBVWhOLEVBQUUsV0FBRixDQURkO0FBRUlpTyw4QkFBc0JqTyxFQUFFLHdCQUFGLEVBQTRCK04sTUFBNUIsR0FBcUNMLEdBQXJDLEdBQTJDLEdBRnJFO0FBR0YsWUFBR1YsUUFBUXJHLE1BQVgsRUFBbUI7QUFDakIsY0FBSStHLE1BQU1PLG1CQUFWLEVBQStCO0FBQzdCakIsb0JBQVEsQ0FBUixFQUFXL0MsS0FBWDtBQUNELFdBRkQsTUFFTztBQUNMK0Msb0JBQVEsQ0FBUixFQUFXOUUsSUFBWCxHQUFrQkMsS0FBbEI7QUFDRDtBQUNGO0FBQ0QsWUFBR3VGLE1BQU8xTixFQUFFLCtCQUFGLEVBQW1DK04sTUFBbkMsR0FBNENMLEdBQTVDLEdBQWtELEdBQTVELEVBQWlFO0FBQy9EMU4sWUFBRSwrQkFBRixFQUFtQ3dFLFFBQW5DLENBQTRDLHFCQUE1QztBQUNEO0FBQ0QsWUFBR2tKLE1BQU8xTixFQUFFLGtDQUFGLEVBQXNDK04sTUFBdEMsR0FBK0NMLEdBQS9DLEdBQXFELEVBQS9ELEVBQW1FO0FBQ2pFMU4sWUFBRSxrQ0FBRixFQUFzQ3dFLFFBQXRDLENBQStDLHFCQUEvQztBQUNEO0FBQ0QsWUFBR2tKLE1BQU1NLGNBQVQsRUFBeUI7QUFDdkJKLGtCQUFRNU4sRUFBRXNELE1BQUYsRUFBVXNLLEtBQVYsRUFBUjtBQUNBLGNBQUlBLFNBQVMsR0FBYixFQUFrQjtBQUNkNU4sY0FBRSxjQUFGLEVBQWtCOE4sTUFBbEIsQ0FBeUJFLGNBQXpCO0FBQ0FoTyxjQUFFLE1BQUYsRUFBVXVFLFdBQVYsQ0FBc0IsV0FBdEI7QUFDQXZFLGNBQUUsd0JBQUYsRUFBNEJ3RSxRQUE1QixDQUFxQyxVQUFyQztBQUNILFdBSkQ7QUFLSztBQUNIeEUsY0FBRSxjQUFGLEVBQWtCOE4sTUFBbEIsQ0FBeUIsQ0FBekI7QUFDQTlOLGNBQUUsTUFBRixFQUFVdUUsV0FBVixDQUFzQixXQUF0QjtBQUNBdkUsY0FBRSx3QkFBRixFQUE0QndFLFFBQTVCLENBQXFDLFVBQXJDO0FBQ0Q7QUFDRixTQVpELE1BWU07QUFDSnhFLFlBQUUsTUFBRixFQUFVd0UsUUFBVixDQUFtQixXQUFuQjtBQUNBeEUsWUFBRSxjQUFGLEVBQWtCOE4sTUFBbEIsQ0FBeUJKLEdBQXpCO0FBQ0UxTixZQUFFLHdCQUFGLEVBQTRCdUUsV0FBNUIsQ0FBd0MsVUFBeEM7QUFDSDtBQUNGO0FBQ0YsS0EzREQ7OztBQThEQXZFLE1BQUUsZUFBRixFQUFtQnNOLE1BQW5CLENBQTBCLFVBQVUxRixDQUFWLEVBQWE7QUFDckNBLFFBQUVKLGNBQUY7QUFDQSxVQUFJMkQsUUFBUW5MLEVBQUUsSUFBRixDQUFaO0FBQ0lvRSxhQUFPNUQsS0FBS2lDLE1BQUwsQ0FBWXlILFdBQVosQ0FBd0JpQixNQUFNL0ssZUFBTixFQUF4QixDQURYO0FBRUlnRSxXQUFLLHNCQUFMLElBQStCcEUsRUFBRSx1QkFBRixFQUEyQmtPLEdBQTNCLEVBQS9COztBQUVKbkwsWUFBTUcsV0FBTixDQUFrQixjQUFsQixFQUFrQyxPQUFsQyxFQUEyQywyQkFBM0M7QUFDRTFDLFdBQUtpQyxNQUFMLENBQVl5SSxVQUFaLENBQXVCLHVCQUF2QixFQUFnREMsS0FBaEQsRUFBdUQvRyxJQUF2RCxFQUE2RCxVQUFVMEgsR0FBVixFQUFlO0FBQzFFLFlBQUdBLElBQUlELE9BQVAsRUFBZTtBQUNiRyxlQUFLO0FBQ0g5QyxtQkFBTyxZQURKO0FBRUgrQyxrQkFBTSxnQ0FGSDtBQUdIc0IsbUJBQU8sSUFISjtBQUlIckIsa0JBQU0sU0FKSDtBQUtIc0IsK0JBQW1CLEtBTGhCLEVBQUw7O0FBT0QsU0FSRCxNQVFNO0FBQ0osY0FBRzFCLElBQUlxQyxNQUFQLEVBQWM7QUFDWixnQkFBSTtBQUNGbkMsbUJBQUssRUFBQzlDLE9BQU8sT0FBUixFQUFtQitDLE1BQU1ILElBQUlxQyxNQUFKLENBQVc3RCxJQUFYLENBQWdCLElBQWhCLENBQXpCLEVBQWtENEIsTUFBTSxPQUF4RCxFQUFtRUMsbUJBQW1CLElBQXRGLEVBQUw7QUFDRCxhQUZELENBRUMsT0FBTXZFLENBQU4sRUFBUTtBQUNQb0UsbUJBQUssRUFBQzlDLE9BQU8sT0FBUixFQUFtQitDLE1BQU0seUNBQXpCLEVBQXNFQyxNQUFNLE9BQTVFLEVBQXVGQyxtQkFBbUIsSUFBMUcsRUFBTDtBQUNEO0FBQ0Y7QUFDRjtBQUNGLE9BbEJEO0FBbUJILEtBMUJEOzs7QUE2QkFuTSxNQUFFLCtCQUFGLEVBQW1Db08sS0FBbkM7QUFDSSxnQkFBWTtBQUNWLFVBQUlSLFFBQVE1TixFQUFFc0QsTUFBRixFQUFVc0ssS0FBVixFQUFaO0FBQ0EsVUFBSUEsU0FBUyxHQUFiLEVBQWtCO0FBQ2hCN0ssY0FBTUcsV0FBTixDQUFrQixtQkFBbEIsRUFBdUMsT0FBdkMsRUFBZ0QseUJBQWhEO0FBQ0ExQyxhQUFLaUMsTUFBTCxDQUFZc0gsU0FBWixDQUFzQixZQUFZL0osRUFBRSxJQUFGLEVBQVFpRixPQUFSLENBQWdCLG9CQUFoQixFQUFzQ2IsSUFBdEMsQ0FBMkMsV0FBM0MsQ0FBWixHQUFzRSxNQUE1RixFQUFvRyxHQUFwRztBQUNEO0FBQ0YsS0FQTCxFQU9PLFlBQVk7QUFDYjVELFdBQUtpQyxNQUFMLENBQVlKLFNBQVosQ0FBc0I0SCxLQUF0QjtBQUNELEtBVEw7Ozs7QUFhQWpLLE1BQUUsWUFBRixFQUFnQjJFLEVBQWhCLENBQW1CLE9BQW5CLEVBQTRCLFlBQVk7QUFDcEMsVUFBSTBKLE1BQU1yTyxFQUFFLElBQUYsRUFBUW9FLElBQVIsQ0FBYSxlQUFiLENBQVY7QUFDSVcsV0FBTS9FLEVBQUVxTyxHQUFGLENBRFY7QUFFQXRMLFlBQU1HLFdBQU4sQ0FBa0IsV0FBbEIsRUFBK0IsT0FBL0IsRUFBd0MsK0JBQXhDO0FBQ0FsRCxRQUFFLFdBQUYsRUFBZXNPLE9BQWYsQ0FBdUIsRUFBQ0MsV0FBV3hKLEdBQUdnSixNQUFILEdBQVlMLEdBQXhCLEVBQXZCLEVBQXFELEdBQXJEO0FBQ0gsS0FMRDs7QUFPRCxHQXhPaUIsRUFBcEI7OztBQTJPQXhMLFFBQVFLLElBQVIsQ0FBYW9CLFNBQWIsR0FBeUI7QUFDdkIwSSxRQUFNLGdCQUFZO0FBQ2hCLFFBQUk3TCxPQUFPLElBQVg7QUFDSWlDLGFBQVNqQyxLQUFLaUMsTUFEbEI7O0FBR0FBLFdBQU9zRSxTQUFQO0FBQ0EvRyxNQUFFLE1BQUYsRUFBVXdFLFFBQVYsQ0FBbUIsV0FBbkI7O0FBRUF4RSxNQUFFLGVBQUYsRUFBbUJzRSxLQUFuQixDQUF5QixZQUFZO0FBQ2pDdkIsWUFBTUcsV0FBTixDQUFrQixXQUFsQixFQUErQixPQUEvQixFQUF3QywrQkFBeEM7QUFDQWxELFFBQUUsV0FBRixFQUFlc08sT0FBZixDQUF1QixFQUFDQyxXQUFXdk8sRUFBRSx3QkFBRixFQUE0QndPLFdBQTVCLEtBQTRDLEVBQXhELEVBQXZCLEVBQW9GLEdBQXBGO0FBQ0gsS0FIRDtBQUlELEdBWnNCLEVBQXpCOzs7QUFlQXRNLFFBQVFXLFdBQVIsQ0FBb0JjLFNBQXBCLEdBQWdDO0FBQzlCMEksUUFBTSxnQkFBWTtBQUNkLFFBQUk3TCxPQUFPLElBQVg7QUFDQUEsU0FBS2lPLFdBQUw7QUFDSCxHQUo2Qjs7QUFNOUJBLGVBQWEsdUJBQVU7QUFDckIsUUFBSWpPLE9BQU8sSUFBWDtBQUNJaUMsYUFBU2pDLEtBQUtpQyxNQURsQjs7QUFHQXpDLE1BQUUsZ0JBQUYsRUFBb0JxRSxLQUFwQixDQUEwQjtBQUN4QlAsWUFBTSxLQURrQjtBQUV4QkMsZ0JBQVUsSUFGYztBQUd4QnlCLHFCQUFlLElBSFM7QUFJeEJ0QixjQUFRLElBSmdCO0FBS3hCbEIsaUJBQVcscWxCQUxhO0FBTXhCQyxpQkFBVztBQUNYO0FBQ0E7QUFSd0IsS0FBMUI7QUFVQWpELE1BQUUsOENBQUYsRUFBa0RrQixJQUFsRCxDQUF1RCxZQUFVLENBQUV1QixPQUFPaUgsU0FBUCxDQUFrQjFKLEVBQUUsSUFBRixFQUFRb0UsSUFBUixDQUFhLEtBQWIsQ0FBbEIsRUFBeUMsQ0FBNUc7QUFDQXBFLE1BQUUsZ0NBQUYsRUFBb0MyRSxFQUFwQyxDQUF1QyxjQUF2QyxFQUF1RCxVQUFTQyxLQUFULEVBQWdCUCxLQUFoQixFQUF1QlEsWUFBdkIsRUFBcUNDLFNBQXJDLEVBQStDO0FBQ3BHLFVBQUlDLEtBQUsvRSxFQUFFcUUsTUFBTXFLLE9BQU4sQ0FBYzVKLFNBQWQsQ0FBRixDQUFUO0FBQ0E5RSxRQUFFLGtEQUFGLEVBQXNEK0ssSUFBdEQsQ0FBMkQsS0FBM0QsRUFBa0VoRyxHQUFHWCxJQUFILENBQVEsS0FBUixDQUFsRTtBQUNELEtBSEQ7O0FBS0QsR0ExQjZCLEVBQWhDOzs7QUE2QkFsQyxRQUFRWSxNQUFSLENBQWVhLFNBQWYsR0FBMkI7QUFDekIwSSxRQUFNLGdCQUFZO0FBQ2hCLFFBQUk3TCxPQUFPLElBQVg7QUFDSWlDLGFBQVNqQyxLQUFLaUMsTUFEbEI7O0FBR0FBLFdBQU9pQyxjQUFQO0FBQ0FqQyxXQUFPOEMsb0JBQVA7QUFDQTlDLFdBQU9zRCx3QkFBUDtBQUNBdEQsV0FBT3NFLFNBQVA7QUFDQXZHLFNBQUs4TCxrQkFBTDtBQUNELEdBVndCOztBQVl6QkEsc0JBQW9CLDhCQUFZO0FBQzVCLFFBQUlxQyxRQUFRLEVBQVo7QUFDSUMsUUFESixDQUNVL0gsS0FEVjtBQUVJckcsV0FBTyxJQUZYO0FBR0lxTyx1QkFBbUIsU0FBbkJBLGdCQUFtQixHQUFZO0FBQzdCN08sUUFBRSw2QkFBRixFQUFpQ3dFLFFBQWpDLENBQTBDLHNCQUExQztBQUNBeEUsUUFBRSxzQkFBRixFQUEwQnFOLElBQTFCO0FBQ0FyTixRQUFFLG9CQUFGLEVBQXdCb04sSUFBeEI7QUFDQXJLLFlBQU1HLFdBQU4sQ0FBa0IsVUFBbEIsRUFBOEIsT0FBOUIsRUFBdUMsMENBQXZDO0FBQ0QsS0FSTDtBQVNJNEwsdUJBQW1CLFNBQW5CQSxnQkFBbUIsR0FBWTtBQUMzQmpJLGNBQVE3RyxFQUFFLCtCQUFGLEVBQW1Db0UsSUFBbkMsQ0FBd0MsT0FBeEMsQ0FBUjtBQUNBLFVBQUd5QyxTQUFTLENBQUMsQ0FBYixFQUFlO0FBQ2I3RyxVQUFFLGNBQUYsRUFBa0JxRSxLQUFsQixDQUF3QixXQUF4QixFQUFxQ3dDLFFBQVEsQ0FBN0M7QUFDQTlELGNBQU1HLFdBQU4sQ0FBa0IsVUFBbEIsRUFBOEIsT0FBOUIsRUFBdUMsNEJBQXZDO0FBQ0QsT0FIRCxNQUdNO0FBQ0YyTDtBQUNIO0FBQ0osS0FqQkw7QUFrQklFLHFCQUFpQixTQUFqQkEsY0FBaUIsQ0FBVUgsSUFBVixFQUFnQjtBQUM3QjdMLFlBQU1HLFdBQU4sQ0FBa0IsbUJBQWxCLEVBQXVDLE9BQXZDLEVBQWdELDZCQUFoRDtBQUNBbEQsUUFBRSxvQkFBRixFQUF3QmdQLElBQXhCLENBQTZCSixLQUFLM0MsSUFBbEM7QUFDQWpNLFFBQUUsd0JBQUYsRUFBNEJnUCxJQUE1QixDQUFpQ0osS0FBS0ssUUFBdEM7QUFDSCxLQXRCTDtBQXVCSXhNLGFBQVNqQyxLQUFLaUMsTUF2QmxCOztBQXlCRnpDLE1BQUUsaUJBQUYsRUFBcUJrUCxVQUFyQixDQUFnQ2xQLEVBQUUsOEJBQUYsRUFBa0MrTixNQUFsQyxHQUEyQ29CLElBQTNFOztBQUVBblAsTUFBRSxjQUFGLEVBQWtCMkUsRUFBbEIsQ0FBcUIsYUFBckIsRUFBb0MsVUFBU0MsS0FBVCxFQUFnQlAsS0FBaEIsRUFBdUJRLFlBQXZCLEVBQW9DO0FBQ3RFLFVBQUl1SyxNQUFNcFAsRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsa0JBQWIsQ0FBVjtBQUNBLFVBQUdpSyxJQUFJOUosRUFBSixDQUFPVCxZQUFQLEVBQXFCVCxJQUFyQixDQUEwQixPQUExQixLQUFzQyxDQUFDLENBQTFDLEVBQTRDO0FBQ3hDeUs7QUFDSDtBQUNGLEtBTEQ7OztBQVFFLFFBQUdRLGVBQWVWLEtBQWxCLEVBQXdCO0FBQ3BCQSxjQUFRVyxLQUFLQyxLQUFMLENBQVdGLGVBQWVWLEtBQTFCLENBQVI7QUFDSCxLQUZELE1BRU07QUFDSjVMLFlBQU1HLFdBQU4sQ0FBa0IsZUFBbEIsRUFBbUMsT0FBbkMsRUFBNEMseUJBQTVDO0FBQ0FsRCxRQUFFb00sSUFBRixDQUFPO0FBQ0x6QyxhQUFLLFFBREE7QUFFTCtCLGdCQUFRLEtBRkg7QUFHTEcsaUJBQVMsaUJBQVVDLEdBQVYsRUFBZTtBQUN0QixjQUFHQSxJQUFJRCxPQUFQLEVBQWdCO0FBQ2R3RCwyQkFBZUcsT0FBZixDQUF1QixPQUF2QixFQUFnQ0YsS0FBS0csU0FBTCxDQUFlM0QsSUFBSTZDLEtBQW5CLENBQWhDO0FBQ0FBLG9CQUFRN0MsSUFBSTZDLEtBQVo7QUFDRDtBQUNGLFNBUkksRUFBUDs7QUFVRDs7QUFFRDNPLE1BQUUsY0FBRixFQUFrQnFFLEtBQWxCLENBQXdCO0FBQ3BCTixnQkFBVSxLQURVO0FBRXBCRyxjQUFRLEtBRlk7QUFHcEI0QixZQUFNLElBSGMsRUFBeEI7OztBQU1BOUYsTUFBRSxvQkFBRixFQUF3QjJFLEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DLFlBQVk7QUFDOUMsVUFBSXVILE9BQU9sTSxFQUFFLElBQUYsRUFBUW9FLElBQVIsQ0FBYSxNQUFiLENBQVgsQ0FBaUNzTCxXQUFqQztBQUNJOUIsY0FBUTVOLEVBQUVzRCxNQUFGLEVBQVVzSyxLQUFWLEVBRFo7QUFFSS9HLFdBRko7QUFHQSxVQUFHcUYsUUFBUSxVQUFYLEVBQXNCO0FBQ2xCd0Qsc0JBQWNqTixPQUFPc0gsU0FBUCxDQUFpQixzQkFBakIsQ0FBZDtBQUNBLFlBQUkyRixnQkFBZ0J6TyxTQUFwQixFQUErQjtBQUM3QnlPLHNCQUFZMUgsSUFBWixDQUFpQixZQUFXO0FBQ3hCOEc7QUFDSCxXQUZEO0FBR0QsU0FKRCxNQUlPO0FBQ0hBO0FBQ0g7QUFDRixPQVRILE1BU1M7QUFDREYsZUFBT3BPLEtBQUtpQyxNQUFMLENBQVkrRCxVQUFaLENBQXVCbUksTUFBTXpDLElBQU4sS0FBZSxFQUF0QyxDQUFQO0FBQ0osWUFBSTBDLElBQUosRUFBVTtBQUNSLGNBQUdoQixTQUFTLEdBQVosRUFBZ0I7QUFDZDhCLDBCQUFjak4sT0FBT3NILFNBQVAsQ0FBaUIsb0JBQWpCLENBQWQ7QUFDQSxnQkFBSTJGLGdCQUFnQnpPLFNBQXBCLEVBQStCO0FBQzdCeU8sMEJBQVkxSCxJQUFaLENBQWlCLFlBQVk7QUFDekIrRywrQkFBZUgsSUFBZjtBQUNILGVBRkQ7QUFHRCxhQUpELE1BSU07QUFDRkcsNkJBQWVILElBQWY7QUFDSDtBQUNGLFdBVEQsTUFTTTtBQUNKRywyQkFBZUgsSUFBZjtBQUNEO0FBQ0Y7QUFDSjtBQUNGLEtBOUJEO0FBK0JILEdBckd3QixFQUEzQjs7O0FBd0dBMU0sUUFBUVEsT0FBUixDQUFnQmlCLFNBQWhCLEdBQTRCO0FBQ3hCMEksUUFBTSxnQkFBWTtBQUNoQixRQUFJN0wsT0FBTyxJQUFYO0FBQ0lpQyxhQUFTakMsS0FBS2lDLE1BRGxCO0FBRUlrTixpQkFBYTNQLEVBQUUsdUNBQUYsQ0FGakI7QUFHSTRQLGlCQUFhNVAsRUFBRSx1Q0FBRixDQUhqQjs7QUFLQXlDLFdBQU93RCxrQkFBUDs7QUFFQTJKLGVBQVdqTCxFQUFYLENBQWMsY0FBZCxFQUE4QixVQUFTQyxLQUFULEVBQWdCUCxLQUFoQixFQUF1QlEsWUFBdkIsRUFBcUNDLFNBQXJDLEVBQStDO0FBQ3ZFLFVBQUlzSyxNQUFNcFAsRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsZUFBYixDQUFWOztBQUVBbkYsUUFBRSwyQkFBRjtBQUNLdUUsaUJBREwsQ0FDaUI2SyxJQUFJOUosRUFBSixDQUFPVCxZQUFQLEVBQXFCVCxJQUFyQixDQUEwQixLQUExQixDQURqQjtBQUVLSSxjQUZMLENBRWM0SyxJQUFJOUosRUFBSixDQUFPUixTQUFQLEVBQWtCVixJQUFsQixDQUF1QixLQUF2QixDQUZkO0FBR0wsS0FORDs7QUFRRXVMLGVBQVd4SyxJQUFYLENBQWdCLGVBQWhCLEVBQWlDaUosS0FBakM7QUFDSSxnQkFBVztBQUNUckwsWUFBTUcsV0FBTixDQUFrQixtQkFBbEIsRUFBdUMsT0FBdkMsRUFBZ0QscUNBQWhEO0FBQ0EwTSxpQkFBV3ZMLEtBQVgsQ0FBaUIsV0FBakIsRUFBOEJyRSxFQUFFLElBQUYsRUFBUW9FLElBQVIsQ0FBYSxPQUFiLENBQTlCO0FBQ0F3TCxpQkFBV3ZMLEtBQVgsQ0FBaUIsWUFBakI7QUFDRCxLQUxMLEVBS08sWUFBVztBQUNadUwsaUJBQVd2TCxLQUFYLENBQWlCLFdBQWpCO0FBQ0QsS0FQTDs7O0FBVUZyRSxNQUFFLHNCQUFGLEVBQTBCc04sTUFBMUIsQ0FBaUMsVUFBVTFGLENBQVYsRUFBYTtBQUM1Q0EsUUFBRUosY0FBRjtBQUNBLFVBQUkyRCxRQUFRbkwsRUFBRSxJQUFGLENBQVo7QUFDSW9FLGFBQU8zQixPQUFPeUgsV0FBUCxDQUFtQmlCLE1BQU0vSyxlQUFOLEVBQW5CLENBRFg7O0FBR0FnRSxXQUFLOUMsSUFBTCxHQUFZLHVDQUFaO0FBQ0E4QyxXQUFLeUwsUUFBTCxHQUFnQixJQUFoQjtBQUNBekwsV0FBSzBMLFlBQUwsR0FBb0IsSUFBcEI7O0FBRUEvTSxZQUFNRyxXQUFOLENBQWtCLG1CQUFsQixFQUF1QyxPQUF2QyxFQUFnRCwrQkFBaEQ7QUFDQTFDLFdBQUtpQyxNQUFMLENBQVl5SSxVQUFaLENBQXVCLHFDQUF2QixFQUE4REMsS0FBOUQsRUFBcUUvRyxJQUFyRSxFQUEyRSxVQUFVMEgsR0FBVixFQUFlO0FBQ3hGLFlBQUdBLElBQUlELE9BQVAsRUFBZTtBQUNiRyxlQUFLO0FBQ0g5QyxtQkFBTyxZQURKO0FBRUgrQyxrQkFBTSxnQ0FGSDtBQUdIc0IsbUJBQU8sSUFISjtBQUlIckIsa0JBQU0sU0FKSDtBQUtIc0IsK0JBQW1CLEtBTGhCLEVBQUw7O0FBT0QsU0FSRCxNQVFNO0FBQ0YsY0FBRzFCLElBQUlxQyxNQUFQLEVBQWM7QUFDVixnQkFBSTtBQUNGbkMsbUJBQUssRUFBQzlDLE9BQU8sT0FBUixFQUFtQitDLE1BQU1ILElBQUlxQyxNQUFKLENBQVc3RCxJQUFYLENBQWdCLElBQWhCLENBQXpCLEVBQWtENEIsTUFBTSxPQUF4RCxFQUFtRUMsbUJBQW1CLElBQXRGLEVBQUw7QUFDRCxhQUZELENBRUMsT0FBTXZFLENBQU4sRUFBUTtBQUNQb0UsbUJBQUssRUFBQzlDLE9BQU8sT0FBUixFQUFtQitDLE1BQU0seUNBQXpCLEVBQXNFQyxNQUFNLE9BQTVFLEVBQXVGQyxtQkFBbUIsSUFBMUcsRUFBTDtBQUNEO0FBQ0o7QUFDSjtBQUNGLE9BbEJEOztBQW9CRCxLQTlCRDtBQStCRCxHQTFEdUIsRUFBNUI7OztBQTZEQWpLLFFBQVFTLE9BQVIsQ0FBZ0JnQixTQUFoQixHQUE0QjtBQUMxQjBJLFFBQU0sZ0JBQVk7QUFDaEIsUUFBSTdMLE9BQU8sSUFBWDtBQUNJaUMsYUFBUyxLQUFLQSxNQURsQjtBQUVBekMsTUFBRSxnREFBRixFQUFvRGtCLElBQXBELENBQXlELFlBQVUsQ0FBRXVCLE9BQU9pSCxTQUFQLENBQWtCMUosRUFBRSxJQUFGLEVBQVFvRSxJQUFSLENBQWEsS0FBYixDQUFsQixFQUEwQyxDQUEvRzs7QUFFQXBFLE1BQUUsa0NBQUYsRUFBc0MyRSxFQUF0QyxDQUF5QyxjQUF6QyxFQUF5RCxVQUFTQyxLQUFULEVBQWdCUCxLQUFoQixFQUF1QlEsWUFBdkIsRUFBcUNDLFNBQXJDLEVBQStDO0FBQ3RHLFVBQUlDLEtBQUsvRSxFQUFFcUUsTUFBTXFLLE9BQU4sQ0FBYzVKLFNBQWQsQ0FBRixDQUFUO0FBQ0U5RSxRQUFFLDBCQUFGLEVBQThCK1AsR0FBOUIsQ0FBa0Msa0JBQWxDLEVBQXNELFNBQU9oTCxHQUFHWCxJQUFILENBQVEsS0FBUixDQUFQLEdBQXNCLEdBQTVFO0FBQ0gsS0FIRDtBQUlELEdBVnlCLEVBQTVCOzs7O0FBY0FsQyxRQUFRVSxZQUFSLENBQXFCZSxTQUFyQixHQUFpQztBQUMvQjBJLFFBQU0sZ0JBQVk7QUFDaEIsUUFBSTdMLE9BQU8sSUFBWDs7QUFFQVIsTUFBRSxxQkFBRixFQUF5QnNOLE1BQXpCLENBQWdDLFVBQVUxRixDQUFWLEVBQWE7QUFDM0NBLFFBQUVKLGNBQUY7QUFDQSxVQUFJMkQsUUFBUW5MLEVBQUUsSUFBRixDQUFaO0FBQ0lvRSxhQUFPLElBQUk0TCxRQUFKLENBQWE3RSxNQUFNLENBQU4sQ0FBYixDQURYOztBQUdBcEksWUFBTUcsV0FBTixDQUFrQixpQkFBbEIsRUFBcUMsT0FBckMsRUFBOEMsNkJBQTlDO0FBQ0ExQyxXQUFLaUMsTUFBTCxDQUFZeUksVUFBWixDQUF1Qiw4QkFBdkIsRUFBdURDLEtBQXZELEVBQThEL0csSUFBOUQsRUFBb0UsVUFBVTBILEdBQVYsRUFBZTtBQUNqRixZQUFHQSxJQUFJRCxPQUFQLEVBQWU7QUFDYkcsZUFBSztBQUNIOUMsbUJBQU8sWUFESjtBQUVIK0Msa0JBQU0sZ0NBRkg7QUFHSHNCLG1CQUFPLElBSEo7QUFJSHJCLGtCQUFNLFNBSkg7QUFLSHNCLCtCQUFtQixLQUxoQixFQUFMOztBQU9ELFNBUkQsTUFRTTtBQUNKLGNBQUcxQixJQUFJcUMsTUFBUCxFQUFjO0FBQ1osZ0JBQUk7QUFDRm5DLG1CQUFLLEVBQUM5QyxPQUFPLE9BQVIsRUFBbUIrQyxNQUFNSCxJQUFJcUMsTUFBSixDQUFXN0QsSUFBWCxDQUFnQixJQUFoQixDQUF6QixFQUFrRDRCLE1BQU0sT0FBeEQsRUFBbUVDLG1CQUFtQixJQUF0RixFQUFMO0FBQ0QsYUFGRCxDQUVDLE9BQU12RSxDQUFOLEVBQVE7QUFDUG9FLG1CQUFLLEVBQUM5QyxPQUFPLE9BQVIsRUFBbUIrQyxNQUFNLHlDQUF6QixFQUFzRUMsTUFBTSxPQUE1RSxFQUF1RkMsbUJBQW1CLElBQTFHLEVBQUw7QUFDRDtBQUNGO0FBQ0Y7QUFDRixPQWxCRCxFQWtCRztBQUNEOEQscUJBQWEsS0FEWjtBQUVEQyxpQkFBUyxxQkFGUjtBQUdEaEcscUJBQWEsS0FIWixFQWxCSDs7QUF1QkQsS0E3QkQ7QUE4QkQsR0FsQzhCLEVBQWpDOzs7QUFxQ0E7QUFDQSxJQUFJaUcsUUFBUTtBQUNSQyxVQUFRLGtCQUFXO0FBQ2ZwUSxNQUFFLGtCQUFGLEVBQXNCcUUsS0FBdEIsQ0FBNEI7QUFDeEJQLFlBQU0sS0FEa0I7QUFFeEJLLGFBQU8sR0FGaUI7QUFHeEJ1QixvQkFBYyxDQUhVO0FBSXhCeEIsY0FBUSxLQUpnQjtBQUt4QkYsaUJBQVcsS0FMYSxFQUE1Qjs7QUFPQWhFLE1BQUUsNkJBQUYsRUFBaUNzRSxLQUFqQyxDQUF1QyxZQUFXO0FBQzlDdEUsUUFBRSxJQUFGLEVBQVF3RSxRQUFSLENBQWlCLFFBQWpCO0FBQ0F4RSxRQUFFLDZCQUFGLEVBQWlDdUUsV0FBakMsQ0FBNkMsUUFBN0M7QUFDQXZFLFFBQUUsa0JBQUYsRUFBc0JxRSxLQUF0QixDQUE0QixXQUE1QixFQUF5Q0ksU0FBUyxHQUFULENBQXpDO0FBQ0gsS0FKRDtBQUtBekUsTUFBRSw2QkFBRixFQUFpQ3NFLEtBQWpDLENBQXVDLFlBQVc7QUFDOUN0RSxRQUFFLElBQUYsRUFBUXdFLFFBQVIsQ0FBaUIsUUFBakI7QUFDQXhFLFFBQUUsNkJBQUYsRUFBaUN1RSxXQUFqQyxDQUE2QyxRQUE3QztBQUNBdkUsUUFBRSxrQkFBRixFQUFzQnFFLEtBQXRCLENBQTRCLFdBQTVCLEVBQXlDSSxTQUFTLEdBQVQsQ0FBekM7QUFDSCxLQUpEO0FBS0gsR0FuQk8sRUFBWjs7O0FBc0JBekUsRUFBRUMsUUFBRixFQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFDekJpUSxRQUFNQyxNQUFOO0FBQ0gsQ0FGRDs7QUFJQTtBQUNBLElBQUl0RSxNQUFNO0FBQ05BLE9BQUssZUFBVztBQUNaOUwsTUFBRSxhQUFGLEVBQWlCcUUsS0FBakIsQ0FBdUI7QUFDbkJQLFlBQU0sS0FEYTtBQUVuQjJCLGdCQUFVLElBRlM7QUFHbkJ0QixhQUFPLEdBSFk7QUFJbkJ1QixvQkFBYyxDQUpLO0FBS25CeEIsY0FBUSxLQUxXO0FBTW5CeUIsa0JBQVksSUFOTztBQU9uQjBLLHFCQUFlLE1BUEk7QUFRbkJDLGtCQUFZO0FBQ1I7QUFDSUMsb0JBQVksSUFEaEI7QUFFSUMsa0JBQVUsU0FGZCxFQURROztBQUtSO0FBQ0lELG9CQUFZLElBRGhCO0FBRUlDLGtCQUFVLFNBRmQsRUFMUTs7QUFTUjtBQUNJRCxvQkFBWSxHQURoQjtBQUVJQyxrQkFBVTtBQUNOOUssd0JBQWMsQ0FEUixFQUZkLEVBVFEsQ0FSTyxFQUF2Qjs7Ozs7QUF5QkExRixNQUFFLHlCQUFGLEVBQTZCc0UsS0FBN0IsQ0FBbUMsWUFBVztBQUMxQ3RFLFFBQUUsYUFBRixFQUFpQnFFLEtBQWpCLENBQXVCLFdBQXZCO0FBQ0gsS0FGRDtBQUdBckUsTUFBRSwwQkFBRixFQUE4QnNFLEtBQTlCLENBQW9DLFlBQVc7QUFDM0N0RSxRQUFFLGFBQUYsRUFBaUJxRSxLQUFqQixDQUF1QixXQUF2QjtBQUNILEtBRkQ7QUFHSCxHQWpDSyxFQUFWOzs7QUFvQ0E7QUFDQXJFLEVBQUVDLFFBQUYsRUFBWUMsS0FBWixDQUFrQixZQUFXO0FBQ3pCNEwsTUFBSUEsR0FBSjtBQUNILENBRkQ7O0FBSUE7QUFDQSxJQUFJMkUsVUFBVTtBQUNWTCxVQUFRLGtCQUFXO0FBQ2ZwUSxNQUFFLGlCQUFGLEVBQXFCcUUsS0FBckIsQ0FBMkI7QUFDdkJQLFlBQU0sS0FEaUI7QUFFdkIyQixnQkFBVSxJQUZhO0FBR3ZCdEIsYUFBTyxHQUhnQjtBQUl2QnVCLG9CQUFjLENBSlM7QUFLdkJ4QixjQUFRLEtBTGU7QUFNdkJ5QixrQkFBWSxJQU5XO0FBT3ZCMkssa0JBQVk7QUFDUjtBQUNJQyxvQkFBWSxJQURoQjtBQUVJQyxrQkFBVSxTQUZkLEVBRFE7O0FBS1I7QUFDSUQsb0JBQVksSUFEaEI7QUFFSUMsa0JBQVUsU0FGZCxFQUxROztBQVNSO0FBQ0lELG9CQUFZLEdBRGhCO0FBRUlDLGtCQUFVO0FBQ045Syx3QkFBYyxDQURSLEVBRmQsRUFUUTs7O0FBZVI7QUFDSTZLLG9CQUFZLEdBRGhCO0FBRUlDLGtCQUFVO0FBQ041Syx5QkFBZSxJQURULEVBRmQsRUFmUSxDQVBXLEVBQTNCOzs7OztBQThCQTVGLE1BQUUsNkJBQUYsRUFBaUNzRSxLQUFqQyxDQUF1QyxZQUFXO0FBQzlDdEUsUUFBRSxpQkFBRixFQUFxQnFFLEtBQXJCLENBQTJCLFdBQTNCO0FBQ0gsS0FGRDtBQUdBckUsTUFBRSw4QkFBRixFQUFrQ3NFLEtBQWxDLENBQXdDLFlBQVc7QUFDL0N0RSxRQUFFLGlCQUFGLEVBQXFCcUUsS0FBckIsQ0FBMkIsV0FBM0I7QUFDSCxLQUZEO0FBR0gsR0F0Q1MsRUFBZDs7O0FBeUNBO0FBQ0FyRSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBVztBQUN6QnVRLFVBQVFMLE1BQVI7QUFDSCxDQUZEOztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0FwUSxFQUFFLFlBQVc7QUFDVCxNQUFHQSxFQUFFc0QsTUFBRixFQUFVc0ssS0FBVixDQUFnQixRQUFoQixDQUFILEVBQTZCO0FBQ3pCNU4sTUFBRSw0QkFBRixFQUFnQ3FOLElBQWhDO0FBQ0g7O0FBRURyTixJQUFFLFlBQUYsRUFBZ0JzRSxLQUFoQixDQUFzQixZQUFXO0FBQzdCdEUsTUFBRSw0QkFBRixFQUFnQzBRLFdBQWhDO0FBQ0gsR0FGRDtBQUdILENBUkQiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qZ2xvYmFsIHdpbmRvdyovXHJcbi8qZ2xvYmFsICQqL1xyXG4vKmdsb2JhbCBqUXVlcnkqL1xyXG4vKmdsb2JhbCBkb2N1bWVudCovXHJcbi8qZ2xvYmFsIHN3YWwqL1xyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuICBcInVzZSBzdHJpY3RcIjtcclxuICAkLmZuLnNlcmlhbGl6ZU9iamVjdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcbiAgICBvcHRpb25zID0galF1ZXJ5LmV4dGVuZCh7fSwgb3B0aW9ucyk7XHJcblxyXG4gICAgdmFyIHNlbGYgPSB0aGlzLFxyXG4gICAgICAgIGpzb24gPSB7fSxcclxuICAgICAgICBwdXNoX2NvdW50ZXJzID0ge30sXHJcbiAgICAgICAgcGF0dGVybnMgPSB7XHJcbiAgICAgICAgICBcInZhbGlkYXRlXCI6IC9eW2EtekEtWl1bYS16QS1aMC05X10qKD86XFxbKD86XFxkKnxbYS16QS1aMC05X10rKVxcXSkqJC8sXHJcbiAgICAgICAgICBcImtleVwiOiAvW2EtekEtWjAtOV9dK3woPz1cXFtcXF0pL2csXHJcbiAgICAgICAgICBcInB1c2hcIjogL14kLyxcclxuICAgICAgICAgIFwiZml4ZWRcIjogL15cXGQrJC8sXHJcbiAgICAgICAgICBcIm5hbWVkXCI6IC9eW2EtekEtWjAtOV9dKyQvXHJcbiAgICAgICAgfTtcclxuXHJcblxyXG4gICAgdGhpcy5idWlsZCA9IGZ1bmN0aW9uIChiYXNlLCBrZXksIHZhbHVlKSB7XHJcbiAgICAgIGJhc2Vba2V5XSA9IHZhbHVlO1xyXG4gICAgICByZXR1cm4gYmFzZTtcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5wdXNoX2NvdW50ZXIgPSBmdW5jdGlvbiAoa2V5KSB7XHJcbiAgICAgIGlmIChwdXNoX2NvdW50ZXJzW2tleV0gPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIHB1c2hfY291bnRlcnNba2V5XSA9IDA7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHB1c2hfY291bnRlcnNba2V5XSsrO1xyXG4gICAgfTtcclxuXHJcbiAgICBqUXVlcnkuZWFjaChqUXVlcnkodGhpcykuc2VyaWFsaXplQXJyYXkoKSwgZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgLy8gc2tpcCBpbnZhbGlkIGtleXNcclxuICAgICAgaWYgKCFwYXR0ZXJucy52YWxpZGF0ZS50ZXN0KHRoaXMubmFtZSkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHZhciBrLFxyXG4gICAgICAgICAga2V5cyA9IHRoaXMubmFtZS5tYXRjaChwYXR0ZXJucy5rZXkpLFxyXG4gICAgICAgICAgbWVyZ2UgPSB0aGlzLnZhbHVlLFxyXG4gICAgICAgICAgcmV2ZXJzZV9rZXkgPSB0aGlzLm5hbWU7XHJcblxyXG4gICAgICB3aGlsZSAoKGsgPSBrZXlzLnBvcCgpKSAhPT0gdW5kZWZpbmVkKSB7XHJcblxyXG4gICAgICAgIC8vIGFkanVzdCByZXZlcnNlX2tleVxyXG4gICAgICAgIHJldmVyc2Vfa2V5ID0gcmV2ZXJzZV9rZXkucmVwbGFjZShuZXcgUmVnRXhwKFwiXFxcXFtcIiArIGsgKyBcIlxcXFxdJFwiKSwgJycpO1xyXG5cclxuICAgICAgICAvLyBwdXNoXHJcbiAgICAgICAgaWYgKGsubWF0Y2gocGF0dGVybnMucHVzaCkpIHtcclxuICAgICAgICAgIG1lcmdlID0gc2VsZi5idWlsZChbXSwgc2VsZi5wdXNoX2NvdW50ZXIocmV2ZXJzZV9rZXkpLCBtZXJnZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBmaXhlZFxyXG4gICAgICAgIGVsc2UgaWYgKGsubWF0Y2gocGF0dGVybnMuZml4ZWQpKSB7XHJcbiAgICAgICAgICBtZXJnZSA9IHNlbGYuYnVpbGQoW10sIGssIG1lcmdlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIG5hbWVkXHJcbiAgICAgICAgZWxzZSBpZiAoay5tYXRjaChwYXR0ZXJucy5uYW1lZCkpIHtcclxuICAgICAgICAgIG1lcmdlID0gc2VsZi5idWlsZCh7fSwgaywgbWVyZ2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAganNvbiA9IGpRdWVyeS5leHRlbmQodHJ1ZSwganNvbiwgbWVyZ2UpO1xyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIHJldHVybiBqc29uO1xyXG4gIH07XHJcbn0pO1xyXG5cclxudmFyIElkeWxsaWMgPSBmdW5jdGlvbiAoKSB7fTtcclxuSWR5bGxpYy5IZWxwZXIgPSBmdW5jdGlvbiAoY29uZmlnKSB7XHJcbiAgICB0aGlzLmNvbmZpZyA9IGNvbmZpZztcclxuICAgIHRoaXMuYXVkaW9fdGFnID0gbmV3IEF1ZGlvKCk7XHJcbn07XHJcblxyXG5JZHlsbGljLkhvbWUgPSBmdW5jdGlvbiAocm9vdFNjb3BlKSB7XHJcbiAgICB0aGlzLmhlbHBlciA9IHJvb3RTY29wZS5oZWxwZXI7XHJcbn07XHJcblxyXG5JZHlsbGljLkNyZWF0ZXMgPSBmdW5jdGlvbiAocm9vdFNjb3BlKSB7XHJcbiAgICB0aGlzLmhlbHBlciA9IHJvb3RTY29wZS5oZWxwZXI7XHJcbn07XHJcblxyXG5JZHlsbGljLkNhcmVlcnMgPSBmdW5jdGlvbiAocm9vdFNjb3BlKSB7XHJcbiAgICB0aGlzLmhlbHBlciA9IHJvb3RTY29wZS5oZWxwZXI7XHJcbn07XHJcblxyXG5JZHlsbGljLkNhcmVlckRldGFpbCA9IGZ1bmN0aW9uIChyb290U2NvcGUpIHtcclxuICAgIHRoaXMuaGVscGVyID0gcm9vdFNjb3BlLmhlbHBlcjtcclxufTtcclxuXHJcbklkeWxsaWMuQ2FzZVN0dWRpZXMgPSBmdW5jdGlvbiAocm9vdFNjb3BlKSB7XHJcbiAgICB0aGlzLmhlbHBlciA9IHJvb3RTY29wZS5oZWxwZXI7XHJcbn07XHJcblxyXG5JZHlsbGljLlN0dWRpbyA9IGZ1bmN0aW9uIChyb290U2NvcGUpIHtcclxuICAgIHRoaXMuaGVscGVyID0gcm9vdFNjb3BlLmhlbHBlcjtcclxufTtcclxuXHJcbnZhciBfdXRpbCA9IHtcclxuICBwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBkYXRhLXJvbGU9XCJub25lXCIgY2xhc3M9XCJzbGljay1wcmV2LWljb24gaGlkZGVuLXhzXCIgYXJpYS1sYWJlbD1cIlByZXZpb3VzXCIgdGFiaW5kZXg9XCIwXCIgcm9sZT1cImJ1dHRvblwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHZpZXdCb3g9XCIwIDAgNTQgMzhcIj48ZGVmcz48c3R5bGU+LmJyYW5kLW9yYW5nZS1maWxsLWNvbG9ye2ZpbGw6I2VlNjMyMTt9PC9zdHlsZT48L2RlZnM+PHBhdGggZD1cIk0wIDE5YTE5IDE5IDAgMCAwIDM3Ljk1IDEuMDZoMTQuOTNhMSAxIDAgMSAwIDAtMkgzNy45NUExOSAxOSAwIDAgMCAwIDE5em0yIDBhMTcgMTcgMCAwIDEgMzMuOTUtLjk0SDE3LjgxTDIzIDEyLjg5YTEuMTMgMS4xMyAwIDAgMC0xLjU4LTEuNjFsLTcuMTQgNi45MWExLjA1IDEuMDUgMCAwIDAgMCAxLjUzbDcuMTQgNi45MmExLjE0IDEuMTQgMCAwIDAgLjc5LjMyIDEuMTIgMS4xMiAwIDAgMCAuNzktLjMgMS4wNyAxLjA3IDAgMCAwIDAtMS41NGwtNS4yNC01LjA2aDE4LjE5QTE3IDE3IDAgMCAxIDIgMTl6XCIgY2xhc3M9XCJicmFuZC1vcmFuZ2UtZmlsbC1jb2xvclwiLz48L3N2Zz48L2J1dHRvbj4nLFxyXG4gIG5leHRBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGRhdGEtcm9sZT1cIm5vbmVcIiBjbGFzcz1cInNsaWNrLW5leHQtaWNvbiBoaWRkZW4teHNcIiBhcmlhLWxhYmVsPVwiTmV4dFwiIHRhYmluZGV4PVwiMFwiIHJvbGU9XCJidXR0b25cIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB2aWV3Qm94PVwiMCAwIDU0IDM4XCI+PGRlZnM+PHN0eWxlPi5icmFuZC1vcmFuZ2UtZmlsbC1jb2xvcntmaWxsOiNlZTYzMjE7fTwvc3R5bGU+PC9kZWZzPjxwYXRoIGQ9XCJNMzUgMGExOSAxOSAwIDAgMC0xOC45NSAxOEgxLjEyYTEgMSAwIDEgMCAwIDJoMTQuOTNBMTkgMTkgMCAxIDAgMzUgMHptMCAzNmExNyAxNyAwIDAgMS0xNi45NS0xNmgxOC4xNEwzMSAyNS4xMWExLjExIDEuMTEgMCAwIDAgMCAxLjU3IDEuMTQgMS4xNCAwIDAgMCAxLjU4IDBsNy4xNC02LjkyYTEuMDUgMS4wNSAwIDAgMCAwLTEuNTJsLTcuMTQtNi45MWExLjE0IDEuMTQgMCAwIDAtMS41OCAwIDEuMDggMS4wOCAwIDAgMCAwIDEuNTVMMzYuMTkgMThIMTguMDVBMTcgMTcgMCAxIDEgMzUgMzZ6XCIgY2xhc3M9XCJicmFuZC1vcmFuZ2UtZmlsbC1jb2xvclwiLz48L3N2Zz48L2J1dHRvbj4nLFxyXG5cclxuICBzZW5kRXZlbnRHQTogZnVuY3Rpb24gKGV2ZW50Q2F0ZWdvcnksIGV2ZW50QWN0aW9uLCBldmVudExhYmVsKSB7XHJcbiAgICBpZih3aW5kb3cuZ2Epe1xyXG4gICAgICB3aW5kb3cuZ2EoJ3NlbmQnLCB7XHJcbiAgICAgICAgaGl0VHlwZTogJ2V2ZW50JyxcclxuICAgICAgICBldmVudENhdGVnb3J5OiBldmVudENhdGVnb3J5IHx8ICcnLFxyXG4gICAgICAgIGV2ZW50QWN0aW9uOiBldmVudEFjdGlvbiB8fCAnJyxcclxuICAgICAgICBldmVudExhYmVsOiBldmVudExhYmVsIHx8ICcnXHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH0sXHJcblxyXG4gIHNlbmRFeGNlcHRpb25HQTogZnVuY3Rpb24gKGV4RGVzY3JpcHRpb24pIHtcclxuICAgIGlmKHdpbmRvdy5nYSl7XHJcbiAgICAgIHdpbmRvdy5nYSgnc2VuZCcsICdleGNlcHRpb24nLCB7XHJcbiAgICAgICAgaGl0VHlwZTogJ2V2ZW50JyxcclxuICAgICAgICAnZXhEZXNjcmlwdGlvbic6IGV4RGVzY3JpcHRpb24sXHJcbiAgICAgICAgJ2V4RmF0YWwnOiBmYWxzZVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbn07XHJcblxyXG5JZHlsbGljLkhlbHBlci5wcm90b3R5cGUgPSB7XHJcbiAgICBpbml0QmFzaWNTbGljazogZnVuY3Rpb24gKCkge1xyXG4gICAgICAkKCcuc2xpZGVzJykuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcclxuICAgICAgICAgICAgZHJhZ2dhYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgc3dpcGU6IGZhbHNlLFxyXG4gICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgICBwcmV2QXJyb3c6IF91dGlsLnByZXZBcnJvdyxcclxuICAgICAgICAgICAgbmV4dEFycm93OiBfdXRpbC5uZXh0QXJyb3csXHJcbiAgICAgICAgICAgIHNwZWVkOiAxMDAwLy8sXHJcbiAgICAgICAgICAvLyBmYWRlOiB0cnVlLFxyXG4gICAgICAgICAgLy8gY3NzRWFzZTogJ2xpbmVhcidcclxuICAgICAgICB9O1xyXG4gICAgICAgICQuZXh0ZW5kKHBhcmFtcywgKCQodGhpcykuZGF0YSgnc2xpY2snKSB8fCB7fSkpO1xyXG4gICAgICAgICQodGhpcykuc2xpY2socGFyYW1zKTtcclxuICAgICAgICAvL0N1c3RvbWUgU2xpZGVyIENsaWNrXHJcbiAgICAgICAgJChcIi5wcm9qZWN0LXNlY3Rpb24gLnNsaWRlci0xXCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKFwiLnByb2plY3Qtc2VjdGlvbiAuc2xpZGVyLTJcIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICQoXCIucHJvamVjdC1zZWN0aW9uIC5mYmdcIikuYWRkQ2xhc3MoXCJoaWRlLXNsaWRlclwiKTtcclxuICAgICAgICAgICAgJChcIi5wcm9qZWN0LXNlY3Rpb24gLmJiZ1wiKS5yZW1vdmVDbGFzcyhcImhpZGUtc2xpZGVyXCIpO1xyXG4gICAgICAgICAgICAkKFwiLnNsaWRlc1wiKS5zbGljaygnc2xpY2tHb1RvJywgcGFyc2VJbnQoJzAnKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJChcIi5wcm9qZWN0LXNlY3Rpb24gLnNsaWRlci0yXCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKFwiLnByb2plY3Qtc2VjdGlvbiAuc2xpZGVyLTFcIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICQoXCIucHJvamVjdC1zZWN0aW9uIC5iYmdcIikuYWRkQ2xhc3MoXCJoaWRlLXNsaWRlclwiKTtcclxuICAgICAgICAgICAgJChcIi5wcm9qZWN0LXNlY3Rpb24gLmZiZ1wiKS5yZW1vdmVDbGFzcyhcImhpZGUtc2xpZGVyXCIpO1xyXG4gICAgICAgICAgICAkKFwiLnNsaWRlc1wiKS5zbGljaygnc2xpY2tHb1RvJywgcGFyc2VJbnQoJzEnKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcblxyXG5cclxuICAgIGhvd1dlRG9JdFNsaWNrOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICQoXCIuaG93LXdlLWRvLXNsaWRlc1wiKS5zbGljayh7XHJcbiAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIG5leHRBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGRhdGEtcm9sZT1cIm5vbmVcIiBjbGFzcz1cInNsaWNrLW5leHQtaWNvblwiIGFyaWEtbGFiZWw9XCJOZXh0XCIgdGFiaW5kZXg9XCIwXCIgcm9sZT1cImJ1dHRvblwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHZpZXdCb3g9XCIwIDAgNTQgMzhcIj48ZGVmcz48c3R5bGU+LmJyYW5kLW9yYW5nZS1maWxsLWNvbG9ye2ZpbGw6I2VlNjMyMTt9PC9zdHlsZT48L2RlZnM+PHBhdGggZD1cIk0zNSAwYTE5IDE5IDAgMCAwLTE4Ljk1IDE4SDEuMTJhMSAxIDAgMSAwIDAgMmgxNC45M0ExOSAxOSAwIDEgMCAzNSAwem0wIDM2YTE3IDE3IDAgMCAxLTE2Ljk1LTE2aDE4LjE0TDMxIDI1LjExYTEuMTEgMS4xMSAwIDAgMCAwIDEuNTcgMS4xNCAxLjE0IDAgMCAwIDEuNTggMGw3LjE0LTYuOTJhMS4wNSAxLjA1IDAgMCAwIDAtMS41MmwtNy4xNC02LjkxYTEuMTQgMS4xNCAwIDAgMC0xLjU4IDAgMS4wOCAxLjA4IDAgMCAwIDAgMS41NUwzNi4xOSAxOEgxOC4wNUExNyAxNyAwIDEgMSAzNSAzNnpcIiBjbGFzcz1cImJyYW5kLW9yYW5nZS1maWxsLWNvbG9yXCIvPjwvc3ZnPjwvYnV0dG9uPicsXHJcbiAgICAgICAgc3BlZWQ6IDEwMDBcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAkKFwiLmhvdy13ZS1kby1pdC1zZWN0aW9uIC5uYXYtbGlua3MgbGkgYVwiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBfdXRpbC5zZW5kRXZlbnRHQSgnSG93IHdlIGRvIGl0JywgJ0NsaWNrJywgJ0hvdyB3ZSBkbyBpdCBCbHVlIFNjcmVlbiBjbGljaycpO1xyXG4gICAgICAgICAgJChcIi5ob3ctd2UtZG8tc2xpZGVzXCIpLnNsaWNrKCdzbGlja0dvVG8nLCAkKHRoaXMpLmRhdGEoJ2luZGV4JykpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgICQoJy5ob3ctd2UtZG8tc2xpZGVzJykub24oJ2JlZm9yZUNoYW5nZScsIGZ1bmN0aW9uKGV2ZW50LCBzbGljaywgY3VycmVudFNsaWRlLCBuZXh0U2xpZGUpe1xyXG4gICAgICAgIHZhciBlbCA9ICQodGhpcyksXHJcbiAgICAgICAgICAgIG1haW5Db250YWluZXIgPSBlbC5jbG9zZXN0KFwiLmhvdy13ZS1kby1pdC1zZWN0aW9uXCIpLFxyXG4gICAgICAgICAgICBuYXZMaW5rc0NvbnRhaW5lciA9IG1haW5Db250YWluZXIuZmluZChcIi5uYXYtbGlua3MtY29udGFpbmVyXCIpLFxyXG4gICAgICAgICAgICBhY3RpdmVMaSA9IG5hdkxpbmtzQ29udGFpbmVyLmZpbmQoXCIuYWN0aXZlXCIpLFxyXG4gICAgICAgICAgICBhbGxMaSA9IG5hdkxpbmtzQ29udGFpbmVyLmZpbmQoXCJsaVwiKTtcclxuXHJcbiAgICAgICAgYWN0aXZlTGkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgIGFsbExpLmVxKG5leHRTbGlkZSkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgdGVhbUltYWdlU2xpZGVyU2xpY2s6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgJChcIi50ZWFtLWltYWdlLXNsaWRlclwiKS5zbGljayh7XHJcbiAgICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgYXV0b3BsYXlTcGVlZDogNTAwMCxcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogNSxcclxuICAgICAgICBjZW50ZXJNb2RlOiB0cnVlLFxyXG4gICAgICAgIHZhcmlhYmxlV2lkdGg6IHRydWUsXHJcbiAgICAgICAgYXNOYXZGb3I6ICcudGVhbS1uYW1lLXNsaWRlcidcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAkKCcudGVhbS1uYW1lLXNsaWRlcicpLnNsaWNrKHtcclxuICAgICAgICBkb3RzOiBmYWxzZSxcclxuICAgICAgICBhdXRvcGxheTogdHJ1ZSxcclxuICAgICAgICBhdXRvcGxheVNwZWVkOiA1MDAwLFxyXG4gICAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgICBhc05hdkZvcjogJy50ZWFtLWltYWdlLXNsaWRlcicsXHJcbiAgICAgICAgcHJldkFycm93OiBfdXRpbC5wcmV2QXJyb3csXHJcbiAgICAgICAgbmV4dEFycm93OiBfdXRpbC5uZXh0QXJyb3csXHJcbiAgICAgICAgZmFkZTogdHJ1ZVxyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgY29yZVRlYW1JbWFnZVNsaWRlclNsaWNrOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICQoXCIuY29yZV90ZWFtLWltYWdlLXNsaWRlclwiKS5zbGljayh7XHJcbiAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXHJcbiAgICAgICAgZm9jdXNPblNlbGVjdDogdHJ1ZSxcclxuICAgICAgICBhc05hdkZvcjogJy5jb3JlX3RlYW0tbmFtZS1zbGlkZXInXHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgJCgnLmNvcmVfdGVhbS1uYW1lLXNsaWRlcicpLnNsaWNrKHtcclxuICAgICAgICBhdXRvcGxheTogdHJ1ZSxcclxuICAgICAgICBhdXRvcGxheVNwZWVkOiA1MDAwLFxyXG4gICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgYXNOYXZGb3I6ICcuY29yZV90ZWFtLWltYWdlLXNsaWRlcicsXHJcbiAgICAgICAgZmFkZTogdHJ1ZVxyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgaW5pdFR3b1NsaWRlclNsaWNrOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICQoJy5zbGlkZXMtZm9yJykuc2xpY2soe1xyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICBhdXRvcGxheTogdHJ1ZSxcclxuICAgICAgICBhdXRvcGxheVNwZWVkOiA1MDAwLFxyXG4gICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgZmFkZTogdHJ1ZSxcclxuICAgICAgICBhc05hdkZvcjogJy5zbGlkZXMtbmF2J1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgICQoJy5zbGlkZXMtbmF2Jykuc2xpY2soe1xyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMyxcclxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIGFzTmF2Rm9yOiAnLnNsaWRlcy1mb3InLFxyXG4gICAgICAgIGNlbnRlck1vZGU6IHRydWUsXHJcbiAgICAgICAgZm9jdXNPblNlbGVjdDogdHJ1ZVxyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgcmFuZG9tSW5kZXg6IGZ1bmN0aW9uKGxlbil7XHJcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBsZW4pO1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRfcmFuZG9tOiBmdW5jdGlvbihkYXRhLCBjb3VudCl7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgaWYoY291bnQgJiYgY291bnQgPj0gMSkge1xyXG4gICAgICAgIHZhciBhcnIgPSBbXSxcclxuICAgICAgICAgICAgbGVuID0gZGF0YS5sZW5ndGgsXHJcbiAgICAgICAgICAgIGluZGV4SGFzaCA9IHt9O1xyXG4gICAgICAgIGlmKGNvdW50ID49IGxlbil7XHJcbiAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICB9XHJcbiAgICAgICAgd2hpbGUoMSl7XHJcbiAgICAgICAgICB2YXIgaW5kZXggPSBzZWxmLnJhbmRvbUluZGV4KGxlbik7XHJcbiAgICAgICAgICBpZihhcnIubGVuZ3RoID09IGNvdW50KXtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZighaW5kZXhIYXNoLmhhc093blByb3BlcnR5KGluZGV4KSAmJiBpbmRleCA8IGxlbil7XHJcbiAgICAgICAgICAgIGluZGV4SGFzaFtpbmRleF0gPSB0cnVlO1xyXG4gICAgICAgICAgICBhcnIucHVzaChkYXRhW2luZGV4XSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhcnI7XHJcbiAgICAgIH1lbHNlIHtcclxuICAgICAgICByZXR1cm4gZGF0YVtzZWxmLnJhbmRvbUluZGV4KGRhdGEubGVuZ3RoKV0gfHwgZGF0YVswXTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICB2aWRlb0luaXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgLyogUGxheSB2aWRlbyBpbiBhIG1vZGFsIHdpbmRvdyAqL1xyXG4gICAgICB2YXIgbXlDb250ZW50ID0gJCgnLm1vZGFsX2NvbnRlbnQnKSxcclxuICAgICAgICAgIHBsYXllcixcclxuICAgICAgICAgIHRyaWdnZXJCdXR0b25zID0gJCgnLm1vZGFsX3RyaWdnZXIsIC5idG5fd2F0Y2hfdmlkZW8nKSxcclxuICAgICAgICAgIGNsb3NlQnV0dG9uID0gJCgnLm1vZGFsX2hlYWQgYScpLFxyXG4gICAgICAgICAgdmltZW9JZCxcclxuICAgICAgICAgIG1vZGFsX292ZXJsYXkgPSAkKCcubW9kYWxfb3ZlcmxheScpO1xyXG5cclxuICAgICAgZnVuY3Rpb24gaGlkZU1vZGFsKGV2dCl7XHJcbiAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgJCgnYm9keScpLnJlbW92ZUNsYXNzKCdtb2RhbF9vcGVuZWQnKTtcclxuICAgICAgICBtb2RhbF9vdmVybGF5LnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICBfdXRpbC5zZW5kRXZlbnRHQSgnVmlkZW8gTW9kYWwnLCAnQ2xpY2snLCAnVmlkZW8gTW9kZWwgQ2xvc2VkJyk7XHJcbiAgICAgICAgdmFyIHRtcCA9ICQoJzxkaXYgY2xhc3M9XCJpZnJhbWVfY29udGFpbmVyXCI+PC9kaXY+Jyk7XHJcbiAgICAgICAgbXlDb250ZW50XHJcbiAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnbW9kYWxfb3BlbicpXHJcbiAgICAgICAgICAgIC5maW5kKCcuaWZyYW1lX2NvbnRhaW5lcicpWzBdLnJlcGxhY2VXaXRoKHRtcFswXSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgIHBsYXllci5wYXVzZVZpZGVvKCk7XHJcbiAgICAgICAgfWNhdGNoKGUpe31cclxuICAgICAgICBwbGF5ZXIgPSBudWxsO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBtb2RhbF9vdmVybGF5LmNsaWNrKGhpZGVNb2RhbCk7XHJcbiAgICAgICQoY2xvc2VCdXR0b24pLmNsaWNrKGhpZGVNb2RhbCk7XHJcblxyXG4gICAgICBmdW5jdGlvbiBsb2FkVmltZW9WaWRlbyAodmltZW9JZCkge1xyXG4gICAgICAgIHZhciBlbCxcclxuICAgICAgICAgICAgcG9wdXBFbCA9ICQoXCIjdmlkZW9fbW9kYWxfY29udGVudFwiKSxcclxuICAgICAgICAgICAgZGF0YTtcclxuICAgICAgICAkKCcudGh1bWJuYWlsLXNlY3Rpb24uYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgIHBsYXllci5sb2FkVmlkZW8odmltZW9JZCkudGhlbihmdW5jdGlvbihpZCkge1xyXG4gICAgICAgICAgcGxheWVyLnBsYXkoKS5jYXRjaCgpO1xyXG4gICAgICAgICAgZWwgPSAkKCcudGh1bWJuYWlsLXNlY3Rpb25bZGF0YS12aWRlby1pZD1cIicraWQrJ1wiXScpO1xyXG4gICAgICAgICAgZGF0YSA9IGVsLmRhdGEoKTtcclxuICAgICAgICAgIGVsLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdWaW1lbyBWaWRlbyBQbGF5ZWQnLCAnQ2xpY2snLCBkYXRhLnZpbWVvX3RpdGxlKTtcclxuICAgICAgICAgIC8vIHBvcHVwRWwuZmluZCgnLnZpbWVvX3RpdGxlJykuaHRtbChkYXRhLnZpbWVvX3RpdGxlKTtcclxuICAgICAgICAgIC8vIHBvcHVwRWwuZmluZCgnLnZpbWVvX2Rlc2NyaXB0aW9uJykuaHRtbChkYXRhLnZpbWVvX2Rlc2NyaXB0aW9uKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCgnI3ZpZGVvX21vZGFsX2NvbnRlbnQgLnRodW1ibmFpbC1zZWN0aW9uJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgdmltZW9JZCA9ICQodGhpcykuZGF0YSgndmlkZW9JZCcpO1xyXG4gICAgICAgICAgbG9hZFZpbWVvVmlkZW8odmltZW9JZCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgdHJpZ2dlckJ1dHRvbnMuY2xpY2soZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB2YXIgZWwgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICBkYXRhID0gZWwuZGF0YSgpLFxyXG4gICAgICAgICAgICBtb2RlbEVsID0gJChkYXRhLm1vZGFsX2lkKSxcclxuICAgICAgICAgICAgY29udGFpbmVyID0gbW9kZWxFbC5maW5kKCcuaWZyYW1lX2NvbnRhaW5lcicpWzBdO1xyXG4gICAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdWaWRlbyBNb2RhbCcsICdDbGljaycsIGRhdGEudmlkZW9Gb3IgKyBcIiBNb2RlbCBPcGVuXCIpO1xyXG4gICAgICAgICQoJ2JvZHknKS5hZGRDbGFzcygnbW9kYWxfb3BlbmVkJyk7XHJcbiAgICAgICAgbW9kYWxfb3ZlcmxheS5hZGRDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgbW9kZWxFbC5hZGRDbGFzcygnbW9kYWxfb3BlbicpO1xyXG4gICAgICAgIGlmKGRhdGEudmlkZW9fdHlwZSA9PSAneW91dHViZScpIHtcclxuICAgICAgICAgIHBsYXllciA9IG5ldyBZVC5QbGF5ZXIoY29udGFpbmVyLCB7XHJcbiAgICAgICAgICAgIHZpZGVvSWQ6IGRhdGEudmlkZW8sXHJcbiAgICAgICAgICAgIHBsYXllclZhcnM6IHsnYXV0b3BsYXknOiAxLCAnY29udHJvbHMnOiAwfSxcclxuICAgICAgICAgICAgZXZlbnRzOiB7XHJcbiAgICAgICAgICAgICAgJ29uUmVhZHknOiBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdWaWRlbyBQbGF5ZWQnLCAnQ2xpY2snLCBkYXRhLnZpZGVvRm9yKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1lbHNlIGlmKGRhdGEudmlkZW9fdHlwZSA9PSAndmltZW8nKXtcclxuICAgICAgICAgIHBsYXllciA9IG5ldyBWaW1lby5QbGF5ZXIoY29udGFpbmVyLCB7XHJcbiAgICAgICAgICAgIGlkOiBkYXRhLnZpZGVvLFxyXG4gICAgICAgICAgICBsb29wOiBmYWxzZSxcclxuICAgICAgICAgICAgdGl0bGU6IGZhbHNlLFxyXG4gICAgICAgICAgICBhdXRvcGxheTogdHJ1ZVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYoZGF0YS52aWRlb190eXBlID09ICd2aW1lb19hbGJ1bScpe1xyXG4gICAgICAgICAgbW9kZWxFbC5maW5kKCcuc2xpZGVzJykuc2xpY2soJ3NsaWNrR29UbycsIDApO1xyXG4gICAgICAgICAgdmltZW9JZCA9IG1vZGVsRWwuZmluZCgnLnRodW1ibmFpbC1zZWN0aW9uJykubm90KCcuc2xpY2stY2xvbmVkJykuZXEoMCkuZGF0YSgndmlkZW9JZCcpO1xyXG4gICAgICAgICAgcGxheWVyID0gbmV3IFZpbWVvLlBsYXllcihjb250YWluZXIsIHtcclxuICAgICAgICAgICAgaWQ6IHZpbWVvSWQsXHJcbiAgICAgICAgICAgIGxvb3A6IGZhbHNlLFxyXG4gICAgICAgICAgICB0aXRsZTogZmFsc2VcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgcGxheWVyLmdldEVuZGVkKCkudGhlbihmdW5jdGlvbihlbmRlZCkge1xyXG4gICAgICAgICAgfSkuY2F0Y2goZnVuY3Rpb24oZXJyb3IpIHtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgcGxheWVyLm9uKCdlbmRlZCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgICAgICB2YXIgbmV4dEVsID0gJCgnLnRodW1ibmFpbC1zZWN0aW9uW2RhdGEtdmlkZW8taWQ9XCInK3ZpbWVvSWQrJ1wiXScpLm5vdCgnLnNsaWNrLWNsb25lZCcpLm5leHQoKSxcclxuICAgICAgICAgICAgICAgICAgICBuZXh0SW5kZXggPSBuZXh0RWwuZGF0YSgnaW5kZXgnKTtcclxuICAgICAgICAgICAgICAgIG1vZGVsRWwuZmluZCgnLnNsaWRlcycpLnNsaWNrKCdzbGlja0dvVG8nLCBuZXh0SW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgdmltZW9JZCA9IG5leHRFbC5kYXRhKCd2aWRlb0lkJyk7XHJcbiAgICAgICAgICAgICAgICBsb2FkVmltZW9WaWRlbyh2aW1lb0lkKTtcclxuICAgICAgICAgICAgICB9Y2F0Y2goZSl7fVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBsb2FkVmltZW9WaWRlbyh2aW1lb0lkKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBsb2FkSW1hZ2U6IGZ1bmN0aW9uICh1cmwpIHtcclxuICAgICAgICB2YXIgYSA9IG5ldyBJbWFnZSgpO1xyXG4gICAgICAgIGEuc3JjID0gdXJsO1xyXG4gICAgfSxcclxuXHJcbiAgICBwbGF5QXVkaW86IGZ1bmN0aW9uIChzcmMsIHZvbHVtZSkge1xyXG4gICAgICAgIHRoaXMuYXVkaW9fdGFnLnBhdXNlKCk7XHJcbiAgICAgICAgdGhpcy5hdWRpb190YWcuc3JjID0gc3JjO1xyXG4gICAgICAgIGlmKHZvbHVtZSkge1xyXG4gICAgICAgICAgdGhpcy5hdWRpb190YWcudm9sdW1lID0gdm9sdW1lO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5hdWRpb190YWcucGxheSgpLmNhdGNoKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIHByb2Nlc3NEYXRhOiBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgIHZhciBkZXRhaWxzID0gJycsXHJcbiAgICAgICAgICAgIHRhZ3MgPSBkYXRhLnRhZ3M7XHJcbiAgICAgICAgaWYoZGF0YS5oYXNPd25Qcm9wZXJ0eSgndGFncycpKXtcclxuICAgICAgICAgIGRldGFpbHMgKz0gXCJMb29raW5nIGZvclwiO1xyXG4gICAgICAgICAgaWYodGFncy5oYXNPd25Qcm9wZXJ0eSgncHJvZHVjdF90YWdzJykpe1xyXG4gICAgICAgICAgICBkZXRhaWxzICs9ICcgUFJPRFVDVDogJyArIHRhZ3MucHJvZHVjdF90YWdzLmpvaW4oXCIsIFwiKSArICcuJztcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmKHRhZ3MuaGFzT3duUHJvcGVydHkoJ2Rlc2lnbl90YWdzJykpe1xyXG4gICAgICAgICAgICBkZXRhaWxzICs9ICcgREVTSUdOOiAnICsgdGFncy5kZXNpZ25fdGFncy5qb2luKFwiLCBcIikgKyAnLic7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZih0YWdzLmhhc093blByb3BlcnR5KCdkZXZlbG9wbWVudF90YWdzJykpe1xyXG4gICAgICAgICAgICBkZXRhaWxzICs9ICcgREVWRUxPUE1FTlQ6ICcgKyB0YWdzLmRldmVsb3BtZW50X3RhZ3Muam9pbihcIiwgXCIpICsgJy4nO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYodGFncy5oYXNPd25Qcm9wZXJ0eSgnYnVkZ2V0X3RhZ3MnKSl7XHJcbiAgICAgICAgICAgIGRldGFpbHMgKz0gJyBCVURHRVQ6ICcgKyB0YWdzLmJ1ZGdldF90YWdzLmpvaW4oXCIsIFwiKSArICcuJztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICBkZXRhaWxzID0gXCJMb29raW5nIGZvciBvdGhlciBkZXRhaWxzXCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRhdGEuZGV0YWlscyA9IGRldGFpbHM7XHJcbiAgICAgICAgZGVsZXRlIGRhdGEudGFncztcclxuICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgIH0sXHJcblxyXG4gICAgYWRkQWpheExvYWRlcjogZnVuY3Rpb24oc2NvcGUpe1xyXG4gICAgICB2YXIgbG9hZGVyID0gJzxzcGFuIGNsYXNzPVwiYWpheC1sb2FkZXJcIj48L3NwYW4+JztcclxuICAgICAgaWYoc2NvcGUuZmluZCgnLmFqYXgtbG9hZGVyJykpe1xyXG4gICAgICAgIHNjb3BlLmZpbmQoJy5hamF4LWxvYWRlcicpLnJlbW92ZSgpO1xyXG4gICAgICB9XHJcbiAgICAgIHNjb3BlLmFkZENsYXNzKCdsb2FkZXJJbicpO1xyXG4gICAgICBzY29wZS5hcHBlbmQobG9hZGVyKTtcclxuICAgICAgc2NvcGUuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcclxuICAgIH0sXHJcblxyXG4gICAgcmVtb3ZlQWpheExvYWRlcjogZnVuY3Rpb24oc2NvcGUpe1xyXG4gICAgICBzY29wZS5yZW1vdmVDbGFzcygnbG9hZGVySW4nKTtcclxuICAgICAgc2NvcGUuZmluZCgnLmFqYXgtbG9hZGVyJykucmVtb3ZlKCk7XHJcbiAgICAgIHNjb3BlLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XHJcbiAgICB9LFxyXG5cclxuICAgIHN1Ym1pdERhdGE6IGZ1bmN0aW9uICh1cmwsIGpGcm9tLCBkYXRhLCBjYiwgZXh0cmFQYXJhbXMpIHtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzLFxyXG4gICAgICAgICAgY29uZmlnID0gc2VsZi5jb25maWcsXHJcbiAgICAgICAgICBwYXJhbXMsXHJcbiAgICAgICAgICBidG4gPSBqRnJvbS5maW5kKCdidXR0b25bdHlwZT1cInN1Ym1pdFwiXScpO1xyXG4gICAgICBzZWxmLmFkZEFqYXhMb2FkZXIoYnRuKTtcclxuXHJcbiAgICAgIHBhcmFtcyA9IHtcclxuICAgICAgICB1cmw6IGNvbmZpZy5iYXNlVXJsICsgdXJsLFxyXG4gICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICdBdXRob3JpemF0aW9uJzogJ1Rva2VuIHRva2VuPScgKyBjb25maWcuYXBpS2V5XHJcbiAgICAgICAgfSxcclxuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICBjYWNoZTogZmFsc2UsXHJcbiAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcclxuICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXMpIHtcclxuICAgICAgICAgIHNlbGYucmVtb3ZlQWpheExvYWRlcihidG4pO1xyXG4gICAgICAgICAgaWYocmVzLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgakZyb21bMF0ucmVzZXQoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmKGNiKSB7XHJcbiAgICAgICAgICAgIGNiKHJlcyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBlcnJvcjogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgc2VsZi5yZW1vdmVBamF4TG9hZGVyKGJ0bik7XHJcbiAgICAgICAgICBfdXRpbC5zZW5kRXhjZXB0aW9uR0EoXCJTZXJ2ZXIgRXJyb3IgZm9yIFwiKyB1cmwpO1xyXG4gICAgICAgICAgc3dhbCh7dGl0bGU6IFwiT29wcyFcIiwgICB0ZXh0OiBcIlNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGxhdGVyLlwiLCAgIHR5cGU6IFwiZXJyb3JcIiwgICBjb25maXJtQnV0dG9uVGV4dDogXCJPS1wiIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgJC5leHRlbmQocGFyYW1zLCBleHRyYVBhcmFtcyB8fCB7fSk7XHJcbiAgICAgICQuYWpheChwYXJhbXMpO1xyXG4gICAgfVxyXG59O1xyXG5cclxuSWR5bGxpYy5wcm90b3R5cGUgPSB7XHJcbiAgaW5pdDogZnVuY3Rpb24gKGNvbmZpZykge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgc2VsZi5oZWxwZXIgPSBuZXcgSWR5bGxpYy5IZWxwZXIoY29uZmlnKTtcclxuICAgIHNlbGYuaGVscGVyLmluaXRCYXNpY1NsaWNrKCk7XHJcbiAgICBzZWxmLmluaXRFdmVudExpc3RlbmVycyhjb25maWcpO1xyXG5cclxuICAgIHN3aXRjaCAoY29uZmlnLnBhZ2VfdHlwZSl7XHJcbiAgICAgIGNhc2UgXCJob21lXCI6XHJcbiAgICAgICAgc2VsZi5ob21lID0gbmV3IElkeWxsaWMuSG9tZShzZWxmKTtcclxuICAgICAgICBzZWxmLmhvbWUuaW5pdCgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgY2FzZSBcImNyZWF0ZXNcIjpcclxuICAgICAgICBzZWxmLmNyZWF0ZXMgPSBuZXcgSWR5bGxpYy5DcmVhdGVzKHNlbGYpO1xyXG4gICAgICAgIHNlbGYuY3JlYXRlcy5pbml0KCk7XHJcbiAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICBjYXNlIFwiY2FzZV9zdHVkaWVzXCI6XHJcbiAgICAgICAgc2VsZi5jYXNlU3R1ZGllcyA9IG5ldyBJZHlsbGljLkNhc2VTdHVkaWVzKHNlbGYpO1xyXG4gICAgICAgIHNlbGYuY2FzZVN0dWRpZXMuaW5pdCgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgY2FzZSBcInN0dWRpb1wiOlxyXG4gICAgICAgIHNlbGYuc3R1ZGlvID0gbmV3IElkeWxsaWMuU3R1ZGlvKHNlbGYpO1xyXG4gICAgICAgIHNlbGYuc3R1ZGlvLmluaXQoKTtcclxuICAgICAgICBicmVhaztcclxuXHJcbiAgICAgIGNhc2UgXCJjYXJlZXJzXCI6XHJcbiAgICAgICAgc2VsZi5jYXJlZXJzID0gbmV3IElkeWxsaWMuQ2FyZWVycyhzZWxmKTtcclxuICAgICAgICBzZWxmLmNhcmVlcnMuaW5pdCgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgY2FzZSBcImNhcmVlcl9kZXRhaWxcIjpcclxuICAgICAgICBzZWxmLmNhcmVlckRldGFpbCA9IG5ldyBJZHlsbGljLkNhcmVlckRldGFpbChzZWxmKTtcclxuICAgICAgICBzZWxmLmNhcmVlckRldGFpbC5pbml0KCk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcblxyXG4gIH0sXHJcblxyXG4gIGluaXRFdmVudExpc3RlbmVyczogZnVuY3Rpb24gKGNvbmZpZykge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzLFxyXG4gICAgICAgIGlzX2hvbWUgPSBjb25maWcucGFnZV90eXBlID09IFwiaG9tZVwiLFxyXG4gICAgICAgIGlzX3N0dWRpbyA9IGNvbmZpZy5wYWdlX3R5cGUgPT0gXCJzdHVkaW9cIjtcclxuXHJcbiAgICBpZihpc19ob21lKSB7XHJcbiAgICAgIHZhciB2aWRlb0VsID0gJChcIiNiZy12aWRlb1wiKTtcclxuICAgICAgaWYgKHZpZGVvRWwubGVuZ3RoKSB7XHJcbiAgICAgICAgICB2aWRlb0VsWzBdLnBsYXkoKS5jYXRjaCgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgJChkb2N1bWVudCkub24oJ2NoYW5nZScsIFwiLmRhdGVfc2VjdGlvbiBpbnB1dFt0eXBlPSdyYWRpbyddXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIGluZGV4ID0gJCh0aGlzKS5wYXJlbnQoKS5pbmRleCgpLFxyXG4gICAgICAgICAgZWwgPSAkKFwiI2Nob29zZV9hbm90aGVyX3RpbWVfZmllbGRfc2V0IGZpZWxkc2V0XCIpLmVxKGluZGV4KTtcclxuXHJcbiAgICAgICQoXCIjY2hvb3NlX2Fub3RoZXJfdGltZV9maWVsZF9zZXQgZmllbGRzZXRcIikuYWRkQ2xhc3MoJ2hpZGRlbicpLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XHJcbiAgICAgIGVsLnJlbW92ZUNsYXNzKCdoaWRkZW4nKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xyXG4gICAgICBlbC5maW5kKFwiaW5wdXRbdHlwZT0ncmFkaW8nXVwiKVswXS5jaGVja2VkID0gdHJ1ZTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoZG9jdW1lbnQpLm9uKCdjaGFuZ2UnLCBcIi5jaG9vc2VfdG9tb3Jyb3dfdGltZSBpbnB1dFt0eXBlPSdyYWRpbyddXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgJChcIiNjaG9vc2VfYW5vdGhlcl90aW1lX2ZpZWxkX3NldFwiKS5hZGRDbGFzcygnaGlkZGVuJykuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIFwiLmNob29zZV9hbm90aGVyX3RpbWVcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgZWwgPSAkKFwiLmRhdGVfc2VjdGlvbiBpbnB1dFt0eXBlPSdyYWRpbyddXCIpO1xyXG4gICAgICAkKFwiI2Nob29zZV9hbm90aGVyX3RpbWVfZmllbGRfc2V0XCIpLnJlbW92ZUNsYXNzKCdoaWRkZW4nKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xyXG4gICAgICAkKFwiZmllbGRzZXQgaW5wdXRbdHlwZT0ncmFkaW8nXVwiKS5yZW1vdmVBdHRyKCdjaGVja2VkJyk7XHJcbiAgICAgIGVsLmVxKDApWzBdLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICBlbC5lcSgwKS50cmlnZ2VyKCdjaGFuZ2UnKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5tZW51LXBvcHVwLWljb24gLm1lbnUtaWNvbi1idG4nKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdNZW51JywgJ0NsaWNrJywgJ01lbnUgQnV0dG9uIE9wZW5lZCcpO1xyXG4gICAgICAkKCcubWVudS1zZWN0aW9uLXdyYXAnKS5yZW1vdmVDbGFzcygnaGlkZGVuJykuc2hvdygpO1xyXG4gICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcygnbWVudS1vcGVuJyk7XHJcbiAgICAgICQoJy5jbG9zZScpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCcubWVudS1zZWN0aW9uLXdyYXAnKS5hZGRDbGFzcygnaGlkZGVuJykuaGlkZSgpO1xyXG4gICAgICAgICQoXCJib2R5XCIpLnJlbW92ZUNsYXNzKCdtZW51LW9wZW4nKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKFwiLnZpZGVvX2VtYWlsX2Zvcm1cIikuc3VibWl0KGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgdmFyIGpGcm9tID0gJCh0aGlzKSxcclxuICAgICAgICAgIGRhdGEgPSBqRnJvbS5zZXJpYWxpemVPYmplY3QoKTtcclxuXHJcbiAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdWaWRlbyBMaW5rJywgJ0NsaWNrJywgJ1ZpZGVvIExpbmsgQWpheCByZXF1ZXN0Jyk7XHJcbiAgICAgIHNlbGYuaGVscGVyLnN1Ym1pdERhdGEoJ2FwaS92MS9pbnRyb192aWRlbycsIGpGcm9tLCBkYXRhLCBmdW5jdGlvbiAocmVzKSB7XHJcbiAgICAgICAgakZyb21bMF0ucmVzZXQoKTtcclxuICAgICAgICBzd2FsKHtcclxuICAgICAgICAgIHRpdGxlOiBcIlN0YXkgVHVuZWRcIixcclxuICAgICAgICAgIHRleHQ6IFwiWW91IHdpbGwgc2hvcnRseSByZWNlaXZlIHZpZGVvIGxpbmsgZW1haWxcIixcclxuICAgICAgICAgIHRpbWVyOiAzMDAwLFxyXG4gICAgICAgICAgdHlwZTogJ3N1Y2Nlc3MnLFxyXG4gICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgJChcIi5mYWN0X2Jvb2tfZm9ybVwiKS5zdWJtaXQoZnVuY3Rpb24gKGUpIHtcclxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB2YXIgakZyb20gPSAkKHRoaXMpLFxyXG4gICAgICAgICAgZGF0YSA9IGpGcm9tLnNlcmlhbGl6ZU9iamVjdCgpO1xyXG5cclxuICAgICAgX3V0aWwuc2VuZEV2ZW50R0EoJ0ZhY3QgQm9vaycsICdDbGljaycsICdGYWN0IGJvb2sgQWpheCByZXF1ZXN0Jyk7XHJcbiAgICAgIHNlbGYuaGVscGVyLnN1Ym1pdERhdGEoJ2FwaS92MS9kb3dubG9hZHMuanNvbicsIGpGcm9tLCBkYXRhLCBmdW5jdGlvbiAocmVzKSB7XHJcbiAgICAgICAgakZyb21bMF0ucmVzZXQoKTtcclxuICAgICAgICBzd2FsKHtcclxuICAgICAgICAgIHRpdGxlOiBcIlN0YXkgVHVuZWRcIixcclxuICAgICAgICAgIHRleHQ6IFwiWW91IHdpbGwgc2hvcnRseSByZWNlaXZlIGRvd25sb2FkIGxpbmsgZW1haWxcIixcclxuICAgICAgICAgIHRpbWVyOiAzMDAwLFxyXG4gICAgICAgICAgdHlwZTogJ3N1Y2Nlc3MnLFxyXG4gICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIHRvcCA9IHRoaXMucGFnZVlPZmZzZXQsXHJcbiAgICAgICAgICB3aWR0aCA9ICQod2luZG93KS53aWR0aCgpLFxyXG4gICAgICAgICAgZW5kUG9pbnQgPSAoJChcIltkYXRhLWhpZGUtbWVudS1zZWN0aW9uXVwiKS5oZWlnaHQoKSArICQoXCJbZGF0YS1oaWRlLW1lbnUtc2VjdGlvbl1cIikub2Zmc2V0KCkudG9wIC0gMTUwKTtcclxuICAgICAgICAgIGVuZFBvaW50ID0gZW5kUG9pbnQgPCA1MCA/IDAgOiBlbmRQb2ludDtcclxuICAgICAgaWYgKHRvcCA+IGVuZFBvaW50KSB7XHJcbiAgICAgICAgaWYgKGlzX3N0dWRpbyAmJiB3aWR0aCA+PSA3NjgpIHtcclxuICAgICAgICAgICQoJy5oZWFkZXItc2VjdGlvbi13cmFwIC5wYWdlLWhlYWRpbmcgcCcpLmFkZENsYXNzKCdoaWRlVGV4dCcpLnJlbW92ZUNsYXNzKCdzaG93VGV4dCcpO1xyXG4gICAgICAgICAgJCgnLmhlYWRlci1zZWN0aW9uLXdyYXAgLm1vYmlsZS1zdWJtZW51JykuYWRkQ2xhc3MoJ2hpZGVUZXh0JykucmVtb3ZlQ2xhc3MoJ3Nob3dUZXh0Jyk7XHJcbiAgICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCAucGFnZS1oZWFkaW5nJykuYWRkQ2xhc3MoJ3Njcm9sbEhlYWRpbmcnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCAubW9iaWxlLXN1Ym1lbnUnKS5hZGRDbGFzcygnaGlkZVRleHQnKS5yZW1vdmVDbGFzcygnc2hvd1RleHQnKTtcclxuICAgICAgICAgICQoJy5oZWFkZXItc2VjdGlvbi13cmFwIC5wYWdlLWhlYWRpbmcnKS5hZGRDbGFzcygnaGlkZVRleHQnKS5yZW1vdmVDbGFzcygnc2hvd1RleHQnKTtcclxuICAgICAgICAgICQoJy5oZWFkZXItc2VjdGlvbi13cmFwIC5tZW51LXBvcHVwLWljb24nKS5hZGRDbGFzcygnZml4ZWRQb3NpdGlvbicpLnJlbW92ZUNsYXNzKCdzdGF0aWNQb3NpdGlvbicpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCAucGFnZS1oZWFkaW5nJykucmVtb3ZlQ2xhc3MoJ3Njcm9sbEhlYWRpbmcnKS5yZW1vdmVDbGFzcygnaGlkZVRleHQnKS5hZGRDbGFzcygnc2hvd1RleHQnKTtcclxuICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCAucGFnZS1oZWFkaW5nIHAnKS5yZW1vdmVDbGFzcygnaGlkZVRleHQnKS5hZGRDbGFzcygnc2hvd1RleHQnKTtcclxuICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCAubW9iaWxlLXN1Ym1lbnUnKS5yZW1vdmVDbGFzcygnaGlkZVRleHQnKS5hZGRDbGFzcygnc2hvd1RleHQnKTtcclxuICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCAubWVudS1wb3B1cC1pY29uJykucmVtb3ZlQ2xhc3MoJ2ZpeGVkUG9zaXRpb24nKS5hZGRDbGFzcygnc3RhdGljUG9zaXRpb24nKTtcclxuICAgICAgICAkKCcuaGVhZGVyLXNlY3Rpb24td3JhcCcpLmFkZENsYXNzKCdmaXhlZFBvc2l0aW9uJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmKGlzX2hvbWUpe1xyXG4gICAgICAgICAgdmFyIHZpZXdQb3J0SGVpZ2h0ID0gICQoXCIubWFpbi12aWV3cG9ydC1zZWN0aW9uXCIpLmhlaWdodCgpLFxyXG4gICAgICAgICAgICAgIHZpZGVvRWwgPSAkKFwiI2JnLXZpZGVvXCIpLFxyXG4gICAgICAgICAgICAgIHZpZGVvT3V0Vmlld3BvcnRNYXggPSAkKFwiLndlLXNwZWNpYWxpemUtc2VjdGlvblwiKS5vZmZzZXQoKS50b3AgKyAzMDA7XHJcbiAgICAgICAgaWYodmlkZW9FbC5sZW5ndGgpIHtcclxuICAgICAgICAgIGlmICh0b3AgPiB2aWRlb091dFZpZXdwb3J0TWF4KSB7XHJcbiAgICAgICAgICAgIHZpZGVvRWxbMF0ucGF1c2UoKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZpZGVvRWxbMF0ucGxheSgpLmNhdGNoKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKHRvcCA+ICgkKFwiLnByb2plY3Qtc2VjdGlvbi13cmFwLmJyaWdfYmdcIikub2Zmc2V0KCkudG9wIC0gMTIwKSl7XHJcbiAgICAgICAgICAkKFwiLnByb2plY3Qtc2VjdGlvbi13cmFwLmJyaWdfYmdcIikuYWRkQ2xhc3MoJ2luX3ZpZXdwb3J0X3NlY3Rpb24nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYodG9wID4gKCQoXCIucHJvamVjdC1zZWN0aW9uLXdyYXAuZmFua2F2ZV9iZ1wiKS5vZmZzZXQoKS50b3AgLSA4MCkpe1xyXG4gICAgICAgICAgJChcIi5wcm9qZWN0LXNlY3Rpb24td3JhcC5mYW5rYXZlX2JnXCIpLmFkZENsYXNzKCdpbl92aWV3cG9ydF9zZWN0aW9uJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKHRvcCA+IHZpZXdQb3J0SGVpZ2h0KSB7XHJcbiAgICAgICAgICB3aWR0aCA9ICQod2luZG93KS53aWR0aCgpO1xyXG4gICAgICAgICAgaWYgKHdpZHRoID49IDc2OCkge1xyXG4gICAgICAgICAgICAgICQoXCIucGhvbnRvbS1kaXZcIikuaGVpZ2h0KHZpZXdQb3J0SGVpZ2h0KTtcclxuICAgICAgICAgICAgICAkKFwiYm9keVwiKS5yZW1vdmVDbGFzcyhcIm92ZXJsYXlJblwiKTtcclxuICAgICAgICAgICAgICAkKFwiLndlLXNwZWNpYWxpemUtc2VjdGlvblwiKS5hZGRDbGFzcyhcInNjcm9sbEluXCIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICQoXCIucGhvbnRvbS1kaXZcIikuaGVpZ2h0KDApO1xyXG4gICAgICAgICAgICAkKFwiYm9keVwiKS5yZW1vdmVDbGFzcyhcIm92ZXJsYXlJblwiKTtcclxuICAgICAgICAgICAgJChcIi53ZS1zcGVjaWFsaXplLXNlY3Rpb25cIikuYWRkQ2xhc3MoXCJzY3JvbGxJblwiKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhcIm92ZXJsYXlJblwiKTtcclxuICAgICAgICAgICQoXCIucGhvbnRvbS1kaXZcIikuaGVpZ2h0KHRvcCk7XHJcbiAgICAgICAgICAgICQoXCIud2Utc3BlY2lhbGl6ZS1zZWN0aW9uXCIpLnJlbW92ZUNsYXNzKFwic2Nyb2xsSW5cIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgJChcIiNjb250YWN0X2Zyb21cIikuc3VibWl0KGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgdmFyIGpGcm9tID0gJCh0aGlzKSxcclxuICAgICAgICAgIGRhdGEgPSBzZWxmLmhlbHBlci5wcm9jZXNzRGF0YShqRnJvbS5zZXJpYWxpemVPYmplY3QoKSk7XHJcbiAgICAgICAgICBkYXRhWydnLXJlY2FwdGNoYS1yZXNwb25zZSddID0gJChcIiNnLXJlY2FwdGNoYS1yZXNwb25zZVwiKS52YWwoKTtcclxuXHJcbiAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdDb250YWN0IEZvcm0nLCAnQ2xpY2snLCAnQ29udGFjdCBmb3JtIEFqYXggcmVxdWVzdCcpO1xyXG4gICAgICAgIHNlbGYuaGVscGVyLnN1Ym1pdERhdGEoJ2FwaS92MS9pbnF1aXJpZXMuanNvbicsIGpGcm9tLCBkYXRhLCBmdW5jdGlvbiAocmVzKSB7XHJcbiAgICAgICAgICBpZihyZXMuc3VjY2Vzcyl7XHJcbiAgICAgICAgICAgIHN3YWwoe1xyXG4gICAgICAgICAgICAgIHRpdGxlOiBcIlN0YXkgVHVuZWRcIixcclxuICAgICAgICAgICAgICB0ZXh0OiBcIklkeWxsaWMgdGVhbSB3aWxsIGNvbnRhY3QgeW91LlwiLFxyXG4gICAgICAgICAgICAgIHRpbWVyOiAzMDAwLFxyXG4gICAgICAgICAgICAgIHR5cGU6ICdzdWNjZXNzJyxcclxuICAgICAgICAgICAgICBzaG93Q29uZmlybUJ1dHRvbjogZmFsc2VcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICAgIGlmKHJlcy5lcnJvcnMpe1xyXG4gICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBzd2FsKHt0aXRsZTogXCJPb3BzIVwiLCAgIHRleHQ6IHJlcy5lcnJvcnMuam9pbihcIiwgXCIpLCAgIHR5cGU6IFwiZXJyb3JcIiwgICBjb25maXJtQnV0dG9uVGV4dDogXCJPS1wiIH0pO1xyXG4gICAgICAgICAgICAgIH1jYXRjaChlKXtcclxuICAgICAgICAgICAgICAgIHN3YWwoe3RpdGxlOiBcIk9vcHMhXCIsICAgdGV4dDogXCJTb21ldGhpbmcgd2VudCB3cm9uZy4gUGxlYXNlIHRyeSBsYXRlci5cIiwgICB0eXBlOiBcImVycm9yXCIsICAgY29uZmlybUJ1dHRvblRleHQ6IFwiT0tcIiB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICAkKFwiZm9vdGVyIC5pZHlsbGljLWxvY2F0aW9ucyBpbWdcIikuaG92ZXIoXHJcbiAgICAgICAgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgdmFyIHdpZHRoID0gJCh3aW5kb3cpLndpZHRoKCk7XHJcbiAgICAgICAgICBpZiAod2lkdGggPj0gNzY4KSB7XHJcbiAgICAgICAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdGb290ZXIgdGVhbSBJbWFnZScsICdIb3ZlcicsICdGb290ZXIgVGVhbSBJbWFnZSBIb3ZlcicpO1xyXG4gICAgICAgICAgICBzZWxmLmhlbHBlci5wbGF5QXVkaW8oJy9hdWRpby8nICsgJCh0aGlzKS5jbG9zZXN0KCcuaWR5bGxpYy1sb2NhdGlvbnMnKS5kYXRhKCdzb3VuZFR5cGUnKSArICcubXAzJywgMC40KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBzZWxmLmhlbHBlci5hdWRpb190YWcucGF1c2UoKTtcclxuICAgICAgICB9XHJcbiAgICApO1xyXG5cclxuXHJcbiAgICAkKCcuc2Nyb2xsX3RvJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjbHMgPSAkKHRoaXMpLmRhdGEoJ3doZXJlVG9TY3JvbGwnKSxcclxuICAgICAgICAgICAgZWwgPSAgJChjbHMpO1xyXG4gICAgICAgIF91dGlsLnNlbmRFdmVudEdBKCdTY3JvbGwgVG8nLCAnQ2xpY2snLCAnU2Nyb2xsIHRvIEJvdW5jZSBidXR0b24gY2xpY2snKTtcclxuICAgICAgICAkKFwiaHRtbCxib2R5XCIpLmFuaW1hdGUoe3Njcm9sbFRvcDogZWwub2Zmc2V0KCkudG9wfSwgNDAwKTtcclxuICAgIH0pO1xyXG5cclxuICB9XHJcbn07XHJcblxyXG5JZHlsbGljLkhvbWUucHJvdG90eXBlID0ge1xyXG4gIGluaXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcyxcclxuICAgICAgICBoZWxwZXIgPSBzZWxmLmhlbHBlcjtcclxuXHJcbiAgICBoZWxwZXIudmlkZW9Jbml0KCk7XHJcbiAgICAkKFwiYm9keVwiKS5hZGRDbGFzcyhcIm92ZXJsYXlJblwiKTtcclxuXHJcbiAgICAkKFwiI2dpdmVfbWVfbW9yZVwiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgX3V0aWwuc2VuZEV2ZW50R0EoJ1Njcm9sbCBUbycsICdDbGljaycsICdTY3JvbGwgdG8gQm91bmNlIGJ1dHRvbiBjbGljaycpO1xyXG4gICAgICAgICQoXCJodG1sLGJvZHlcIikuYW5pbWF0ZSh7c2Nyb2xsVG9wOiAkKFwiLm1haW4tcGFnZS10b3Atc2VjdGlvblwiKS5vdXRlckhlaWdodCgpICsgNTB9LCA0MDApO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59O1xyXG5cclxuSWR5bGxpYy5DYXNlU3R1ZGllcy5wcm90b3R5cGUgPSB7XHJcbiAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHNlbGYuaW5pdEZhbmthdmUoKTtcclxuICB9LFxyXG5cclxuICBpbml0RmFua2F2ZTogZnVuY3Rpb24oKXtcclxuICAgIHZhciBzZWxmID0gdGhpcyxcclxuICAgICAgICBoZWxwZXIgPSBzZWxmLmhlbHBlcjtcclxuXHJcbiAgICAkKFwiLnNsaWRlLWZhbmthdmVcIikuc2xpY2soe1xyXG4gICAgICBkb3RzOiBmYWxzZSxcclxuICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgIGF1dG9wbGF5U3BlZWQ6IDUwMDAsXHJcbiAgICAgIGFycm93czogdHJ1ZSxcclxuICAgICAgcHJldkFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgZGF0YS1yb2xlPVwibm9uZVwiIGNsYXNzPVwic2xpY2stcHJldi1pY29uIGhpZGRlbi14c1wiIGFyaWEtbGFiZWw9XCJQcmV2aW91c1wiIHRhYmluZGV4PVwiMFwiIHJvbGU9XCJidXR0b25cIj48c3ZnIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIiB2aWV3Qm94PVwiMCAwIDU0IDM4XCI+PGRlZnM+PHN0eWxlPi5icmFuZC1vcmFuZ2UtZmlsbC1jb2xvcntmaWxsOiNlZTYzMjE7fTwvc3R5bGU+PC9kZWZzPjxwYXRoIGQ9XCJNMCAxOWExOSAxOSAwIDAgMCAzNy45NSAxLjA2aDE0LjkzYTEgMSAwIDEgMCAwLTJIMzcuOTVBMTkgMTkgMCAwIDAgMCAxOXptMiAwYTE3IDE3IDAgMCAxIDMzLjk1LS45NEgxNy44MUwyMyAxMi44OWExLjEzIDEuMTMgMCAwIDAtMS41OC0xLjYxbC03LjE0IDYuOTFhMS4wNSAxLjA1IDAgMCAwIDAgMS41M2w3LjE0IDYuOTJhMS4xNCAxLjE0IDAgMCAwIC43OS4zMiAxLjEyIDEuMTIgMCAwIDAgLjc5LS4zIDEuMDcgMS4wNyAwIDAgMCAwLTEuNTRsLTUuMjQtNS4wNmgxOC4xOUExNyAxNyAwIDAgMSAyIDE5elwiIGNsYXNzPVwiYnJhbmQtb3JhbmdlLWZpbGwtY29sb3JcIi8+PC9zdmc+PC9idXR0b24+JyxcclxuICAgICAgbmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgZGF0YS1yb2xlPVwibm9uZVwiIGNsYXNzPVwic2xpY2stbmV4dC1pY29uIGhpZGRlbi14c1wiIGFyaWEtbGFiZWw9XCJOZXh0XCIgdGFiaW5kZXg9XCIwXCIgcm9sZT1cImJ1dHRvblwiPjxzdmcgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiIHZpZXdCb3g9XCIwIDAgNTQgMzhcIj48ZGVmcz48c3R5bGU+LmJyYW5kLW9yYW5nZS1maWxsLWNvbG9ye2ZpbGw6I2VlNjMyMTt9PC9zdHlsZT48L2RlZnM+PHBhdGggZD1cIk0zNSAwYTE5IDE5IDAgMCAwLTE4Ljk1IDE4SDEuMTJhMSAxIDAgMSAwIDAgMmgxNC45M0ExOSAxOSAwIDEgMCAzNSAwem0wIDM2YTE3IDE3IDAgMCAxLTE2Ljk1LTE2aDE4LjE0TDMxIDI1LjExYTEuMTEgMS4xMSAwIDAgMCAwIDEuNTcgMS4xNCAxLjE0IDAgMCAwIDEuNTggMGw3LjE0LTYuOTJhMS4wNSAxLjA1IDAgMCAwIDAtMS41MmwtNy4xNC02LjkxYTEuMTQgMS4xNCAwIDAgMC0xLjU4IDAgMS4wOCAxLjA4IDAgMCAwIDAgMS41NUwzNi4xOSAxOEgxOC4wNUExNyAxNyAwIDEgMSAzNSAzNnpcIiBjbGFzcz1cImJyYW5kLW9yYW5nZS1maWxsLWNvbG9yXCIvPjwvc3ZnPjwvYnV0dG9uPicsXHJcbiAgICAgIC8vIGZhZGU6IHRydWUsXHJcbiAgICAgIC8vIGNzc0Vhc2U6ICdsaW5lYXInXHJcbiAgICB9KTtcclxuICAgICQoJy5pZGVhdGlvbi1zZWN0aW9uLXdyYXAgLnNsaWRlcyAuc2xpZGVzLWl0ZW1zJykuZWFjaChmdW5jdGlvbigpeyBoZWxwZXIubG9hZEltYWdlKCAkKHRoaXMpLmRhdGEoJ3VybCcpICk7fSk7XHJcbiAgICAkKCcuaWRlYXRpb24tc2VjdGlvbi13cmFwIC5zbGlkZXMnKS5vbignYmVmb3JlQ2hhbmdlJywgZnVuY3Rpb24oZXZlbnQsIHNsaWNrLCBjdXJyZW50U2xpZGUsIG5leHRTbGlkZSl7XHJcbiAgICAgIHZhciBlbCA9ICQoc2xpY2suJHNsaWRlc1tuZXh0U2xpZGVdKTtcclxuICAgICAgJCgnLmlkZWF0aW9uLXNlY3Rpb24td3JhcCAuaWRlYXRpb24td3JhcC11cC1pbWcgaW1nJykuYXR0cignc3JjJywgZWwuZGF0YSgndXJsJykpO1xyXG4gICAgfSk7XHJcblxyXG4gIH1cclxufTtcclxuXHJcbklkeWxsaWMuU3R1ZGlvLnByb3RvdHlwZSA9IHtcclxuICBpbml0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXMsXHJcbiAgICAgICAgaGVscGVyID0gc2VsZi5oZWxwZXI7XHJcblxyXG4gICAgaGVscGVyLmhvd1dlRG9JdFNsaWNrKCk7XHJcbiAgICBoZWxwZXIudGVhbUltYWdlU2xpZGVyU2xpY2soKTtcclxuICAgIGhlbHBlci5jb3JlVGVhbUltYWdlU2xpZGVyU2xpY2soKTtcclxuICAgIGhlbHBlci52aWRlb0luaXQoKTtcclxuICAgIHNlbGYuaW5pdEV2ZW50TGlzdGVuZXJzKCk7XHJcbiAgfSxcclxuXHJcbiAgaW5pdEV2ZW50TGlzdGVuZXJzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBmYWN0cyA9IFtdLFxyXG4gICAgICAgICAgZmFjdCwgaW5kZXgsXHJcbiAgICAgICAgICBzZWxmID0gdGhpcyxcclxuICAgICAgICAgIGFmdGVyTGFzdFNlY3Rpb24gPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoJy5lbWJhcnJhc3NpbmctZmFjdHMtc2VjdGlvbicpLmFkZENsYXNzKCdsYXN0LXNlY3Rpb24tdmlzaWJsZScpO1xyXG4gICAgICAgICAgICAkKCcuYnRuX3JlZnJlc2hfc2VjdGlvbicpLmhpZGUoKTtcclxuICAgICAgICAgICAgJCgnLmJ0bl9zbWlsZV9zZWN0aW9uJykuc2hvdygpO1xyXG4gICAgICAgICAgICBfdXRpbC5zZW5kRXZlbnRHQSgnQWJvdXQgVXMnLCAnQ2xpY2snLCAnQWJvdXQgVXMgTGFzdCBGYWN0IERvd25sb2FkIG1vcmUgdmlzaWJsZScpO1xyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGFib3V0VXNTb3VuZExvYWQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgaW5kZXggPSAkKFwiLmZhY3Rfc2xpZGVfaXRlbS5zbGljay1hY3RpdmVcIikuZGF0YSgnaW5kZXgnKTtcclxuICAgICAgICAgICAgICBpZihpbmRleCAhPSAtMSl7XHJcbiAgICAgICAgICAgICAgICAkKFwiLmZhY3Rfc2xpZGVzXCIpLnNsaWNrKCdzbGlja0dvVG8nLCBpbmRleCArIDEpO1xyXG4gICAgICAgICAgICAgICAgX3V0aWwuc2VuZEV2ZW50R0EoJ0Fib3V0IFVzJywgJ0NsaWNrJywgJ1Nob3cgbW9yZSBmYWN0IG9uIGFib3V0IHVzJyk7XHJcbiAgICAgICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBhZnRlckxhc3RTZWN0aW9uKCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGFmdGVyU291bmRMb2FkID0gZnVuY3Rpb24gKGZhY3QpIHtcclxuICAgICAgICAgICAgICBfdXRpbC5zZW5kRXZlbnRHQSgnRW1iYXJyYXNzaW5nIEZhY3QnLCAnQ2xpY2snLCAnU2hvdyBtb3JlIGVtYmFycmFzc2luZyBmYWN0Jyk7XHJcbiAgICAgICAgICAgICAgJChcIi5yYW5kb21fZmFjdHNfdGV4dFwiKS5odG1sKGZhY3QudGV4dCk7XHJcbiAgICAgICAgICAgICAgJChcIi5yYW5kb21fZmFjdHNfc3ViX3RleHRcIikuaHRtbChmYWN0LnN1Yl90ZXh0KTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBoZWxwZXIgPSBzZWxmLmhlbHBlcjtcclxuXHJcbiAgICAkKFwiLm1vYmlsZS1zdWJtZW51XCIpLnNjcm9sbExlZnQoJChcIi5tb2JpbGUtc3VibWVudSAuYWN0aXZlLXBhZ2VcIikub2Zmc2V0KCkubGVmdCk7XHJcblxyXG4gICAgJChcIi5mYWN0X3NsaWRlc1wiKS5vbignYWZ0ZXJDaGFuZ2UnLCBmdW5jdGlvbihldmVudCwgc2xpY2ssIGN1cnJlbnRTbGlkZSl7XHJcbiAgICAgIHZhciBlbHMgPSAkKHRoaXMpLmZpbmQoJy5mYWN0X3NsaWRlX2l0ZW0nKTtcclxuICAgICAgaWYoZWxzLmVxKGN1cnJlbnRTbGlkZSkuZGF0YSgnaW5kZXgnKSA9PSAtMSl7XHJcbiAgICAgICAgICBhZnRlckxhc3RTZWN0aW9uKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICAgIGlmKHNlc3Npb25TdG9yYWdlLmZhY3RzKXtcclxuICAgICAgICAgIGZhY3RzID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5mYWN0cyk7XHJcbiAgICAgIH1lbHNlIHtcclxuICAgICAgICBfdXRpbC5zZW5kRXZlbnRHQSgnR2V0IEZhY3QgQWpheCcsICdDbGljaycsICdHZXQgRmFjdCBEYXRhIGZyb20gQWpheCcpO1xyXG4gICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICB1cmw6ICcvZmFjdHMnLFxyXG4gICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXMpIHtcclxuICAgICAgICAgICAgaWYocmVzLnN1Y2Nlc3MpIHtcclxuICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdmYWN0cycsIEpTT04uc3RyaW5naWZ5KHJlcy5mYWN0cykpO1xyXG4gICAgICAgICAgICAgIGZhY3RzID0gcmVzLmZhY3RzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgICQoJy5mYWN0X3NsaWRlcycpLnNsaWNrKHtcclxuICAgICAgICAgIGF1dG9wbGF5OiBmYWxzZSxcclxuICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICBmYWRlOiB0cnVlXHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgJChcIi5yZWZyZXNoX2ZhY3RzX2J0blwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHR5cGUgPSAkKHRoaXMpLmRhdGEoJ3R5cGUnKSwgcGxheVByb21pc2UsXHJcbiAgICAgICAgICAgIHdpZHRoID0gJCh3aW5kb3cpLndpZHRoKCksXHJcbiAgICAgICAgICAgIGluZGV4O1xyXG4gICAgICAgIGlmKHR5cGUgPT0gJ2Fib3V0X3VzJyl7XHJcbiAgICAgICAgICAgIHBsYXlQcm9taXNlID0gaGVscGVyLnBsYXlBdWRpbyhcIi9hdWRpby9wYWdlX2ZsaXAubXAzXCIpO1xyXG4gICAgICAgICAgICBpZiAocGxheVByb21pc2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgIHBsYXlQcm9taXNlLnRoZW4oZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgIGFib3V0VXNTb3VuZExvYWQoKTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGFib3V0VXNTb3VuZExvYWQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZhY3QgPSBzZWxmLmhlbHBlci5nZXRfcmFuZG9tKGZhY3RzW3R5cGVdIHx8IFtdKTtcclxuICAgICAgICAgICAgaWYgKGZhY3QpIHtcclxuICAgICAgICAgICAgICBpZih3aWR0aCA+PSA3Njgpe1xyXG4gICAgICAgICAgICAgICAgcGxheVByb21pc2UgPSBoZWxwZXIucGxheUF1ZGlvKFwiL2F1ZGlvL3JlZnJlc2gubXAzXCIpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHBsYXlQcm9taXNlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgcGxheVByb21pc2UudGhlbihmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBhZnRlclNvdW5kTG9hZChmYWN0KTtcclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWZ0ZXJTb3VuZExvYWQoZmFjdCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYWZ0ZXJTb3VuZExvYWQoZmFjdCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcbn07XHJcblxyXG5JZHlsbGljLkNyZWF0ZXMucHJvdG90eXBlID0ge1xyXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXMsXHJcbiAgICAgICAgICBoZWxwZXIgPSBzZWxmLmhlbHBlcixcclxuICAgICAgICAgIHNsaWNrTmF2RWwgPSAkKCcuY3JlYXRlcy12aWV3cG9ydC1zZWN0aW9uIC5zbGlkZXMtbmF2JyksXHJcbiAgICAgICAgICBzbGlja0ZvckVsID0gJCgnLmNyZWF0ZXMtdmlld3BvcnQtc2VjdGlvbiAuc2xpZGVzLWZvcicpO1xyXG5cclxuICAgICAgaGVscGVyLmluaXRUd29TbGlkZXJTbGljaygpO1xyXG5cclxuICAgICAgc2xpY2tGb3JFbC5vbignYmVmb3JlQ2hhbmdlJywgZnVuY3Rpb24oZXZlbnQsIHNsaWNrLCBjdXJyZW50U2xpZGUsIG5leHRTbGlkZSl7XHJcbiAgICAgICAgICAgIHZhciBlbHMgPSAkKHRoaXMpLmZpbmQoJy5zbGlkZXMtaXRlbXMnKTtcclxuXHJcbiAgICAgICAgICAgICQoJy5jcmVhdGVzLXZpZXdwb3J0LXNlY3Rpb24nKVxyXG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKGVscy5lcShjdXJyZW50U2xpZGUpLmRhdGEoJ2NscycpKVxyXG4gICAgICAgICAgICAgICAgLmFkZENsYXNzKGVscy5lcShuZXh0U2xpZGUpLmRhdGEoJ2NscycpKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICAgIHNsaWNrTmF2RWwuZmluZCgnLnNsaWRlcy1pdGVtcycpLmhvdmVyKFxyXG4gICAgICAgICAgICBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICBfdXRpbC5zZW5kRXZlbnRHQSgnQ3JlYXRlIENhc2UgU3R1ZHknLCAnSG92ZXInLCAnQ3JlYXRlcyBQYWdlIENhc2UgU3R1ZHkgTW91c2UgSG92ZXInKTtcclxuICAgICAgICAgICAgICBzbGlja0ZvckVsLnNsaWNrKCdzbGlja0dvVG8nLCAkKHRoaXMpLmRhdGEoJ2luZGV4JykpO1xyXG4gICAgICAgICAgICAgIHNsaWNrRm9yRWwuc2xpY2soJ3NsaWNrUGF1c2UnKTtcclxuICAgICAgICAgICAgfSwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgc2xpY2tGb3JFbC5zbGljaygnc2xpY2tQbGF5Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG5cclxuICAgICAgJChcIiN3aGVyZV90b19iZWdpbl9mb3JtXCIpLnN1Ym1pdChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB2YXIgakZyb20gPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICBkYXRhID0gaGVscGVyLnByb2Nlc3NEYXRhKGpGcm9tLnNlcmlhbGl6ZU9iamVjdCgpKTtcclxuXHJcbiAgICAgICAgZGF0YS5uYW1lID0gXCJPbmx5IEVtYWlsIGlucXVpcmllcyBmcm9tIENyZWF0ZSBQYWdlXCI7XHJcbiAgICAgICAgZGF0YS5sb2NhdGlvbiA9IFwiTkFcIjtcclxuICAgICAgICBkYXRhLnBob25lX251bWJlciA9IFwiTkFcIjtcclxuXHJcbiAgICAgICAgX3V0aWwuc2VuZEV2ZW50R0EoJ0NyZWF0ZXMgSW5xdWlyaWVzJywgJ0NsaWNrJywgJ0NyZWF0ZXMgSW5xdWlyaWVzIEZvcm0gU3VibWl0Jyk7XHJcbiAgICAgICAgc2VsZi5oZWxwZXIuc3VibWl0RGF0YSgnYXBpL3YxL2lucXVpcmllcy9lbWFpbF9pbnF1aXJ5Lmpzb24nLCBqRnJvbSwgZGF0YSwgZnVuY3Rpb24gKHJlcykge1xyXG4gICAgICAgICAgaWYocmVzLnN1Y2Nlc3Mpe1xyXG4gICAgICAgICAgICBzd2FsKHtcclxuICAgICAgICAgICAgICB0aXRsZTogXCJTdGF5IFR1bmVkXCIsXHJcbiAgICAgICAgICAgICAgdGV4dDogXCJJZHlsbGljIHRlYW0gd2lsbCBjb250YWN0IHlvdS5cIixcclxuICAgICAgICAgICAgICB0aW1lcjogMzAwMCxcclxuICAgICAgICAgICAgICB0eXBlOiAnc3VjY2VzcycsXHJcbiAgICAgICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICAgIGlmKHJlcy5lcnJvcnMpe1xyXG4gICAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIHN3YWwoe3RpdGxlOiBcIk9vcHMhXCIsICAgdGV4dDogcmVzLmVycm9ycy5qb2luKFwiLCBcIiksICAgdHlwZTogXCJlcnJvclwiLCAgIGNvbmZpcm1CdXR0b25UZXh0OiBcIk9LXCIgfSk7XHJcbiAgICAgICAgICAgICAgICAgIH1jYXRjaChlKXtcclxuICAgICAgICAgICAgICAgICAgICBzd2FsKHt0aXRsZTogXCJPb3BzIVwiLCAgIHRleHQ6IFwiU29tZXRoaW5nIHdlbnQgd3JvbmcuIFBsZWFzZSB0cnkgbGF0ZXIuXCIsICAgdHlwZTogXCJlcnJvclwiLCAgIGNvbmZpcm1CdXR0b25UZXh0OiBcIk9LXCIgfSk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICB9KTtcclxuICAgIH1cclxufTtcclxuXHJcbklkeWxsaWMuQ2FyZWVycy5wcm90b3R5cGUgPSB7XHJcbiAgaW5pdDogZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzLFxyXG4gICAgICAgIGhlbHBlciA9IHRoaXMuaGVscGVyO1xyXG4gICAgJCgnLndvcmtfYXRfaWR5bGxpY19zZWN0aW9uIC5zbGlkZXMgLnNsaWRlcy1pdGVtcycpLmVhY2goZnVuY3Rpb24oKXsgaGVscGVyLmxvYWRJbWFnZSggJCh0aGlzKS5kYXRhKCd1cmwnKSApOyB9KTtcclxuXHJcbiAgICAkKCcud29ya19hdF9pZHlsbGljX3NlY3Rpb24gLnNsaWRlcycpLm9uKCdiZWZvcmVDaGFuZ2UnLCBmdW5jdGlvbihldmVudCwgc2xpY2ssIGN1cnJlbnRTbGlkZSwgbmV4dFNsaWRlKXtcclxuICAgICAgdmFyIGVsID0gJChzbGljay4kc2xpZGVzW25leHRTbGlkZV0pO1xyXG4gICAgICAgICQoJy53b3JrX2F0X2lkeWxsaWNfc2VjdGlvbicpLmNzcygnYmFja2dyb3VuZC1pbWFnZScsICd1cmwoJytlbC5kYXRhKCd1cmwnKSsnKScpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59O1xyXG5cclxuXHJcbklkeWxsaWMuQ2FyZWVyRGV0YWlsLnByb3RvdHlwZSA9IHtcclxuICBpbml0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgJChcIiNjYXJlZXJfZGV0YWlsX2Zvcm1cIikuc3VibWl0KGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgdmFyIGpGcm9tID0gJCh0aGlzKSxcclxuICAgICAgICAgIGRhdGEgPSBuZXcgRm9ybURhdGEoakZyb21bMF0pO1xyXG5cclxuICAgICAgX3V0aWwuc2VuZEV2ZW50R0EoJ0pvYiBBcHBsaWNhdGlvbicsICdDbGljaycsICdKb2IgQXBwbGljYXRpb24gRm9ybSBTdWJtaXQnKTtcclxuICAgICAgc2VsZi5oZWxwZXIuc3VibWl0RGF0YSgnYXBpL3YxL2pvYl9hcHBsaWNhdGlvbnMuanNvbicsIGpGcm9tLCBkYXRhLCBmdW5jdGlvbiAocmVzKSB7XHJcbiAgICAgICAgaWYocmVzLnN1Y2Nlc3Mpe1xyXG4gICAgICAgICAgc3dhbCh7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIlN0YXkgVHVuZWRcIixcclxuICAgICAgICAgICAgdGV4dDogXCJJZHlsbGljIHRlYW0gd2lsbCBjb250YWN0IHlvdS5cIixcclxuICAgICAgICAgICAgdGltZXI6IDMwMDAsXHJcbiAgICAgICAgICAgIHR5cGU6ICdzdWNjZXNzJyxcclxuICAgICAgICAgICAgc2hvd0NvbmZpcm1CdXR0b246IGZhbHNlXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICBpZihyZXMuZXJyb3JzKXtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICBzd2FsKHt0aXRsZTogXCJPb3BzIVwiLCAgIHRleHQ6IHJlcy5lcnJvcnMuam9pbihcIiwgXCIpLCAgIHR5cGU6IFwiZXJyb3JcIiwgICBjb25maXJtQnV0dG9uVGV4dDogXCJPS1wiIH0pO1xyXG4gICAgICAgICAgICB9Y2F0Y2goZSl7XHJcbiAgICAgICAgICAgICAgc3dhbCh7dGl0bGU6IFwiT29wcyFcIiwgICB0ZXh0OiBcIlNvbWV0aGluZyB3ZW50IHdyb25nLiBQbGVhc2UgdHJ5IGxhdGVyLlwiLCAgIHR5cGU6IFwiZXJyb3JcIiwgICBjb25maXJtQnV0dG9uVGV4dDogXCJPS1wiIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgY29udGVudFR5cGU6IGZhbHNlLFxyXG4gICAgICAgIGVuY3R5cGU6ICdtdWx0aXBhcnQvZm9ybS1kYXRhJyxcclxuICAgICAgICBwcm9jZXNzRGF0YTogZmFsc2VcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn07XHJcblxyXG4vL1dlIExlYXJuIFNlY3Rpb24gU2xpZGVyXHJcbnZhciBsZWFybiA9IHtcclxuICAgIHNsaWRlcjogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJChcIi53ZS1sZWFybi1zbGlkZXNcIikuc2xpY2soe1xyXG4gICAgICAgICAgICBkb3RzOiBmYWxzZSxcclxuICAgICAgICAgICAgc3BlZWQ6IDUwMCxcclxuICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxyXG4gICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJChcIi5XZS1sZWFybi1zZWN0aW9uIC5zbGlkZXItMVwiKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgJChcIi5XZS1sZWFybi1zZWN0aW9uIC5zbGlkZXItMlwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgJChcIi53ZS1sZWFybi1zbGlkZXNcIikuc2xpY2soXCJzbGlja0dvVG9cIiwgcGFyc2VJbnQoXCIwXCIpKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKFwiLldlLWxlYXJuLXNlY3Rpb24gLnNsaWRlci0yXCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICAgICAkKFwiLldlLWxlYXJuLXNlY3Rpb24gLnNsaWRlci0xXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICAgICAkKFwiLndlLWxlYXJuLXNsaWRlc1wiKS5zbGljayhcInNsaWNrR29Ub1wiLCBwYXJzZUludChcIjFcIikpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59O1xyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgICBsZWFybi5zbGlkZXIoKTtcclxufSk7XHJcblxyXG4vL3Jlcy1zbGlkZXJcclxudmFyIHJlcyA9IHtcclxuICAgIHJlczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJChcIi5yZXMtc2xpZGVzXCIpLnNsaWNrKHtcclxuICAgICAgICAgICAgZG90czogZmFsc2UsXHJcbiAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICBzcGVlZDogMzAwLFxyXG4gICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICAgIGFycm93czogZmFsc2UsXHJcbiAgICAgICAgICAgIGNlbnRlck1vZGU6IHRydWUsXHJcbiAgICAgICAgICAgIGNlbnRlclBhZGRpbmc6ICc0MHB4JyxcclxuICAgICAgICAgICAgcmVzcG9uc2l2ZTogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDEyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IFwidW5zbGlja1wiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDE5MjAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IFwidW5zbGlja1wiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDc2NyxcclxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgJChcIi5yZXMtc2xpZGVyIC5sZWZ0LWFycm93XCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKFwiLnJlcy1zbGlkZXNcIikuc2xpY2soJ3NsaWNrUHJldicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQoXCIucmVzLXNsaWRlciAucmlnaHQtYXJyb3dcIikuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoXCIucmVzLXNsaWRlc1wiKS5zbGljaygnc2xpY2tOZXh0Jyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn07XHJcblxyXG4vLyBDYWxsaW5nIHRoZSByZXMgc2xpZGVyIGZ1bmN0aW9uXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gICAgcmVzLnJlcygpO1xyXG59KTtcclxuXHJcbi8vQ2xpZW50cyBzbGlkZXJcclxudmFyIGNsaWVudHMgPSB7XHJcbiAgICBzbGlkZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICQoXCIuY2xpZW50cy1zbGlkZXNcIikuc2xpY2soe1xyXG4gICAgICAgICAgICBkb3RzOiBmYWxzZSxcclxuICAgICAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgICAgIHNwZWVkOiAzMDAsXHJcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcclxuICAgICAgICAgICAgYXJyb3dzOiBmYWxzZSxcclxuICAgICAgICAgICAgY2VudGVyTW9kZTogdHJ1ZSxcclxuICAgICAgICAgICAgcmVzcG9uc2l2ZTogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDEyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IFwidW5zbGlja1wiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDE5MjAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IFwidW5zbGlja1wiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDc2NyxcclxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiA0ODAsXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyaWFibGVXaWR0aDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQoXCIuY2xpZW50cy1zbGlkZXIgLmxlZnQtYXJyb3dcIikuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoXCIuY2xpZW50cy1zbGlkZXNcIikuc2xpY2soJ3NsaWNrUHJldicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgICQoXCIuY2xpZW50cy1zbGlkZXIgLnJpZ2h0LWFycm93XCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKFwiLmNsaWVudHMtc2xpZGVzXCIpLnNsaWNrKCdzbGlja05leHQnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufTtcclxuXHJcbi8vIENhbGxpbmcgdGhlIHJlcyBzbGlkZXIgZnVuY3Rpb25cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgICBjbGllbnRzLnNsaWRlcigpO1xyXG59KTtcclxuXHJcbi8vIC8vU2Nyb2xsIHRvIGNvbnRhY3QgcGFnZVxyXG4vLyAkKGZ1bmN0aW9uKCkge1xyXG4vLyAgICAgY29uc29sZS5sb2coKTtcclxuLy8gICAgIHZhciAkcm9vdCA9ICQoJ2h0bWwsIGJvZHknKTtcclxuLy9cclxuLy8gICAgICQoXCIjY29udGFjdC11c1wiKS5jbGljayhmdW5jdGlvbihlKSB7XHJcbi8vICAgICAgICAgdmFyIGxpbmtJZCA9ICQodGhpcykuYXR0cihcImhyZWZcIik7XHJcbi8vXHJcbi8vICAgICAgICAgaWYgKHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSA9PT0gXCIvXCIpIHtcclxuLy8gICAgICAgICAgICAgJHJvb3QuYW5pbWF0ZSh7XHJcbi8vICAgICAgICAgICAgICAgICBzY3JvbGxUb3A6ICQobGlua0lkKS5vZmZzZXQoKS50b3AgKyA1MDBcclxuLy8gICAgICAgICAgICAgfSwgNTAwKTtcclxuLy8gICAgICAgICB9IGVsc2Uge1xyXG4vLyAgICAgICAgICAgICAkcm9vdC5hbmltYXRlKHtcclxuLy8gICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogJChsaW5rSWQpLm9mZnNldCgpLnRvcFxyXG4vLyAgICAgICAgICAgICB9LCAxMDAwKTtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4vL1xyXG4vLyAgICAgfSk7XHJcbi8vIH0pO1xyXG5cclxuXHJcbiQoZnVuY3Rpb24oKSB7XHJcbiAgICBpZigkKHdpbmRvdykud2lkdGgoXCI8NzY4cHhcIikpe1xyXG4gICAgICAgICQoXCIubmF2LWxpc3QsIC5zb2NpYWwtY29udGFjdFwiKS5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgJChcIi5tZW51LWljb25cIikuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJChcIi5uYXYtbGlzdCwgLnNvY2lhbC1jb250YWN0XCIpLnNsaWRlVG9nZ2xlKCk7XHJcbiAgICB9KTtcclxufSk7XHJcbiJdfQ==
